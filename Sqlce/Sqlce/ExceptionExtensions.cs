using System;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Data.Sqlce.ExceptionExtensions
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Adds exception data for building the exception full blown report. </summary>
        /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para> </remarks>
    public static class Methods
    {

        /// <summary>
        /// Adds the <paramref name="exception"/> data to <paramref name="value"/> exception.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns>
        /// <c>true</c> if it <see cref="Exception"/> is not nothing; otherwise <c>false</c>
        /// </returns>
        public static bool AddExceptionData(Exception value, System.Data.SqlServerCe.SqlCeException exception)
        {
            if (value is object && exception is object)
            {
                value.Data.Add($"{value.Data.Count}-ErrorCode", exception.ErrorCode);
                if ((exception.Errors?.Count) > 0 == true)
                {
                    foreach (System.Data.SqlServerCe.SqlCeError err in exception.Errors)
                        value.Data.Add($"{value.Data.Count}-Error{err.NativeError}", err.ToString());
                }
            }

            return exception is object;
        }

        /// <summary> Adds exception data from the specified exception. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        public static bool AddExceptionData(this Exception exception)
        {
            return AddExceptionData(exception, exception as System.Data.SqlServerCe.SqlCeException);
        }

        /// <summary> Adds an exception data. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionDataThis(Exception exception)
        {
            return exception.AddExceptionData() || Core.ExceptionExtensions.Methods.AddExceptionData(exception) ||
                isr.Core.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData(exception);
        }

        /// <summary> Converts a value to a full blown string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a String. </returns>
        internal static string ToFullBlownString(this Exception value)
        {
            return value.ToFullBlownString(int.MaxValue);
        }

        /// <summary> Converts this object to a full blown string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="level"> The level. </param>
        /// <returns> The given data converted to a String. </returns>
        internal static string ToFullBlownString(this Exception value, int level)
        {
            return isr.Core.ExceptionExtensions.ExceptionExtensionMethods.ToFullBlownString(value, level, AddExceptionDataThis);
        }
    }
}
