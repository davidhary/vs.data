﻿using System;
using System.Reflection;

[assembly: AssemblyTitle(isr.Data.Sqlce.My.MyLibrary.AssemblyTitle)]
[assembly: AssemblyDescription(isr.Data.Sqlce.My.MyLibrary.AssemblyDescription)]
[assembly: AssemblyProduct(isr.Data.Sqlce.My.MyLibrary.AssemblyProduct)]
[assembly: CLSCompliant(true)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
