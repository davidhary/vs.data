﻿
namespace isr.Data.Sqlce.My
{

    /// <summary> Provides assembly information for the class library. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = 545;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Data SQL Compact Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Data SQL Compact Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Data.Sql.Compact";
    }
}