## ISR Data Sqlce<sub>&trade;</sub>: Sql Compact Edition Data Library

Data entities for SQL CE Connection and data access.
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*3.3.7586 2020-10-08*  
Converted to C#.

*3.2.6493 2017-10-11*  
Changes namespace for accessing the Compact edition
sub classes.

*3.1.6331 2017-05-02*  
Replaces messages queue with cancel detail event
arguments.

*3.0.5866 2016-01-23*  
Updates to .NET 4.6.1

*2.0.5163 2014-02-19*  
Uses local sync and async safe event handlers. Uses
diagnosis publishers.

*2.0.5126 2014-01-13*  
Tagged as 2014.

*2.0.5065 2013-11-13*  
Changes Read to Query current release.

*2.0.5029 2013-10-08*  
Imported from the data core library.

*1.2.4968 2013-08-08*  
Imported from the data library.

\(C\) 2006 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)
