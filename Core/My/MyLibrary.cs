
namespace isr.Data.Core.My
{

    /// <summary> Provides assembly information for the class library. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = (int)isr.Core.ProjectTraceEventId.Data;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Data Core SQL Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Data Core SQL Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Data.Core.SQL";
    }
}
