﻿using System;
using System.Reflection;

[assembly: AssemblyTitle(isr.Data.Core.My.MyLibrary.AssemblyTitle)]
[assembly: AssemblyDescription(isr.Data.Core.My.MyLibrary.AssemblyDescription)]
[assembly: AssemblyProduct(isr.Data.Core.My.MyLibrary.AssemblyProduct)]
[assembly: CLSCompliant(true)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
