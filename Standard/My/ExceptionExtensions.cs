using System;

namespace isr.Data.Core.CS.ExceptionExtensions
{

    /// <summary> Adds exception data for building the exception full blown report. </summary>
    /// <license>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    public static class ProjectMethods
    {

        /// <summary>   Adds an exception data. </summary>
        /// <remarks>   David, 3/31/2020. </remarks>
        /// <param name="exception">    The exception. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool AddExceptionData( System.Exception exception )
        {
            return isr.Core.Services.ExceptionExtensions.ProjectMethods.AddExceptionData( exception ); 
        }

    }

    /// <summary> Builds the full blown exception report. </summary>
    /// <license>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    public static class LocalMethods
    {

        /// <summary> Adds an exception data. </summary>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionData( System.Exception exception )
        {
            return ProjectMethods.AddExceptionData( exception );
        }

        /// <summary> Converts a value to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a String. </returns>

        public static string ToFullBlownString( this System.Exception value )
        {
            return LocalMethods.ToFullBlownString( value, int.MaxValue );
        }

        /// <summary> Converts this object to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <param name="level"> The level. </param>
        /// <returns> The given data converted to a String. </returns>

        public static string ToFullBlownString( this System.Exception value, int level )
        {
            return isr.Core.Services.ExceptionExtensions.Methods.ToFullBlownString( value, level, AddExceptionData );
        }

        /// <summary>   Converts this object to a full blown string. </summary>
        /// <remarks>   David, 3/31/2020. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="level">    The level. </param>
        /// <param name="addData">  Information describing the add. </param>
        /// <returns>   The given data converted to a String. </returns>
        public static string ToFullBlownString( Exception value, int level, Func<Exception, bool> addData )
        {
            return isr.Data.Core.CS.ExceptionExtensions.LocalMethods.ToFullBlownString( value, level, addData );
        }

    }

}

