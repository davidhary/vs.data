Imports System.Reflection
<Assembly: AssemblyCompany("Integrated Scientific Resources")>
<Assembly: AssemblyCopyright("(c) 2006 Integrated Scientific Resources, Inc. All rights reserved.")>  
<Assembly: AssemblyTrademark("Licensed under The MIT License.")> 
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)>

