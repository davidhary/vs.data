# Change log
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [3.3.8070] - 2022-02-04
* Targeting Visual Studio 2022, C# 10 and .NET 6.0.
* Update NuGet packages.
* Update build version.
* Display version file when updating build version.

## [3.3.7586] - 2020-10-08
* Converted to C#.

## [3.2.6493] - 2017-10/11
* Changes namespace for accessing the Compact edition sub classes.

## [3.1.6331] - 2017-05/02
* Replaces messages queue with cancel detail event arguments.

## [3.0.5866] - 2016-01-23
* Updates to .NET 4.6.1

## [2.0.5163] - 2014-02-19
* Uses local sync and async safe event handlers. Uses diagnosis publishers.

## [2.0.5126] - 2014-01-13
* Tagged as 2014.

## [2.0.5065] - 2013-11-13
* Changes Read to Query current release.

## [2.0.5029] - 2013-10-08
* Imported from the data core library.

## [1.2.4968] - 2013-08-08
* Imported from the data library.

\(C\) 2006 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
[3.3.8070]: https://bitbucket.org/davidhary/vs.data.git 
