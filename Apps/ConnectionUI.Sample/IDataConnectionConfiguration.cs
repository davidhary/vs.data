﻿//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>   Interface for data connection configuration. </summary>
    /// <remarks>   David, 2020-10-08. </remarks>
	public interface IDataConnectionConfiguration
	{
        /// <summary>   Gets selected source. </summary>
        /// <returns>   The selected source. </returns>
		string GetSelectedSource();
        /// <summary>   Saves a selected source. </summary>
        /// <param name="provider"> The provider. </param>
		void SaveSelectedSource(string provider);

        /// <summary>   Gets selected provider. </summary>
        /// <returns>   The selected provider. </returns>
		string GetSelectedProvider();
        /// <summary>   Saves a selected provider. </summary>
        /// <param name="provider"> The provider. </param>
		void SaveSelectedProvider(string provider);
	}
}
