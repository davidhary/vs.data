//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace Microsoft.Data.ConnectionUI
{
    /// <summary>
    /// Provide a default implementation for the storage of DataConnection Dialog UI configuration.
    /// </summary>
    /// <remarks>   David, 2020-10-08. </remarks>
	public class DataConnectionConfiguration : IDataConnectionConfiguration
	{
        /// <summary>   Filename of the configuration file. </summary>
		private const string _ConfigFileName = @"DataConnection.xml";

        /// <summary>   Full pathname of the full file. </summary>
		private readonly string _FullFilePath = null;

        /// <summary>   The document. </summary>
		private readonly XDocument _XDoc = null;

        /// <summary>   Available data sources: </summary>
		private IDictionary<string, DataSource> _DataSources;

        /// <summary>   Available data providers: </summary>
		private IDictionary<string, DataProvider> _DataProviders;

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="path"> Configuration file path. </param>
		public DataConnectionConfiguration(string path)
		{
			this._FullFilePath = !String.IsNullOrEmpty(path)
                ? Path.GetFullPath(Path.Combine(path, _ConfigFileName))
                : Path.Combine(System.Environment.CurrentDirectory, _ConfigFileName);
            if (!String.IsNullOrEmpty( this._FullFilePath ) && File.Exists( this._FullFilePath ))
			{
                this._XDoc = XDocument.Load( this._FullFilePath );
			}
			else
			{
                this._XDoc = new XDocument();
                this._XDoc.Add(new XElement("ConnectionDialog", new XElement("DataSourceSelection")));
			}

			this.RootElement = this._XDoc.Root;
		}

        /// <summary>   Gets or sets the root element. </summary>
        /// <value> The root element. </value>
		public XElement RootElement { get; set; }

        /// <summary>   Loads a configuration. </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="dialog">   The dialog. </param>
		public void LoadConfiguration(DataConnectionDialog dialog)
		{
			dialog.DataSources.Add(DataSource.SqlDataSource);
			dialog.DataSources.Add(DataSource.SqlFileDataSource);
			dialog.DataSources.Add(DataSource.OracleDataSource);
			dialog.DataSources.Add(DataSource.AccessDataSource);
			dialog.DataSources.Add(DataSource.OdbcDataSource);
			dialog.DataSources.Add(SqlCe.SqlCeDataSource);

			dialog.UnspecifiedDataSource.Providers.Add(DataProvider.SqlDataProvider);
			dialog.UnspecifiedDataSource.Providers.Add(DataProvider.OracleDataProvider);
			dialog.UnspecifiedDataSource.Providers.Add(DataProvider.OleDBDataProvider);
			dialog.UnspecifiedDataSource.Providers.Add(DataProvider.OdbcDataProvider);
			dialog.DataSources.Add(dialog.UnspecifiedDataSource);

            this._DataSources = new Dictionary<string, DataSource> {
                { DataSource.SqlDataSource.Name, DataSource.SqlDataSource },
                { DataSource.SqlFileDataSource.Name, DataSource.SqlFileDataSource },
                { DataSource.OracleDataSource.Name, DataSource.OracleDataSource },
                { DataSource.AccessDataSource.Name, DataSource.AccessDataSource },
                { DataSource.OdbcDataSource.Name, DataSource.OdbcDataSource },
                { SqlCe.SqlCeDataSource.Name, SqlCe.SqlCeDataSource },
                { dialog.UnspecifiedDataSource.DisplayName, dialog.UnspecifiedDataSource }
            };

            this._DataProviders = new Dictionary<string, DataProvider> {
                { DataProvider.SqlDataProvider.Name, DataProvider.SqlDataProvider },
                { DataProvider.OracleDataProvider.Name, DataProvider.OracleDataProvider },
                { DataProvider.OleDBDataProvider.Name, DataProvider.OleDBDataProvider },
                { DataProvider.OdbcDataProvider.Name, DataProvider.OdbcDataProvider },
                { SqlCe.SqlCeDataProvider.Name, SqlCe.SqlCeDataProvider }
            };


            string dsName = this.GetSelectedSource();
            if (!String.IsNullOrEmpty(dsName) && this._DataSources.TryGetValue(dsName, out DataSource ds ))
			{
				dialog.SelectedDataSource = ds;
			}

            string dpName = this.GetSelectedProvider();
            if (!String.IsNullOrEmpty(dpName) && this._DataProviders.TryGetValue(dpName, out DataProvider dp ))
			{
				dialog.SelectedDataProvider = dp;
			}
		}

        /// <summary>   Saves a configuration. </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="dcd">  The data connection dialog. </param>
		public void SaveConfiguration(DataConnectionDialog dcd)
		{
			if (dcd.SaveSelection)
			{
				DataSource ds = dcd.SelectedDataSource;
				if (ds != null)
				{
					if (ds == dcd.UnspecifiedDataSource)
					{
						this.SaveSelectedSource(ds.DisplayName);
					}
					else
					{
						this.SaveSelectedSource(ds.Name);
					}
				}
				DataProvider dp = dcd.SelectedDataProvider;
				if (dp != null)
				{
					this.SaveSelectedProvider(dp.Name);
				}

                this._XDoc.Save( this._FullFilePath );
			}
		}


        /// <summary>   Gets selected source. </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <returns>   The selected source. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public string GetSelectedSource()
		{
			try
			{
				XElement xElem = this.RootElement.Element("DataSourceSelection");
				XElement sourceElem = xElem.Element("SelectedSource");
				if (sourceElem != null)
				{
					return sourceElem.Value as string;
				}
			}
			catch
			{
				return null;
			}
			return null;
		}


        /// <summary>   Gets selected provider. </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <returns>   The selected provider. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public string GetSelectedProvider()
		{
			try
			{
				XElement xElem = this.RootElement.Element("DataSourceSelection");
				XElement providerElem = xElem.Element("SelectedProvider");
				if (providerElem != null)
				{
					return providerElem.Value as string;
				}
			}
			catch
			{
				return null;
			}
			return null;
		}

        /// <summary>   Saves a selected source. </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="source">   Source for the. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public void SaveSelectedSource(string source)
		{
			if (!String.IsNullOrEmpty(source))
			{
				try
				{
					XElement xElem = this.RootElement.Element("DataSourceSelection");
					XElement sourceElem = xElem.Element("SelectedSource");
					if (sourceElem != null)
					{
						sourceElem.Value = source;
					}
					else
					{
						xElem.Add(new XElement("SelectedSource", source));
					}
				}
				catch
				{
				}
			}

		}


        /// <summary>   Saves a selected provider. </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="provider"> The provider. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public void SaveSelectedProvider(string provider)
		{
			if (!String.IsNullOrEmpty(provider))
			{
				try
				{
					XElement xElem = this.RootElement.Element("DataSourceSelection");
					XElement sourceElem = xElem.Element("SelectedProvider");
					if (sourceElem != null)
					{
						sourceElem.Value = provider;
					}
					else
					{
						xElem.Add(new XElement("SelectedProvider", provider));
					}
				}
				catch
				{
				}
			}
		}
	}
}
