//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace Sample
{
    /// <summary>   A sample. </summary>
    /// <remarks>   David, 2020-10-08. </remarks>
	public class Sample
	{
        /// <summary>   Sample 1: </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="args"> An array of command-line argument strings. </param>
		[STAThread]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        private static void Main(string[] args)
		{
            String connectionString = "Data Source=localhost\\sqlx2019;integrated security=SSPI;persist security info=False";
            (bool Success, string ConnectionString) = Microsoft.Data.ConnectionUI.Dialog.My.Dialogs.ShowSqlDataSourceConnectionDialog( connectionString );
            if ( Success )
            {
                using SqlConnection connection = new SqlConnection(ConnectionString ) ;
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = "SELECT * FROM INFORMATION_SCHEMA.TABLES";
                using var reader = command.ExecuteReader();
                Console.WriteLine( "information schema tables:" );
                while ( reader.Read() )
                {
                    Console.WriteLine( reader["TABLE_NAME"] );
                }
                Console.Write( "done; hit enter to exit >>" );
                _ = Console.ReadKey();
            }

		}

#if (false)
        {
		// Sample 1: 
		[STAThread]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        private static void Main(string[] args)
		{
			DataConnectionDialog dcd = new DataConnectionDialog();
			DataConnectionConfiguration dcs = new DataConnectionConfiguration(null);
			dcs.LoadConfiguration(dcd);

			if (DataConnectionDialog.Show(dcd) == DialogResult.OK)
			{
                DbProviderFactory factory = DbProviderFactories.GetFactory(dcd.SelectedDataProvider.Name);
                using (var connection = factory.CreateConnection())
                {
                    connection.ConnectionString = dcd.ConnectionString;
                    connection.Open();
                    var command = connection.CreateCommand();
                    command.CommandType = CommandType.Text;
                    command.CommandText = "SELECT * FROM INFORMATION_SCHEMA.TABLES";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Console.WriteLine(reader["TABLE_NAME"]);
                        }
                        Console.Write( "done; hit enter to exit >>" );
                        _ = Console.ReadKey();
                    }
                }

				// load tables
                //using (SqlConnection connection = new SqlConnection(dcd.ConnectionString))
                //{
                //    connection.Open();
                //    SqlCommand cmd = new SqlCommand("SELECT * FROM sys.Tables", connection);

                //    using (SqlDataReader reader = cmd.ExecuteReader())
                //    {
                //        while (reader.Read())
                //        {
                //            Console.WriteLine(reader.HasRows);
                //        }
                //    }

                //}
			}

			dcs.SaveConfiguration(dcd);
		}
        }
#endif


#if (false)
        {
        // Sample 2: 
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [STAThread]
        static void Main(string[] args)
        {
            DataConnectionDialog dcd = new DataConnectionDialog();
            DataConnectionConfiguration dcs = new DataConnectionConfiguration(null);
            dcs.LoadConfiguration(dcd);
            //dcd.ConnectionString = "Data Source=ziz-vspro-sql05;Initial Catalog=Northwind;Persist Security Info=True;User ID=sa;Password=Admin_007";


            if (DataConnectionDialog.Show(dcd) == DialogResult.OK)
            {
                // load tables
                using (SqlConnection connection = new SqlConnection(dcd.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM sys.Tables", connection);

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Console.WriteLine(reader.HasRows);
                        }
                    }

                }
            }

            dcs.SaveConfiguration(dcd);
        }
        }
#endif
    }
}
