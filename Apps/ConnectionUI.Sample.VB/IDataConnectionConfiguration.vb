﻿'------------------------------------------------------------------------------ 
' <copyright company="Microsoft Corporation"> 
' Copyright (c) Microsoft Corporation. All rights reserved. 
' </copyright> 
'------------------------------------------------------------------------------ 

Imports System
Imports System.Collections.Generic
Imports System.Text

Namespace Microsoft.Data.ConnectionUI.VB

    ''' <summary> Interface for data connection configuration. </summary>
    ''' <remarks> David, 2020-10-08. </remarks>
    Public Interface IDataConnectionConfiguration

        ''' <summary> Gets selected source. </summary>
        ''' <returns> The selected source. </returns>
        Function GetSelectedSource() As String

        ''' <summary> Saves a selected source. </summary>
        ''' <param name="provider"> The provider. </param>
        Sub SaveSelectedSource(ByVal provider As String)

        ''' <summary> Gets selected provider. </summary>
        ''' <returns> The selected provider. </returns>
        Function GetSelectedProvider() As String

        ''' <summary> Saves a selected provider. </summary>
        ''' <param name="provider"> The provider. </param>
        Sub SaveSelectedProvider(ByVal provider As String)
    End Interface

End Namespace