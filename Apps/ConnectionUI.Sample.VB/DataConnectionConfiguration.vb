'------------------------------------------------------------------------------ 
' <copyright company="Microsoft Corporation"> 
' Copyright (c) Microsoft Corporation. All rights reserved. 
' </copyright> 
'------------------------------------------------------------------------------ 
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Xml.Linq
Imports System.IO

Imports Microsoft.Data.ConnectionUI

Namespace Microsoft.Data.ConnectionUI.VB

    ''' <summary>
    ''' Provide a default implementation for the storage of DataConnection Dialog UI configuration.
    ''' </summary>
    ''' <remarks> David, 2020-10-08. </remarks>
    Public Class DataConnectionConfiguration
        Implements IDataConnectionConfiguration

        ''' <summary> Filename of the configuration file. </summary>
        Private Const _ConfigFileName As String = "DataConnection.xml"

        ''' <summary> Full pathname of the full file. </summary>
        Private ReadOnly _FullFilePath As String = Nothing

        ''' <summary> The document. </summary>
        Private ReadOnly _XDoc As XDocument = Nothing

        ''' <summary> The data sources. </summary>
        Private _DataSources As IDictionary(Of String, DataSource)

        ''' <summary> The data providers. </summary>
        Private _DataProviders As IDictionary(Of String, DataProvider)

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-08. </remarks>
        ''' <param name="path"> Configuration file path. </param>
        Public Sub New(ByVal path As String)
            Me._FullFilePath = If(Not [String].IsNullOrEmpty(path),
                System.IO.Path.GetFullPath(System.IO.Path.Combine(path, _ConfigFileName)),
                System.IO.Path.Combine(System.Environment.CurrentDirectory, _ConfigFileName))
            If Not [String].IsNullOrEmpty(Me._FullFilePath) AndAlso File.Exists(Me._FullFilePath) Then
                Me._XDoc = XDocument.Load(Me._FullFilePath)
            Else
                Me._XDoc = New XDocument()
                Me._XDoc.Add(New XElement("ConnectionDialog", New XElement("DataSourceSelection")))
            End If

            Me.RootElement = Me._XDoc.Root
        End Sub

        ''' <summary> Gets or sets the root element. </summary>
        ''' <value> The root element. </value>
        Public Property RootElement() As XElement

        ''' <summary> Loads a configuration. </summary>
        ''' <remarks> David, 2020-10-08. </remarks>
        ''' <param name="dialog"> The dialog. </param>
        Public Sub LoadConfiguration(ByVal dialog As DataConnectionDialog)
            dialog.DataSources.Add(DataSource.SqlDataSource)
            dialog.DataSources.Add(DataSource.SqlFileDataSource)
            dialog.DataSources.Add(DataSource.OracleDataSource)
            dialog.DataSources.Add(DataSource.AccessDataSource)
            dialog.DataSources.Add(DataSource.OdbcDataSource)
            dialog.DataSources.Add(SqlCe.SqlCeDataSource)

            dialog.UnspecifiedDataSource.Providers.Add(DataProvider.SqlDataProvider)
            dialog.UnspecifiedDataSource.Providers.Add(DataProvider.OracleDataProvider)
            dialog.UnspecifiedDataSource.Providers.Add(DataProvider.OleDBDataProvider)
            dialog.UnspecifiedDataSource.Providers.Add(DataProvider.OdbcDataProvider)
            dialog.DataSources.Add(dialog.UnspecifiedDataSource)

            Me._DataSources = New Dictionary(Of String, DataSource) From {
                {DataSource.SqlDataSource.Name, DataSource.SqlDataSource},
                {DataSource.SqlFileDataSource.Name, DataSource.SqlFileDataSource},
                {DataSource.OracleDataSource.Name, DataSource.OracleDataSource},
                {DataSource.AccessDataSource.Name, DataSource.AccessDataSource},
                {DataSource.OdbcDataSource.Name, DataSource.OdbcDataSource},
                {SqlCe.SqlCeDataSource.Name, SqlCe.SqlCeDataSource},
                {dialog.UnspecifiedDataSource.DisplayName, dialog.UnspecifiedDataSource}
            }

            Me._DataProviders = New Dictionary(Of String, DataProvider) From {
                {DataProvider.SqlDataProvider.Name, DataProvider.SqlDataProvider},
                {DataProvider.OracleDataProvider.Name, DataProvider.OracleDataProvider},
                {DataProvider.OleDBDataProvider.Name, DataProvider.OleDBDataProvider},
                {DataProvider.OdbcDataProvider.Name, DataProvider.OdbcDataProvider},
                {SqlCe.SqlCeDataProvider.Name, SqlCe.SqlCeDataProvider}
            }


            Dim ds As DataSource = Nothing
            Dim dsName As String = Me.GetSelectedSource()
            If Not [String].IsNullOrEmpty(dsName) AndAlso Me._DataSources.TryGetValue(dsName, ds) Then
                dialog.SelectedDataSource = ds
            End If

            Dim dp As DataProvider = Nothing
            Dim dpName As String = Me.GetSelectedProvider()
            If Not [String].IsNullOrEmpty(dpName) AndAlso Me._DataProviders.TryGetValue(dpName, dp) Then
                dialog.SelectedDataProvider = dp
            End If
        End Sub

        ''' <summary> Saves a configuration. </summary>
        ''' <remarks> David, 2020-10-08. </remarks>
        ''' <param name="dcd"> The dcd. </param>
        Public Sub SaveConfiguration(ByVal dcd As DataConnectionDialog)
            If dcd.SaveSelection Then
                Dim ds As DataSource = dcd.SelectedDataSource
                If ds IsNot Nothing Then
                    If ds Is dcd.UnspecifiedDataSource Then
                        Me.SaveSelectedSource(ds.DisplayName)
                    Else
                        Me.SaveSelectedSource(ds.Name)
                    End If
                End If
                Dim dp As DataProvider = dcd.SelectedDataProvider
                If dp IsNot Nothing Then
                    Me.SaveSelectedProvider(dp.Name)
                End If

                Me._XDoc.Save(Me._FullFilePath)
            End If
        End Sub

        ''' <summary> Gets selected source. </summary>
        ''' <remarks> David, 2020-10-08. </remarks>
        ''' <returns> The selected source. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
        Public Function GetSelectedSource() As String Implements IDataConnectionConfiguration.GetSelectedSource
            Try
                Dim xElem As XElement = Me.RootElement.Element("DataSourceSelection")
                Dim sourceElem As XElement = xElem.Element("SelectedSource")
                If sourceElem IsNot Nothing Then
                    Return TryCast(sourceElem.Value, String)
                End If
            Catch
                Return Nothing
            End Try
            Return Nothing
        End Function

        ''' <summary> Gets selected provider. </summary>
        ''' <remarks> David, 2020-10-08. </remarks>
        ''' <returns> The selected provider. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
        Public Function GetSelectedProvider() As String Implements IDataConnectionConfiguration.GetSelectedProvider
            Try
                Dim xElem As XElement = Me.RootElement.Element("DataSourceSelection")
                Dim providerElem As XElement = xElem.Element("SelectedProvider")
                If providerElem IsNot Nothing Then
                    Return TryCast(providerElem.Value, String)
                End If
            Catch
                Return Nothing
            End Try
            Return Nothing
        End Function

        ''' <summary> Saves a selected source. </summary>
        ''' <remarks> David, 2020-10-08. </remarks>
        ''' <param name="source"> Source for the. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
        Public Sub SaveSelectedSource(ByVal source As String) Implements IDataConnectionConfiguration.SaveSelectedSource
            If Not [String].IsNullOrEmpty(source) Then
                Try
                    Dim xElem As XElement = Me.RootElement.Element("DataSourceSelection")
                    Dim sourceElem As XElement = xElem.Element("SelectedSource")
                    If sourceElem IsNot Nothing Then
                        sourceElem.Value = source
                    Else
                        xElem.Add(New XElement("SelectedSource", source))
                    End If
                Catch
                End Try

            End If
        End Sub

        ''' <summary> Saves a selected provider. </summary>
        ''' <remarks> David, 2020-10-08. </remarks>
        ''' <param name="provider"> The provider. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
        Public Sub SaveSelectedProvider(ByVal provider As String) Implements IDataConnectionConfiguration.SaveSelectedProvider
            If Not [String].IsNullOrEmpty(provider) Then
                Try
                    Dim xElem As XElement = Me.RootElement.Element("DataSourceSelection")
                    Dim sourceElem As XElement = xElem.Element("SelectedProvider")
                    If sourceElem IsNot Nothing Then
                        sourceElem.Value = provider
                    Else
                        xElem.Add(New XElement("SelectedProvider", provider))
                    End If
                Catch
                End Try
            End If
        End Sub
    End Class
End Namespace
