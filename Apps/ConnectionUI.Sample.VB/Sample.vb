'------------------------------------------------------------------------------ 
' <copyright company="Microsoft Corporation"> 
' Copyright (c) Microsoft Corporation. All rights reserved. 
' </copyright> 
'------------------------------------------------------------------------------ 

Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Microsoft.Data.ConnectionUI.VB
    Friend Module Sample
        ' Sample 1: 
#Disable Warning IDE0060 ' Remove unused parameter

        ''' <summary> Main entry-point for this application. </summary>
        ''' <remarks> David, 2020-10-08. </remarks>
        ''' <param name="args"> The arguments. </param>
        <STAThread()>
        Public Sub Main(ByVal args As String())
#Enable Warning IDE0060 ' Remove unused parameter
            Dim connectionString As String = "Data Source=localhost\sqlx2019;integrated security=SSPI;persist security info=False"
            Dim r As (Success As Boolean, ConnectionString As String) = Global.Microsoft.Data.ConnectionUI.Dialog.My.Dialogs.ShowSqlDataSourceConnectionDialog(connectionString)
            If r.Success Then
                ' load tables 
                Using connection As New SqlConnection(r.ConnectionString)
                    connection.Open()
                    Console.WriteLine("system tables:")
                    Dim cmd As New SqlCommand("SELECT * FROM sys.Tables", connection)
                    Using reader As SqlDataReader = cmd.ExecuteReader()
                        While reader.Read()
                            Console.WriteLine(reader(0))
                        End While
                    End Using
                End Using
                Console.Write("done; hit enter to exit >>")
                Console.ReadKey()
            End If
        End Sub

#Disable Warning IDE0060 ' Remove unused parameter

        ''' <summary> Main sample 1. </summary>
        ''' <remarks> David, 2020-10-08. </remarks>
        ''' <param name="args"> The arguments. </param>
        <STAThread()>
        Public Sub MainSample1(ByVal args As String())
#Enable Warning IDE0060 ' Remove unused parameter
            Dim dcd As New DataConnectionDialog()
            Dim dcs As New DataConnectionConfiguration(Nothing)
            dcs.LoadConfiguration(dcd)

            If DataConnectionDialog.Show(dcd) = DialogResult.OK Then
                ' load tables 
                Using connection As New SqlConnection(dcd.ConnectionString)
                    connection.Open()
                    Dim cmd As New SqlCommand("SELECT * FROM sys.Tables", connection)

                    Using reader As SqlDataReader = cmd.ExecuteReader()
                        While reader.Read()
                            Console.WriteLine(reader.HasRows)
                        End While

                    End Using
                End Using
            End If

            dcs.SaveConfiguration(dcd)
        End Sub

#Disable Warning IDE0060 ' Remove unused parameter

        ''' <summary> Main sample 2. </summary>
        ''' <remarks> David, 2020-10-08. </remarks>
        ''' <param name="args"> The arguments. </param>
        <STAThread()>
        Public Sub MainSample2(ByVal args As String())
#Enable Warning IDE0060 ' Remove unused parameter
            Dim dcd As New DataConnectionDialog()
            Dim dcs As New DataConnectionConfiguration(Nothing)
            dcs.LoadConfiguration(dcd)
            dcd.ConnectionString = "Data Source=localhost\sqlx2019;Initial Catalog=tempdb;Persist Security Info=True;User ID=sa;Password=Admin_007"
            If DataConnectionDialog.Show(dcd) = DialogResult.OK Then
                ' load tables 
                Using connection As New SqlConnection(dcd.ConnectionString)
                    connection.Open()
                    Dim cmd As New SqlCommand("SELECT * FROM sys.Tables", connection)

                    Using reader As SqlDataReader = cmd.ExecuteReader()
                        While reader.Read()
                            Console.WriteLine(reader.HasRows)
                        End While

                    End Using
                End Using
            End If

            dcs.SaveConfiguration(dcd)
        End Sub

    End Module
End Namespace
