''' <summary> An OLE database provider. </summary>
''' <remarks>
''' (c) 2018 PIEBALD consulting. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/23/2018, https://www.codeproject.com/Tips/1231609/Enumerating-OleDb-Providers.
''' </para>
''' </remarks>
Public NotInheritable Class OledbProviderInfo

    ''' <summary> Constructor. </summary>
    ''' <remarks>
    ''' See: <para>
    ''' https://msdn.microsoft.com/en-us/library/system.data.oledb.oledbenumerator.getrootenumerator(v=vs.110).aspx
    ''' </para><para>
    '''  0 SOURCES_NAME </para><para>
    '''  1 SOURCES_PARSENAME </para><para>
    '''  2 SOURCES_DESCRIPTION </para><para>
    '''  3 SOURCES_TYPE  One of the following enumeration members: </para><para>
    '''       Binder (0) </para><para>
    '''       DataSource_MDP (1) </para><para>
    '''       DataSource_TDP (2) </para><para>
    '''       Enumerator (3). </para><para>
    '''       These correspond to the values returned in the SOURCES_TYPE column  </para><para>
    '''       of the native OLE DB sources row set. </para><para>
    '''  4 SOURCES_ISPARENT </para><para>
    '''  5 SOURCES_CLSID </para>
    ''' </remarks>
    ''' <param name="record"> The record. </param>
    Public Sub New(ByVal record As System.Data.IDataRecord)
        MyBase.New()
        If record IsNot Nothing Then
            Me.Name = CStr(record("SOURCES_NAME"))
            Me.ParseName = System.Guid.Parse(CStr(record("SOURCES_PARSENAME")))
            Me.ProviderType = CType(record("SOURCES_TYPE"), ProviderType)
            Me.Description = CStr(record("SOURCES_DESCRIPTION"))
            Me.IsParent = CBool(record("SOURCES_ISPARENT"))
            Me.ClassId = System.Guid.Parse(CStr(record("SOURCES_CLSID")))
        End If
    End Sub

    ''' <summary> Build unique identifier. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="oledbProviderInfo"> Information describing the oledb provider. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildUniqueId(ByVal oledbProviderInfo As OledbProviderInfo) As String
        If oledbProviderInfo Is Nothing Then Throw New ArgumentNullException(NameOf(oledbProviderInfo))
        Return $"{oledbProviderInfo.Name}.{CInt(oledbProviderInfo.ProviderType)}"
    End Function

    ''' <summary> Gets a unique identifier. </summary>
    ''' <value> The identifier of the unique. </value>
    Public ReadOnly Property UniqueId() As String
        Get
            Return OledbProviderInfo.BuildUniqueId(Me)
        End Get
    End Property

    ''' <summary> Gets or sets the name. </summary>
    ''' <value> The name. </value>
    Public ReadOnly Property Name() As String

    ''' <summary> Gets or sets the name of the parse. </summary>
    ''' <value> The name of the parse. </value>
    Public ReadOnly Property ParseName() As System.Guid

    ''' <summary> Gets or sets the description. </summary>
    ''' <value> The description. </value>
    Public ReadOnly Property Description() As String

    ''' <summary> Gets or sets the type of the provider. </summary>
    ''' <value> The type of the provider. </value>
    Public ReadOnly Property ProviderType() As ProviderType

    ''' <summary> Gets or sets the is parent. </summary>
    ''' <value> The is parent. </value>
    Public ReadOnly Property IsParent() As Boolean

    ''' <summary> Gets or sets the identifier of the class. </summary>
    ''' <value> The identifier of the class. </value>
    Public ReadOnly Property ClassId() As System.Guid

End Class

''' <summary> Collection of oledb provider informations. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/24/2018 </para>
''' </remarks>
Public Class OledbProviderInfoCollection
    Inherits Collections.ObjectModel.KeyedCollection(Of String, OledbProviderInfo)

    ''' <summary>
    ''' When implemented in a derived class, extracts the key from the specified element.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="item"> The element from which to extract the key. </param>
    ''' <returns> The key for the specified element. </returns>
    Protected Overrides Function GetKeyForItem(item As OledbProviderInfo) As String
        If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
        Return item.UniqueId
    End Function

    ''' <summary> Gets the providers. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> An OledbProviderInfoCollection. </returns>
    Public Shared Function Providers() As OledbProviderInfoCollection
        Dim result As New OledbProviderInfoCollection
        result.EnumerateProviders()
        Return result
    End Function

    ''' <summary> Enumerate providers. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the providers in this collection.
    ''' </returns>
    Public Function EnumerateProviders() As IEnumerable(Of OledbProviderInfo)
        Me.Clear()
        Dim l As New List(Of OledbProviderInfo)
        Using dr As System.Data.IDataReader = System.Data.OleDb.OleDbEnumerator.GetRootEnumerator()
            Do While dr.Read()
                Dim value As OledbProviderInfo = New OledbProviderInfo(dr)
                l.Add(value)
                ' apparently, name + type is not unique. Neither is the class id.
                If Not Me.Contains(value.UniqueId) Then Me.Add(value)
            Loop
        End Using
        Return l
    End Function

End Class

''' <summary> Values that represent provider types. </summary>
''' <remarks> David, 10/8/2020. </remarks>
Public Enum ProviderType

    ''' <summary> An enum constant representing the binder option. </summary>
    Binder = 0

    ''' <summary> An enum constant representing the data source mdp option. </summary>
    DataSourceMdp = 1

    ''' <summary> An enum constant representing the data source tdp option. </summary>
    DataSourceTdp = 2

    ''' <summary> Values that represent rators. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Enumerator = 3
End Enum

