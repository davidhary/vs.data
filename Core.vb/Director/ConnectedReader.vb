''' <summary> Implements a database connection with an open reader. </summary>
''' <remarks>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 10/21/2009, 1.2.3581.x. </para>
''' </remarks>
Public Class ConnectedReader
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ConnectedReader" /> class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="reader">     The reader. </param>
    Public Sub New(ByVal connection As IDbConnection, ByVal reader As IDataReader)
        MyBase.New()
        Me.Connection = connection
        Me.Reader = reader
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    ''' class provided proper implementation.
    ''' </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.CloseConnection()
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

    ''' <summary> Gets or sets the connection. </summary>
    ''' <value> The connection. </value>
    Public ReadOnly Property Connection() As System.Data.IDbConnection

    ''' <summary> Gets or sets the reader. </summary>
    ''' <value> The reader. </value>
    Public ReadOnly Property Reader() As System.Data.IDataReader

    ''' <summary> Closes the reader and the connection. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Sub CloseConnection()

        If Me.Reader IsNot Nothing Then
            Me.Reader.Close()
            Me.Reader.Dispose()
        End If

        If Me.Connection IsNot Nothing Then
            Me.Connection.Close()
            Me.Connection.Dispose()
        End If

    End Sub

End Class
