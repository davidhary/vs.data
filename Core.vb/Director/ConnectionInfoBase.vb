''' <summary> Parses connection strings. </summary>
''' <remarks>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 10/21/2009, 1.2.3581.x. </para>
''' </remarks>
Public MustInherit Class ConnectionInfoBase
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ConnectionInfoBase" /> class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    Protected Sub New(ByVal connectionString As String)
        MyBase.New()
        Me._ConnectionString = connectionString
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="ConnectionInfoBase" /> class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <param name="databaseName">     Name of the database. </param>
    Protected Sub New(ByVal connectionString As String, ByVal databaseName As String)
        Me.New(connectionString)
        Me._DatabaseName = databaseName
    End Sub

#End Region

#Region " CONNECTION INFO "

    ''' <summary> The data source compatibility level. </summary>
    Private _DataSourceCompatibilityLevel As Integer

    ''' <summary> Gets or sets the SQL compatibility level for this entity. </summary>
    ''' <value> The data source compatibility level. </value>
    Public Property DataSourceCompatibilityLevel() As Integer
        Get
            Return Me._DataSourceCompatibilityLevel
        End Get
        Set(value As Integer)
            If Not Me.DataSourceCompatibilityLevel.Equals(value) Then
                Me._DataSourceCompatibilityLevel = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The connection string. </summary>
    Private _ConnectionString As String

    ''' <summary> Gets or sets the connection string. </summary>
    ''' <value> The connection string. </value>
    Public Property ConnectionString() As String
        Get
            Return Me._ConnectionString
        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrWhiteSpace(value) Then
                Me.ParseConnectionString(value)
            End If
            Me._ConnectionString = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Information describing the database file. </summary>
    Private _DatabaseFileInfo As System.IO.FileInfo

    ''' <summary> Gets or sets (protected) the database file information. </summary>
    ''' <value> Information describing the database file. </value>
    Public Property DatabaseFileInfo() As System.IO.FileInfo
        Get
            Return Me._DatabaseFileInfo
        End Get
        Protected Set(value As System.IO.FileInfo)
            If value Is Nothing AndAlso Me.DatabaseFileInfo IsNot Nothing Then
                Me._DatabaseFileInfo = value
                Me.NotifyPropertyChanged()
            ElseIf value IsNot Nothing AndAlso Not value.Equals(Me.DatabaseFileInfo) Then
                Me._DatabaseFileInfo = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Name of the database. </summary>
    Private _DatabaseName As String

    ''' <summary> Gets or sets the database name. </summary>
    ''' <value> The name of the database. </value>
    Public Property DatabaseName() As String
        Get
            Return Me._DatabaseName
        End Get
        Protected Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.DatabaseName) Then
                Me._DatabaseName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Name of the host. </summary>
    Private _HostName As String

    ''' <summary> Gets or sets the database server Host name. </summary>
    ''' <value> The name of the Host. </value>
    Public Property HostName() As String
        Get
            Return Me._HostName
        End Get
        Protected Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.HostName) Then
                Me._HostName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Name of the instance. </summary>
    Private _InstanceName As String '

    ''' <summary> Gets or sets the database server instance name. </summary>
    ''' <value> The name of the instance. </value>
    Public Property InstanceName() As String
        Get
            Return Me._InstanceName
        End Get
        Protected Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.InstanceName) Then
                Me._InstanceName = value
                Me.NotifyPropertyChanged()
            End If

        End Set
    End Property

    ''' <summary> Name of the server. </summary>
    Private _ServerName As String

    ''' <summary> Gets or sets the database server name. </summary>
    ''' <value> The name of the server. </value>
    Public Property ServerName() As String
        Get
            Return Me._ServerName
        End Get
        Protected Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.ServerName) Then
                Me._ServerName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The credentials. </summary>
    Private _Credentials As SqlClient.SqlCredential

    ''' <summary> Gets or sets the credentials. </summary>
    ''' <value> The credentials. </value>
    Public Property Credentials As SqlClient.SqlCredential
        Get
            Return Me._Credentials
        End Get
        Set(value As SqlClient.SqlCredential)
            If value IsNot Nothing AndAlso
                Not (String.Equals(value?.UserId, Me.Credentials?.UserId) AndAlso
                     Security.SecureString.Equals(value?.Password, Me.Credentials?.Password)) Then
                Me._Credentials = New SqlClient.SqlCredential(value.UserId, value.Password)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " PARSERS "

    ''' <summary>
    ''' Parses the connection string getting the server, instance,and database names.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    Public MustOverride Sub ParseConnectionString(ByVal connectionString As String)

#End Region

End Class

