Imports System.Runtime.CompilerServices

Namespace ConnectionExtensions

    ''' <summary> Includes extensions for <see cref="IDbConnection">Connection</see>. </summary>
    ''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

#Region " CONNECTION STATE "

        ''' <summary> Returns true if 'connection' is closed. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The connection. </param>
        ''' <returns> <c>true</c> if closed; otherwise <c>false</c> </returns>
        <Extension>
        Public Function IsClosed(ByVal connection As IDbConnection) As Boolean
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Return connection.State = ConnectionState.Closed
        End Function

        ''' <summary> Returns true if 'connection' is open. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The connection. </param>
        ''' <returns> <c>true</c> if open; otherwise <c>false</c> </returns>
        <Extension>
        Public Function IsOpen(ByVal connection As IDbConnection) As Boolean
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Return (connection.State And ConnectionState.Open) = ConnectionState.Open
        End Function

#End Region

    End Module

End Namespace
