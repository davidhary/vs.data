Imports System.Data.Common

Imports isr.Core
Imports isr.Data.Core.ExceptionExtensions

''' <summary> Base data manager class. </summary>
''' <remarks>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 10/21/2009, 1.2.3581.x. </para>
''' </remarks>
Public MustInherit Class DatabaseManagerBase
    Inherits ConnectionInfoBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DatabaseManagerBase" /> class.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    Protected Sub New(ByVal connectionString As String)
        MyBase.New(connectionString)
        Me._CurrentReleaseVersion = String.Empty
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DatabaseManagerBase" /> class.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <param name="databaseName">     Name of the database. </param>
    Protected Sub New(ByVal connectionString As String, ByVal databaseName As String)
        MyBase.New(connectionString, databaseName)
        Me._CurrentReleaseVersion = String.Empty
    End Sub

#End Region

#Region " PRESET "

    ''' <summary> Initializes the known state. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public MustOverride Sub InitializeKnownState()

#End Region

#Region " CONNECTION MANAGER "

    ''' <summary> True if last connection failed. </summary>
    Private _LastConnectionFailed As Boolean

    ''' <summary> Gets or sets the outcome of last connection attempt. </summary>
    ''' <value> The last connection failed. </value>
    Public Property LastConnectionFailed() As Boolean
        Get
            Return Me._LastConnectionFailed
        End Get
        Set(value As Boolean)
            If Not Me.LastConnectionFailed.Equals(value) Then
                Me._LastConnectionFailed = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Pings the database host. With file-based databases, such as SQL Compact, this tests if the
    ''' database file exists.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> <c>True</c> if the server was located; Otherwise, <c>False</c>. </returns>
    Public MustOverride Function PingHost(ByVal timeout As TimeSpan) As Boolean

    ''' <summary> tries to open a connection. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> A Tuple(Of TraceEventType, String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryOpen(ByVal connection As IDbConnection) As Tuple(Of TraceEventType, String)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim activity As String = String.Empty
        Dim item1 As TraceEventType = TraceEventType.Verbose
        Dim item2 As String = String.Empty
        Try
            If String.IsNullOrWhiteSpace(connection.ConnectionString) Then
                item1 = TraceEventType.Warning
                item2 = "Connection string not specified"
            Else
                Dim wasOpen As Boolean = ConnectionState.Open = (connection.State And ConnectionState.Open)
                If wasOpen Then
                    item2 = "Connection was open"
                Else
                    activity = $"opening {connection.ConnectionString}"
                    connection.Open()
                    If connection.State = ConnectionState.Closed Then
                        item1 = TraceEventType.Warning
                        item2 = $"Connection {connection.ConnectionString} failed to open"
                    End If
                End If
            End If
        Catch ex As Exception
            item1 = TraceEventType.Error
            item2 = $"Exception {activity};. {ex.ToFullBlownString}"
        Finally
        End Try
        Return New Tuple(Of TraceEventType, String)(item1, item2)
    End Function

    ''' <summary>
    ''' Returns true if able to connect to the database using the specified connection string.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString">   Specifies the connection string to connect to. </param>
    ''' <param name="keepConnectionOpen"> True to keep connection open. </param>
    ''' <param name="e">                  Cancel details event information. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function TryConnect(ByVal connectionString As String, ByVal keepConnectionOpen As Boolean, ByVal e As ActionEventArgs) As Boolean
        Dim r As (Success As Boolean, Details As String) = Me.TryConnect(connectionString, keepConnectionOpen)
        If Not r.Success Then
            e.RegisterFailure(r.Details)
        End If
        Return Not e.Failed
    End Function

    ''' <summary>
    ''' Returns true if able to connect to the database using the specified connection string.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="keepConnectionOpen"> True to keep connection open. </param>
    ''' <param name="e">                  Cancel details event information. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function TryConnect(ByVal keepConnectionOpen As Boolean, ByVal e As ActionEventArgs) As Boolean
        Return Me.TryConnect(Me.ConnectionString, keepConnectionOpen, e)
    End Function

    ''' <summary>
    ''' Returns true if able to connect to the database using the specified connection string.
    ''' </summary>
    ''' <remarks> David, 3/16/2020. </remarks>
    ''' <param name="connectionString">   Specifies the connection string to connect to. </param>
    ''' <param name="keepConnectionOpen"> True to keep connection open. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public MustOverride Function TryConnect(ByVal connectionString As String, ByVal keepConnectionOpen As Boolean) As (Success As Boolean, Details As String)

    ''' <summary>
    ''' Returns true if able to connect to the database using the specified connection string.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="keepConnectionOpen"> True to keep connection open. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    Public Function TryConnect(ByVal keepConnectionOpen As Boolean) As (Success As Boolean, Details As String)
        Return Me.TryConnect(Me.ConnectionString, keepConnectionOpen)
    End Function

    ''' <summary> Query if 'connection' is closed. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> <c>true</c> if closed; otherwise <c>false</c> </returns>
    Public Shared Function IsClosed(ByVal connection As IDbConnection) As Boolean
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return connection.State = ConnectionState.Closed
    End Function

    ''' <summary> Query if 'connection' is open. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <returns> True if open, false if not. </returns>
    Public Shared Function IsOpen(ByVal connection As IDbConnection) As Boolean
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Return (connection.State And ConnectionState.Open) = ConnectionState.Open
    End Function

#End Region

#Region " COMMAND "

    ''' <summary> Executes the command operation. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the database connection string. </param>
    ''' <param name="commandText">      The command text. </param>
    ''' <returns> The number of rows affected. </returns>
    Public MustOverride Function ExecuteCommand(ByVal connectionString As String, ByVal commandText As String) As Integer

#End Region

#Region " QUERY "

    ''' <summary> Executes the specified query command and returns a 64-bit signed value. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the connection string. </param>
    ''' <param name="queryCommand">     Specifies a query. </param>
    ''' <returns> The 64-bit signed value of the field. </returns>
    Public Function QueryInt64(ByVal connectionString As String, ByVal queryCommand As String) As Int64
        Return Me.ExecuteReader(connectionString, queryCommand).Reader.GetInt64(0)
    End Function

    ''' <summary> Executes the specified query command and returns a long integer. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="queryCommand"> Specifies a query. </param>
    ''' <returns> The 64-bit signed value of the field. </returns>
    Public Function QueryInt64(ByVal queryCommand As String) As Int64
        Return Me.QueryInt64(Me.ConnectionString, queryCommand)
    End Function

    ''' <summary> Reads a string type field from the database using the specified query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the connection string. </param>
    ''' <param name="queryCommand">     Specifies a query. </param>
    ''' <param name="columnIndex">      Specifies the index where the returned value resides. </param>
    ''' <returns> The string value of the specified field. </returns>
    Public Function QueryString(ByVal connectionString As String, ByVal queryCommand As String, ByVal columnIndex As Integer) As String

        Using connReader As ConnectedReader = Me.ExecuteReader(connectionString, queryCommand)
            Return If(connReader.Reader.Read, connReader.Reader.GetString(columnIndex), String.Empty)
        End Using

    End Function

    ''' <summary> Reads a string type field from the database using the specified query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the connection string. </param>
    ''' <param name="queryCommand">     Specifies a query. </param>
    ''' <param name="columnName">       Specifies the column name where the returned value resides. </param>
    ''' <returns> The string value of the specified field. </returns>
    Public Function QueryString(ByVal connectionString As String, ByVal queryCommand As String, ByVal columnName As String) As String

        Using connReader As ConnectedReader = Me.ExecuteReader(connectionString, queryCommand)
            If connReader.Reader.Read Then
                Dim columnIndex As Integer = connReader.Reader.GetOrdinal(columnName)
                Return If(columnIndex >= 0, connReader.Reader.GetString(columnIndex), String.Empty)
            Else
                Return String.Empty
            End If
        End Using

    End Function

#End Region

#Region " PROVIDERS "

    ''' <summary> Gets the installed providers. </summary>
    ''' <value> The installed providers. </value>
    Public Shared ReadOnly Property InstalledProviders() As DataTable
        Get
            Return DbProviderFactories.GetFactoryClasses()
        End Get
    End Property

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="table"> The table. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Shared Function ToString(ByVal table As DataTable) As String
        If table Is Nothing Then Throw New ArgumentNullException(NameOf(table))
        Dim builder As New System.Text.StringBuilder
        For Each column As DataColumn In table.Columns
            builder.Append($"{If(builder.Length > 0, ",", "")}{column.ColumnName}")
        Next
        For Each row As DataRow In table.Rows
            Dim colBuilder As New System.Text.StringBuilder
            For Each column As DataColumn In table.Columns
                colBuilder.Append($"{If(colBuilder.Length > 0, ",", "")}{row(column)}")
            Next
            builder.AppendLine(colBuilder.ToString)
        Next
        Return builder.ToString
    End Function

#End Region

#Region " READER "

    ''' <summary>
    ''' Executes the specified query command and returns reference to the
    ''' <see cref="ConnectedReader">connected reader</see>
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the connection string. </param>
    ''' <param name="queryCommand">     Specifies a query. </param>
    ''' <returns> An instance of the <see cref="ConnectedReader">reader</see>. </returns>
    Public MustOverride Function ExecuteReader(ByVal connectionString As String, ByVal queryCommand As String) As ConnectedReader

    ''' <summary>
    ''' Executes the specified query command and returns reference to the
    ''' <see cref="ConnectedReader">connected reader</see>
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="queryCommand"> Specifies a query. </param>
    ''' <returns> An instance of the <see cref="ConnectedReader">reader</see>. </returns>
    Public Function ExecuteReader(ByVal queryCommand As String) As ConnectedReader
        Return Me.ExecuteReader(Me.ConnectionString, queryCommand)
    End Function

#End Region

#Region " TABLES "

    ''' <summary> Returns true if a record exists satisfying the query command. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the connection string. </param>
    ''' <param name="queryCommand">     Specifies a query. </param>
    ''' <returns>
    ''' <c>True</c> if a record exists satisfying the query command; Otherwise, <c>False</c>.
    ''' </returns>
    Public Function RecordExists(ByVal connectionString As String, ByVal queryCommand As String) As Boolean

        Using connReader As ConnectedReader = Me.ExecuteReader(connectionString, queryCommand)
            Return connReader.Reader.Read
        End Using

    End Function

    ''' <summary> Returns true if the specified table exists. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the connection string. </param>
    ''' <param name="value">            Specifies the table name. </param>
    ''' <returns> <c>True</c> if a table exists; Otherwise, <c>False</c>. </returns>
    Public Function TableExists(ByVal connectionString As String, ByVal value As String) As Boolean

        Dim queryCommand As String = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{0}'"
        queryCommand = String.Format(Globalization.CultureInfo.CurrentCulture, queryCommand, value)
        Return Me.RecordExists(connectionString, queryCommand)

    End Function

    ''' <summary> Returns true if the specified table exists. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="value"> Specifies the table name. </param>
    ''' <returns> <c>True</c> if a table exists; Otherwise, <c>False</c>. </returns>
    Public Function TableExists(ByVal value As String) As Boolean
        Return Me.TableExists(Me.ConnectionString, value)
    End Function

    ''' <summary> Gets the number of distinct SQL commands defined in the specified query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="query"> Specifies the query or file name where the query is located. A file name
    '''                      begins with '@'. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function DeriveCommandCount(ByVal query As String) As Integer
        Return isr.Data.Core.DatabaseManagerBase.PrepareCommandQuery(query).Length
    End Function

    ''' <summary>
    ''' Prepares a query for processing as a connection command. If preceded with '@' the query is
    ''' retrieved form file. Multiple commands must be delimited with a ';' and CR or ';' and CR-LF.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="query"> Specifies a query string or a file name. </param>
    ''' <returns> A String(). </returns>
    Public Shared Function PrepareCommandQuery(ByVal query As String) As String()

        If String.IsNullOrWhiteSpace(query) OrElse query.Length < 2 Then

            Return New String() {""}

        Else

            ' check if using file or query
            If query.Substring(0, 1) = "@" Then
                ' if the is a file name then get the file name
                Dim filePath As String = query.Substring(1)
                query = My.Computer.FileSystem.ReadAllText(filePath)
            End If

            ' split queries to lines
            Dim lines As String()
            lines = query.Split(Environment.NewLine.ToCharArray, StringSplitOptions.RemoveEmptyEntries)
            Dim commandList As New List(Of String)
            Dim commandLine As New System.Text.StringBuilder
            For Each line As String In lines
                If commandLine.Length > 0 Then
                    commandLine.AppendLine()
                End If
                commandLine.Append(line)
                If line.Trim.EndsWith(";", StringComparison.OrdinalIgnoreCase) OrElse
                   line.Trim.EndsWith("GO", StringComparison.OrdinalIgnoreCase) Then
                    commandList.Add(commandLine.ToString)
                    commandLine = New System.Text.StringBuilder
                End If
            Next
            Return commandList.ToArray

        End If

    End Function

    ''' <summary> Number of query errors. </summary>
    Private _QueryErrorCount As Integer

    ''' <summary> Gets or sets (protected) the number of update queries that failed. </summary>
    ''' <value> The number of query errors. </value>
    Public Property QueryErrorCount() As Integer
        Get
            Return Me._QueryErrorCount
        End Get
        Protected Set(value As Integer)
            Me._QueryErrorCount = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Number of query updates. </summary>
    Private _QueryUpdateCount As Integer

    ''' <summary> Gets or sets (protected) the number of update query that worked. </summary>
    ''' <value> The number of query updates. </value>
    Public Property QueryUpdateCount() As Integer
        Get
            Return Me._QueryUpdateCount
        End Get
        Protected Set(value As Integer)
            Me._QueryUpdateCount = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString">  Specifies the database connection string. </param>
    ''' <param name="query">             Specifies the query or file name where the query is located.
    '''                                  A file name begins with '@'. </param>
    ''' <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
    '''                                  command. </param>
    ''' <returns>
    ''' The total number of minor errors minus the number of successful command executions.
    ''' </returns>
    Public MustOverride Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String, ByVal ignoreMinorErrors As Boolean) As Integer

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString">  Specifies the database connection string. </param>
    ''' <param name="query">             Specifies the query lines. </param>
    ''' <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
    '''                                  command. </param>
    ''' <returns>
    ''' The total number of minor errors minus the number of successful command executions.
    ''' </returns>
    Public MustOverride Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String(), ByVal ignoreMinorErrors As Boolean) As Integer

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="query"> Specifies the query or file name where the query is located. A file name
    '''                      begins with '@'. </param>
    ''' <returns>
    ''' The total number of minor errors minus the number of successful command executions.
    ''' </returns>
    Public Function UpdateTableQuery(ByVal query As String) As Integer
        Return Me.UpdateTableQuery(Me.ConnectionString, query)
    End Function

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="query"> Specifies the query lines. </param>
    ''' <returns>
    ''' The total number of minor errors minus the number of successful command executions.
    ''' </returns>
    Public Function UpdateTableQuery(ByVal query As String()) As Integer
        Return Me.UpdateTableQuery(Me.ConnectionString, query)
    End Function

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the database connection string. </param>
    ''' <param name="query">            Specifies the query of file name where the query is located.
    '''                                 A file name begins with '@'. </param>
    ''' <returns>
    ''' The total number of minor errors minus the number of successful command executions.
    ''' </returns>
    Public Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String) As Integer
        Dim recordCount As Int32
        If Not String.IsNullOrWhiteSpace(query) Then
            recordCount = Me.UpdateTableQuery(connectionString, query, False)
        End If
        Return recordCount
    End Function

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the database connection string. </param>
    ''' <param name="query">            Specifies the query lines. </param>
    ''' <returns> The number of records affected by the query. </returns>
    Public Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String()) As Integer
        Dim recordCount As Int32
        If query IsNot Nothing Then
            recordCount = Me.UpdateTableQuery(connectionString, query, False)
        End If
        Return recordCount
    End Function

#End Region

#Region " VERIFICATION "

    ''' <summary> Gets or sets the sentinel indicating a failed release. </summary>
    ''' <value> The failed release. </value>
    Protected ReadOnly Property FailedRelease As String = "<failed>"

    ''' <summary> The current release version. </summary>
    Private _CurrentReleaseVersion As String

    ''' <summary>
    ''' Gets or sets (protected) the current version. Reads from the database if the stored version
    ''' is null or fails reading the version or last verification failed.
    ''' </summary>
    ''' <value> The current release version. </value>
    Public Property CurrentReleaseVersion() As String
        Get
            Return Me._CurrentReleaseVersion
        End Get
        Protected Set(value As String)
            Me._CurrentReleaseVersion = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The current release version query text. </summary>
    Private _CurrentReleaseVersionQueryText As String

    ''' <summary> Gets or sets (protected) the text used to query the current release. </summary>
    ''' <value> The current release version query text. </value>
    Public Property CurrentReleaseVersionQueryText() As String
        Get
            Return Me._CurrentReleaseVersionQueryText
        End Get
        Set(value As String)
            Me._CurrentReleaseVersionQueryText = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> True if database verified. </summary>
    Private _DatabaseVerified As Boolean

    ''' <summary>
    ''' Gets or sets (protected) the sentinel indicating if the database was successfully verified.
    ''' </summary>
    ''' <value> The database verified. </value>
    Public Property DatabaseVerified() As Boolean
        Get
            Return Me._DatabaseVerified
        End Get
        Protected Set(value As Boolean)
            Me._DatabaseVerified = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Queries the current version. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the connection string. </param>
    ''' <returns> The current release version. </returns>
    Public Function QueryCurrentReleaseVersion(ByVal connectionString As String) As String
        Me.CurrentReleaseVersion = Me.QueryString(connectionString, Me.CurrentReleaseVersionQueryText, 0)
        Return Me.CurrentReleaseVersion
    End Function

    ''' <summary> Verifies that the local database is connectible and is current. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connectionString"> Specifies the database connection string. </param>
    ''' <param name="version">          Specifies the database version. </param>
    ''' <param name="e">                Action event information. </param>
    ''' <returns> <c>True</c> if verified; Otherwise, <c>False</c>. </returns>
    Public Function VerifyDatabaseVersion(ByVal connectionString As String, ByVal version As String, ByVal e As ActionEventArgs) As Boolean

        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        ' clear the verification status.
        Me.DatabaseVerified = False

        ' set default properties.
        If Me.TryConnect(connectionString, True, e) Then
            Me.QueryCurrentReleaseVersion(connectionString)
            Me.DatabaseVerified = System.Version.Equals(New System.Version(Me.CurrentReleaseVersion), New System.Version(version))
            If Not Me.DatabaseVerified Then
                e.RegisterFailure($"Local database version mismatch;. Expected version '{version}' and found '{Me.CurrentReleaseVersion}'")
            End If
        End If
        Return Me.DatabaseVerified

    End Function

#End Region

#Region " SHARED COMMAND "

    ''' <summary> Executes the command operation. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="dataConnection"> The data connection. </param>
    ''' <param name="commandText">    The command text. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function ExecuteCommand(ByVal dataConnection As IDbConnection, ByVal commandText As String) As Integer
        If dataConnection Is Nothing Then Throw New ArgumentNullException(NameOf(dataConnection))
        If String.IsNullOrEmpty(commandText) Then Return 0
        Dim command As IDbCommand = dataConnection.CreateCommand()
        command.CommandText = commandText
        If dataConnection.State <> ConnectionState.Open Then dataConnection.Open()
        Dim rowCount As Integer = command.ExecuteNonQuery()
        Return rowCount
    End Function

    ''' <summary> Executes the scalar operation. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="dataConnection"> The data connection. </param>
    ''' <param name="commandText">    The command text. </param>
    ''' <returns> An Object. </returns>
    Public Shared Function ExecuteScalar(ByVal dataConnection As IDbConnection, ByVal commandText As String) As Object
        If dataConnection Is Nothing Then Throw New ArgumentNullException(NameOf(dataConnection))
        If String.IsNullOrEmpty(commandText) Then Return 0
        Dim command As IDbCommand = dataConnection.CreateCommand()
        command.CommandText = commandText
        If dataConnection.State <> ConnectionState.Open Then dataConnection.Open()
        Return command.ExecuteScalar
    End Function

#End Region

#Region " SQL TIME RANGE "

    ''' <summary> Gets the SQL data time range. </summary>
    ''' <value> The SQL data time range. </value>
    Public Shared ReadOnly Property SqlDateTimeRange As isr.Core.Constructs.RangeDateTime
        Get
            Return New isr.Core.Constructs.RangeDateTime(SqlTypes.SqlDateTime.MinValue.Value, SqlTypes.SqlDateTime.MaxValue.Value)
        End Get
    End Property

#End Region

End Class

