Imports isr.Data.Core.ExceptionExtensions

''' <summary> Implements a database manager entity for SQL Compact Edition. </summary>
''' <remarks>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 10/21/2009, 1.2.3581.x. </para>
''' </remarks>
Public Class DatabaseManager
    Inherits DatabaseManagerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DatabaseManagerBase" /> class.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    Public Sub New(ByVal connectionString As String)
        MyBase.New(connectionString)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DatabaseManagerBase" /> class.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <param name="databaseName">     Name of the database. </param>
    Public Sub New(ByVal connectionString As String, ByVal databaseName As String)
        MyBase.New(connectionString, databaseName)
    End Sub

#End Region

#Region " PRESET "

    ''' <summary> Initializes the known state. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Overrides Sub InitializeKnownState()
        Me.CurrentReleaseVersionQueryText = "SELECT ReleaseRevision, IsCurrentRelease FROM ReleaseHistory WHERE (IsCurrentRelease = 1)"
    End Sub

#End Region

#Region " CONNECTION INFO "

#Region " BUILDERS "

    ''' <summary> A pattern specifying the connection string. </summary>
    ''' <value> The connection string pattern. </value>
    Protected Shared Property ConnectionStringPattern As String = "data source={0};initial catalog={1};integrated security=SSPI;persist security info=False"

    ''' <summary>
    ''' Returns a connection string to an SQL catalog on the specified local server instance.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="pattern">      Specifies the connection string pattern. </param>
    ''' <param name="instanceName"> Specifies the local server instance name. </param>
    ''' <param name="databaseName"> Specifies the database name. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildLocalConnectionString(ByVal pattern As String, ByVal instanceName As String, ByVal databaseName As String) As String

        If String.IsNullOrWhiteSpace(pattern) Then Throw New ArgumentNullException(NameOf(pattern))
        If String.IsNullOrWhiteSpace(databaseName) Then Throw New ArgumentNullException(NameOf(databaseName))
        If instanceName Is Nothing Then instanceName = String.Empty
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             pattern, DatabaseManager.BuildLocalServerInstanceName(instanceName), databaseName)

    End Function

    ''' <summary> Gets or sets the user connection string pattern. </summary>
    ''' <value> The user connection string pattern. </value>
    Protected Shared Property UserConnectionStringPattern As String = "Data Source={0};Initial Catalog={1};User ID={2};Password={3}"

    ''' <summary>
    ''' Returns a connection string to an SQL catalog on the specified local server instance.
    ''' </summary>
    ''' <remarks> David, 3/21/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="pattern">      Specifies the connection string pattern. </param>
    ''' <param name="instanceName"> Specifies the local server instance name. </param>
    ''' <param name="databaseName"> Specifies the database name. </param>
    ''' <param name="userName">     Name of the user. </param>
    ''' <param name="password">     The password secure string. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildLocalConnectionString(ByVal pattern As String, ByVal instanceName As String, ByVal databaseName As String,
                                                      ByVal userName As String, ByVal password As Security.SecureString) As String

        If String.IsNullOrWhiteSpace(pattern) Then Throw New ArgumentNullException(NameOf(pattern))
        If String.IsNullOrWhiteSpace(databaseName) Then Throw New ArgumentNullException(NameOf(databaseName))
        If String.IsNullOrWhiteSpace(userName) Then Throw New ArgumentNullException(NameOf(userName))
        If password Is Nothing Then Throw New ArgumentNullException(NameOf(password))
        If instanceName Is Nothing Then instanceName = String.Empty
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             pattern, DatabaseManager.BuildLocalServerInstanceName(instanceName), databaseName, userName, password)

    End Function

    ''' <summary>
    ''' Returns a server name in the format 'Machine Name' or 'Machine Name\Instance name',
    ''' e.g., "ABC\SQLEXPRESS".
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="instanceName"> Specifies the local server instance name. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildLocalServerInstanceName(ByVal instanceName As String) As String

        If instanceName Is Nothing Then
            instanceName = String.Empty
        End If

        Dim dataSourcePattern As String = "{0}\{1}"

        If String.IsNullOrWhiteSpace(instanceName) Then
            Return "LocalHost" '  Environment.MachineName
        Else
            Return String.Format(Globalization.CultureInfo.CurrentCulture,
                        dataSourcePattern, Environment.MachineName, instanceName)
        End If

    End Function

    ''' <summary>
    ''' Returns a connection string to an SQL catalog on the specified local server instance.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="instanceName"> Specifies the local server instance name. </param>
    ''' <param name="databaseName"> Specifies the database name. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildLocalConnectionString(ByVal instanceName As String, ByVal databaseName As String) As String

        If instanceName Is Nothing Then
            instanceName = String.Empty
        End If

        If String.IsNullOrWhiteSpace(databaseName) Then
            Throw New ArgumentNullException(NameOf(databaseName))
        End If

        Return DatabaseManager.BuildLocalConnectionString(DatabaseManager.ConnectionStringPattern, instanceName, databaseName)

    End Function

#End Region

#Region " PARSERS "

    ''' <summary>
    ''' Parses the connection string getting the server, instance,and database names.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="connectionString"> The connection string. </param>
    Public Overrides Sub ParseConnectionString(ByVal connectionString As String)

        If String.IsNullOrWhiteSpace(connectionString) Then Throw New ArgumentNullException(NameOf(connectionString))

        ' data source=MUSCAT\SQLEXPRESS;initial catalog=Provers70;persist security info=False;User id=bank;Password=syncWithMe;
        If String.IsNullOrWhiteSpace(connectionString) Then Throw New ArgumentNullException(NameOf(connectionString))

        Dim syntaxParts As New Queue(Of String)(connectionString.Split(";"c))

        If syntaxParts.Count < 2 Then Throw New ArgumentException($"Invalid connection string '{connectionString}'.", NameOf(connectionString))

        Me.DataSourceCompatibilityLevel = 2

        Dim dataSources As New Queue(Of String)(syntaxParts.Dequeue.Split("="c))
        If dataSources.Count < 2 Then Throw New ArgumentException($"Invalid connection string data sources '{connectionString}'.", NameOf(connectionString))
        Dim empty As String = dataSources.Dequeue

        Me.ServerName = dataSources.Dequeue
        Dim serverElements As New Queue(Of String)(Me.ServerName.Split("\"c))
        If serverElements.Any Then Me.HostName = serverElements.Dequeue
        If serverElements.Any Then Me.InstanceName = serverElements.Dequeue

        Dim elements As New Queue(Of String)(syntaxParts.Dequeue.Split("="c))
        empty = elements.Dequeue
        If elements.Any Then Me.DatabaseName = elements.Dequeue

        Dim userId As String = String.Empty
        Dim candidate As String
        Dim ss As New Security.SecureString()
        Do While syntaxParts.Any
            candidate = syntaxParts.Dequeue
            If candidate.StartsWith("user id", StringComparison.CurrentCultureIgnoreCase) Then
                userId = candidate.Split("="c).Last
            ElseIf candidate.StartsWith("password", StringComparison.InvariantCultureIgnoreCase) Then
                For Each cc As Char In candidate.Split("="c).Last.ToCharArray
                    ss.AppendChar(cc)
                Next
                ss.MakeReadOnly()
            End If
        Loop
        If Not (String.IsNullOrEmpty(userId) OrElse String.IsNullOrEmpty(ss.ToString)) Then
            Me.Credentials = New SqlClient.SqlCredential(userId, ss)
        End If

        ' this requires SQL Management Objects
        Me.DatabaseFileInfo = Nothing

    End Sub

#End Region

#End Region

#Region " CONNECTION "

    ''' <summary> Pings the database server. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> <c>True</c> if the server was located; Otherwise, <c>False</c>. </returns>
    Public Overrides Function PingHost(ByVal timeout As TimeSpan) As Boolean
        Return My.Computer.Network.Ping(Me.ServerName, CInt(timeout.TotalMilliseconds))
    End Function

    ''' <summary> Creates the connection. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> The new connection. </returns>
    Public Shared Function CreateConnection() As SqlClient.SqlConnection
        Dim conn As System.Data.SqlClient.SqlConnection = Nothing
        Dim temp As System.Data.SqlClient.SqlConnection = Nothing
        Try
            temp = New System.Data.SqlClient.SqlConnection()
            conn = temp
            temp = Nothing
        Catch
            Throw
        Finally
            temp?.Dispose()
        End Try
        Return conn
    End Function

    ''' <summary> Opens a connection. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <returns> A SqlClient.SqlConnection. </returns>
    Public Shared Function OpenConnection(ByVal connectionString As String) As SqlClient.SqlConnection
        If String.IsNullOrWhiteSpace(connectionString) Then Throw New ArgumentNullException(NameOf(connectionString))
        Dim conn As System.Data.SqlClient.SqlConnection = Nothing
        Dim temp As System.Data.SqlClient.SqlConnection = Nothing
        Try
            temp = DatabaseManager.CreateConnection
            temp.ConnectionString = connectionString
            temp.Open()
            conn = temp
            temp = Nothing
        Catch
            Throw
        Finally
            temp?.Dispose()
        End Try
        Return conn

    End Function

    ''' <summary>
    ''' Returns true if able to connect to the database using the specified connection string.
    ''' </summary>
    ''' <remarks> David, 3/16/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connectionString">   Specifies the connection string to connect to. </param>
    ''' <param name="keepConnectionOpen"> True to keep connection open. </param>
    ''' <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overrides Function TryConnect(ByVal connectionString As String, ByVal keepConnectionOpen As Boolean) As (Success As Boolean, Details As String)
        If String.IsNullOrWhiteSpace(connectionString) Then Throw New ArgumentNullException(NameOf(connectionString))
        Dim activity As String = $"opening {connectionString}"
        Dim conn As System.Data.SqlClient.SqlConnection = Nothing
        Dim outcome As (success As Boolean, details As String) = (True, String.Empty)
        Try
            conn = DatabaseManager.OpenConnection(connectionString)
            MyBase.LastConnectionFailed = DatabaseManager.IsClosed(conn)
            If Me.LastConnectionFailed Then
                outcome = (Not Me.LastConnectionFailed, "Connection failed to open")
            End If
        Catch ex As Exception
            conn?.Dispose()
            conn = Nothing
            MyBase.LastConnectionFailed = True
            outcome = (Not Me.LastConnectionFailed, $"Exception {activity};. {ex.ToFullBlownString}")
        Finally
            If Not keepConnectionOpen Then
                ' apparently, close also disposes. otherwise, uncomment this:
                ' If not DatabaseManager.IsClosed(connection) Then conn.Close()
                conn?.Dispose()
            End If
        End Try
        Return outcome

    End Function

#End Region

#Region " COMMAND "

    ''' <summary> Executes the specified command and returns the number of rows affected. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the connection string. </param>
    ''' <param name="commandText">      Specifies a command. </param>
    ''' <returns> The number of rows affected. </returns>
    Public Overrides Function ExecuteCommand(ByVal connectionString As String, ByVal commandText As String) As Integer

        Dim rowCount As Integer = 0
        Using connection As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(connectionString)
            Dim sqlCommand As System.Data.SqlClient.SqlCommand = connection.CreateCommand()
            sqlCommand.CommandText = commandText
            connection.Open()
            rowCount = sqlCommand.ExecuteNonQuery()
            ' apparently close also disposes. Otherwise, include: connection.Close()
        End Using
        Return rowCount

    End Function

#End Region

#Region " DATABASE "

    ''' <summary> Connects to the specified database on the specified local server instance. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="instanceName"> Specifies the server instance name, such as 'SQLEXPRESS'. </param>
    ''' <param name="databaseName"> Name of the database. </param>
    ''' <returns> A SqlClient.SqlConnection. </returns>
    Public Shared Function ConnectLocalDatabase(ByVal instanceName As String, ByVal databaseName As String) As SqlClient.SqlConnection

        If instanceName Is Nothing Then
            instanceName = String.Empty
        End If

        If String.IsNullOrWhiteSpace(databaseName) Then
            Throw New ArgumentNullException(NameOf(databaseName))
        End If

        Dim conn As System.Data.SqlClient.SqlConnection = Nothing

        ' Get the connection string to the master catalog
        Dim connString As String = DatabaseManager.BuildLocalConnectionString(instanceName, databaseName)

        My.Application.Log.WriteEntry("Created connection string to local database: ", TraceEventType.Verbose)
        My.Application.Log.WriteEntry(connString, TraceEventType.Verbose)
        conn = New System.Data.SqlClient.SqlConnection(connString)
        If DatabaseManager.IsClosed(conn) Then
            My.Application.Log.WriteEntry("Connecting to database", TraceEventType.Verbose)
            conn.Open()
        End If
        My.Application.Log.WriteEntry("Connected to database", TraceEventType.Verbose)

        Return conn

    End Function

#End Region

#Region " READER "

    ''' <summary>
    ''' Executes the specified query command and returns reference to the
    ''' <see cref="ConnectedReader">connected reader</see>
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the connection string. </param>
    ''' <param name="queryCommand">     Specifies a query. </param>
    ''' <returns> An instance of the <see cref="ConnectedReader">reader</see>. </returns>
    Public Overrides Function ExecuteReader(ByVal connectionString As String, ByVal queryCommand As String) As ConnectedReader

        Dim c As New System.Data.SqlClient.SqlConnection(connectionString)
        Return ExecuteReader(c, queryCommand)

    End Function

    ''' <summary>
    ''' Executes the specified query command and returns reference to the
    ''' <see cref="ConnectedReader">connected reader</see>
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">   Specifies a
    '''                             <see cref="SqlClient.SqlConnection">connection</see>. </param>
    ''' <param name="queryCommand"> Specifies a query. </param>
    ''' <returns> A ConnectedReader. </returns>
    Public Overloads Shared Function ExecuteReader(ByVal connection As System.Data.SqlClient.SqlConnection, ByVal queryCommand As String) As ConnectedReader

        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))


        Dim command As New System.Data.SqlClient.SqlCommand() With {
                .Connection = connection,
                .CommandTimeout = 15,
                .CommandType = System.Data.CommandType.Text,
                .CommandText = queryCommand
            }


        If DatabaseManager.IsClosed(connection) Then
            command.Connection.Open()
        End If
        Dim reader As System.Data.SqlClient.SqlDataReader = command.ExecuteReader(
                                                                  System.Data.CommandBehavior.CloseConnection)
        Return New ConnectedReader(connection, reader)

    End Function

#End Region

#Region " UPDATE QUERY "

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks>
    ''' If using multiple SQL commands, each command ends with a ';' followed by a new line.
    ''' </remarks>
    ''' <param name="connectionString">  Specifies the database connection string. </param>
    ''' <param name="query">             Specifies the query of file name where the query is located.
    '''                                  A file name begins with '@'. </param>
    ''' <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
    '''                                  command. </param>
    ''' <returns>
    ''' The total number of minor errors minus the number of successful command executions.
    ''' </returns>
    Public Overrides Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String, ByVal ignoreMinorErrors As Boolean) As Integer
        Return Me.UpdateTableQuery(connectionString, DatabaseManagerBase.PrepareCommandQuery(query), ignoreMinorErrors)
    End Function

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks>
    ''' If using multiple SQL commands, each command ends with a ';' followed by a new line.
    ''' </remarks>
    ''' <param name="connectionString">  Specifies the database connection string. </param>
    ''' <param name="query">             Specifies the query of file name where the query is located.
    '''                                  A file name begins with '@'. </param>
    ''' <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
    '''                                  command. </param>
    ''' <returns>
    ''' The total number of minor errors minus the number of successful command executions.
    ''' </returns>
    Public Overrides Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String(), ByVal ignoreMinorErrors As Boolean) As Integer
        Using connection As New System.Data.SqlClient.SqlConnection(connectionString)
            Return Me.UpdateTableQuery(connection, query, ignoreMinorErrors)
        End Using
    End Function

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks>
    ''' If using multiple SQL commands, each command ends with a ';' followed by a new line.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">        Specifies the database connection. </param>
    ''' <param name="query">             Specifies the query of file name where the query is located.
    '''                                  A file name begins with '@'. </param>
    ''' <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
    '''                                  command. </param>
    ''' <returns>
    ''' The total number of minor errors minus the number of successful command executions.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overloads Function UpdateTableQuery(ByVal connection As System.Data.SqlClient.SqlConnection, ByVal query As String(), ByVal ignoreMinorErrors As Boolean) As Integer

        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim recordCount As Int32
        Dim isOpened As Boolean

        MyBase.QueryUpdateCount = 0
        MyBase.QueryErrorCount = 0
        If Not query Is Nothing Then
            Using command As New System.Data.SqlClient.SqlCommand()
                command.Connection = connection
                command.CommandType = System.Data.CommandType.Text
                If DatabaseManager.IsClosed(connection) Then
                    isOpened = True
                    connection.Open()
                End If
                For Each queryCommand As String In query
                    If Not String.IsNullOrWhiteSpace(queryCommand) Then
                        command.CommandText = queryCommand
                        If ignoreMinorErrors Then
                            Try
                                '? each update returns -1 if success or and error  if failed.
                                recordCount += command.ExecuteNonQuery()
                                MyBase.QueryUpdateCount += 1
                            Catch
                                MyBase.QueryErrorCount += 1
                            Finally
                            End Try
                        Else
                            recordCount += command.ExecuteNonQuery()
                            MyBase.QueryUpdateCount += 1
                        End If
                    End If
                Next
            End Using
            If isOpened Then
                connection.Close()
            End If
        End If
        Return recordCount
    End Function

#End Region

End Class
