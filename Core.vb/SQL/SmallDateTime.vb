﻿''' <summary> Small date time. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/18/2015 </para>
''' </remarks>
Public NotInheritable Class SmallDateTime

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Private Sub New()
    End Sub

    ''' <summary> The minimum value. </summary>
    Public Shared ReadOnly MinValue As New System.Data.SqlTypes.SqlDateTime(New DateTime(1900, 1, 1, 0, 0, 0))

    ''' <summary> The maximum value. </summary>
    Public Shared ReadOnly MaxValue As New System.Data.SqlTypes.SqlDateTime(New DateTime(2079, 6, 6, 23, 59, 0))
End Class
