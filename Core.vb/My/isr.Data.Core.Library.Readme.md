## ISR Data Core<sub>&trade;</sub>: Data Core Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)

### Revision History [](#){name=Revision-History}

*3.3.6667 04/03/18*  
2018 release.

*3.2.6493 10/11/17*  
Removes the places messages queue with cancel detail
event arguments.

*3.1.6331 05/02/17*  
Replaces messages queue with cancel detail event
arguments.

*3.0.5866 01/23/16*  
Updates to .NET 4.6.1

*2.0.5163 02/19/14*  
Uses local sync and async safe event handlers. Uses
diagnosis publishers.

*2.0.5126 01/13/14*  
Tagged as 2014.

*2.0.5065 11/13/13*  
Changes Read to Query current release.

*2.0.5029 10/08/13*  
Imported from the data core library.

*1.2.4968 08/08/13*  
Imported from the data library.

\(C\) 2006 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudio.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Data Libraries](https://bitbucket.org/davidhary/vs.data)
