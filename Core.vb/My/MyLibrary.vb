Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Data

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Data Core SQL Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Data Core SQL Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Data.Core.SQL"

    End Class

End Namespace
