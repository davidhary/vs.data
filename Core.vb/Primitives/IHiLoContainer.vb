﻿''' <summary> The general contract implemented by the class that contains the Hi/Lo information for
''' generating the Hi/Lo identifier. </summary>
''' <remarks> <para>
''' Implementers should provide a public default constructor.
''' </para>
''' <para>
''' Implementers <b>must</b> be thread safe.
''' </para> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Interface IHiloContainer

    ''' <summary> Gets or sets a lock for fetching or saving to the identity store. </summary>
    ''' <value> The lock. </value>
    Property Lock() As Object

    ''' <summary> Fetches the <see cref="HiLoRecord">identifier record</see> from the container. </summary>
    ''' <returns> <c>True</c> if success. </returns>
    Function FetchRecord() As HiloRecord

    ''' <summary> Saves the <see cref="HiLoRecord">identifier record</see> to the container. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>True</c> if success. </returns>
    Function SaveRecord(ByVal value As HiloRecord) As Boolean

End Interface
