﻿''' <summary> Builds an <c>Int64</c> value constructed using a hi/lo algorithm. </summary>
''' <remarks> Uses a database table to store a set of values for generating a long identifier
''' constructed using a hi/lo algorithm. The high value must be fetched in a separate transaction
''' so the generator must be able to obtain a new connection and commit it.  <para>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 10/21/2009, 1.2.3581.x. </para></remarks>
Public Class HiloGenerator

#Region " CONSTRUCTION and CLEANUP "

    Private _HiLoRecord As HiloRecord


    ''' <summary> A constructor to initialize a hi/lo generator for generating identifiers spanning the
    ''' entire positive range of <see cref="Int32">32 bit integers</see>. </summary>
    Public Sub New()
        MyBase.New()
        Me._hiLoRecord = New HiloRecord(0, 0, 0)
    End Sub

#End Region

#Region " GENERATOR "

    ''' <summary> Generates a new identifier. Saves the current record in the container. </summary>
    ''' <remarks> A <see cref="HiLoRecord.Low"></see> locally incremented value is used with a
    ''' <see cref="HiLoRecord.seed">seed</see> to generate a
    ''' <see cref="HiLoRecord.Identifier"></see>long identifier.
    ''' When the locally incremented value exceeds a <see cref="HiLoRecord.MaximumLow">maximum</see>,
    ''' a new
    ''' <see cref="HiLoRecord.High">high</see> value is fetched from the
    ''' container. This high value is used to create a new seed. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="container"> . </param>
    ''' <returns> The id or<para>
    ''' 0 if failed in New Identifier</para><para>
    ''' -1 if failed fetching the record</para><para>
    ''' -2 if failed saving the record</para> </returns>
    Public Overridable Function Generate(ByVal container As IHiloContainer) As Long

        If container Is Nothing Then
            Throw New ArgumentNullException("container")
        End If
        If Me._hiLoRecord.MaximumLow = 0 Then

            SyncLock container.Lock()
                Me._hiLoRecord = container.FetchRecord()
                If Me._hiLoRecord.High = 0 Then
                    Return -1
                End If
                Dim id As Long = Me._hiLoRecord.NewIdentifier()
                If id > 0 Then
                    If Not container.SaveRecord(Me._hiLoRecord) Then
                        id = -2
                    End If
                End If
                Return id
            End SyncLock

        ElseIf Me._hiLoRecord.Low > Me._hiLoRecord.MaximumLow Then

            SyncLock container.Lock()
                Dim record As HiloRecord = container.FetchRecord()
                If record.High = 0 Then
                    Return -1
                End If
                Me._hiLoRecord = New HiloRecord(record.High + 1, 1, record.MaximumLow)
                If Not container.SaveRecord(record) Then
                    Return -2
                End If
            End SyncLock

        Else
            SyncLock container.Lock()
                Me._hiLoRecord.NewIdentifier()
                If Not container.SaveRecord(Me._hiLoRecord) Then
                    Return -2
                End If
            End SyncLock
        End If

        Return Me._hiLoRecord.Identifier

    End Function

#End Region

End Class

''' <summary> Holds the hi/lo record. </summary>
''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para> </remarks>
Public Structure HiloRecord

    ''' <summary> Constructor. </summary>
    ''' <param name="high">       The high value. </param>
    ''' <param name="low">        The locally incremented value. </param>
    ''' <param name="maximumLow"> The maximum value of the <see cref="_low">locally incremented
    ''' value.</see>  
    ''' Once the locally incremented value exceeds the <see cref="_maximumLow">maximum</see>
    ''' a new <see cref="_high">high value</see> is fetched from the
    ''' <see cref="IHiLoContainer">container</see>
    ''' for creating a new seed for generating a new sequence of identifiers. </param>
    Public Sub New(ByVal high As Int32, ByVal low As Int32, ByVal maximumLow As Int32)
        Me._High = high
        Me._Low = low
        Me._MaximumLow = maximumLow
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <param name="record"> The record. </param>
    Public Sub New(ByVal record As HiloRecord)
        Me.New(record.High, record.Low, record.MaximumLow)
    End Sub

#Region " EQUALS "

    ''' <summary> Returns true if the instances are equal. </summary>
    ''' <param name="left">  Specifies a left hand value for comparison. </param>
    ''' <param name="right"> Specifies a right hand value for comparison. </param>
    ''' <returns> <c>True</c> if equals; <c>False</c> otherwise. </returns>
    Public Overloads Shared Function Equals(ByVal left As HiloRecord, ByVal right As HiloRecord) As Boolean
        Return left isnot nothing andlaso left.Equals(right)
    End Function

    ''' <summary>
    ''' Returns true if the instances are equal.
    ''' </summary>
    ''' <param name="left">Specifies a left hand value for comparison</param>
    ''' <param name="right">Specifies a right hand value for comparison</param>
    ''' <returns> <c>True</c> if equals; <c>False</c> otherwise. </returns>
    Public Shared Operator =(ByVal left As HiloRecord, ByVal right As HiloRecord) As Boolean
		Return HiloGenerator.Equals(left, right)
    End Operator

    ''' <summary> Returns true if the instances are equal. </summary>
    ''' <param name="left">  Specifies a left hand value for comparison. </param>
    ''' <param name="right"> Specifies a right hand value for comparison. </param>
    ''' <returns> <c>True</c> if not equal; <c>False</c> otherwise. </returns>
    Public Shared Operator <>(ByVal left As HiloRecord, ByVal right As HiloRecord) As Boolean
        Return Not HiloGenerator.Equals(left, right)
    End Operator

    ''' <summary> Returns true if the values are equal. </summary>
    ''' <param name="other"> The hi lo record to compare to this object. </param>
    ''' <returns> <c>True</c> if equals; <c>False</c> otherwise. </returns>
    Public Overloads Function Equals(ByVal other As HiloRecord) As Boolean
        Return other is not nothign andalso me.High = other.High AndAlso
               me.Low = other.Low AndAlso
               me.MaximumLow = other.MaximumLow
    End Function

    ''' <summary> Returns true if the values are equal. </summary>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns> <c>True</c> if equals; <c>False</c> otherwise. </returns>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
		Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, ValidationOutcome))
    End Function

    ''' <summary> Returns a unique hash code for this instance. </summary>
    ''' <returns> A hash code for this object. </returns>

    Public Overrides Function GetHashCode() As Integer
        Return Me._High.GetHashCode() Xor Me._Low.GetHashCode() Xor Me._MaximumLow.GetHashCode()
    End Function

#End Region

    ''' <summary> Gets the maximum value of the <see cref="_low">locally incremented value.</see>  
    ''' Once the locally incremented value exceeds the <see cref="_maximumLow">maximum</see>
    ''' a new <see cref="_high">high value </see> is fetched from the
    ''' <see cref="IHiLoContainer">container</see>
    ''' for creating a new seed for generating a new sequence of identifiers. </summary>
    ''' <value> The maximum low. </value>
    Public Property MaximumLow() As Int32

    ''' <summary> Gets the high value. </summary>
    ''' <value> The high. </value>
    Public Property High() As Int32

    ''' <summary> Gets the locally incremented value. </summary>
    ''' <value> The low. </value>
    Public Property Low() As Int32

    ''' <summary> Gets the current identifier. </summary>
    ''' <value> The identifier. </value>
    Public ReadOnly Property Identifier() As Long
        Get
            Return Me.Seed + Me._Low
        End Get
    End Property

    ''' <summary> Increments the low value and returns a new identifier. </summary>
    ''' <returns> new identifier. </returns>
    Public Function NewIdentifier() As Long
        Me._Low += 1
        Return Identifier
    End Function

    ''' <summary> Gets the current seed. </summary>
    ''' <value> The seed. </value>
    Public ReadOnly Property Seed() As Long
        Get
            Return Me._MaximumLow * (Me._High - 1)
        End Get
    End Property

    ''' <summary> Gets the represented value. </summary>
    ''' <returns> A String representing the value. </returns>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             "Hi:{0},Lo:{1},MaxLo:{2}", Me._High, Me._Low, Me._MaximumLow)
    End Function

End Structure