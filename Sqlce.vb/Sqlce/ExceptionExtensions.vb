Imports System.Runtime.CompilerServices

Namespace ExceptionExtensions

    ''' <summary> Adds exception data for building the exception full blown report. </summary>
    ''' <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary>
        ''' Adds the <paramref name="exception"/> data to <paramref name="value"/> exception.
        ''' </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="value">     The value. </param>
        ''' <param name="exception"> The exception. </param>
        ''' <returns>
        ''' <c>true</c> if it <see cref="Exception"/> is not nothing; otherwise <c>false</c>
        ''' </returns>
        Public Function AddExceptionData(ByVal value As System.Exception, ByVal exception As SqlServerCe.SqlCeException) As Boolean
            If value IsNot Nothing AndAlso exception IsNot Nothing Then
                value.Data.Add($"{value.Data.Count}-ErrorCode", exception.ErrorCode)
                If exception.Errors?.Count > 0 Then
                    For Each err As SqlServerCe.SqlCeError In exception.Errors
                        value.Data.Add($"{value.Data.Count}-Error{err.NativeError}", err.ToString)
                    Next
                End If
            End If
            Return exception IsNot Nothing
        End Function

        ''' <summary> Adds exception data from the specified exception. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        <Extension>
        Public Function AddExceptionData(ByVal exception As System.Exception) As Boolean
            Return Methods.AddExceptionData(exception, TryCast(exception, SqlServerCe.SqlCeException))
        End Function

        ''' <summary> Adds an exception data. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Private Function AddExceptionDataThis(ByVal exception As System.Exception) As Boolean
            Return Methods.AddExceptionData(exception) OrElse
                   isr.Data.Core.ExceptionExtensions.Methods.AddExceptionData(exception) OrElse
                   isr.Core.ExceptionExtensions.Methods.AddExceptionData(exception)
        End Function

        ''' <summary> Converts a value to a full blown string. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception) As String
            Return Methods.ToFullBlownString(value, Integer.MaxValue)
        End Function

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <remarks> David, 10/8/2020. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="level"> The level. </param>
        ''' <returns> The given data converted to a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception, ByVal level As Integer) As String
            Return isr.Core.ExceptionExtensions.Methods.ToFullBlownString(value, level, AddressOf AddExceptionDataThis)
        End Function

    End Module

End Namespace

