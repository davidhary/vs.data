Imports isr.Core
Imports isr.Data.Sqlce.ExceptionExtensions

''' <summary> Implements a database manager entity for SQL Compact Edition. </summary>
''' <remarks>
''' David, 10/21/2009, 1.2.3581. <para>
''' David, 09/16/2011, 1.2.4276. Use namespace isr.Data.Compact. </para><para>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class DatabaseManager
    Inherits Core.DatabaseManagerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Core.DatabaseManagerBase" /> class.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    Public Sub New(ByVal connectionString As String)
        MyBase.New(connectionString)
        Me.CurrentReleaseVersionQueryText = "SELECT ReleaseRevision, IsCurrentRelease FROM ReleaseHistory WHERE (IsCurrentRelease = 1)"
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Core.DatabaseManagerBase" /> class.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <param name="databaseName">     Name of the database. </param>
    Public Sub New(ByVal connectionString As String, ByVal databaseName As String)
        MyBase.New(connectionString, databaseName)
        Me.CurrentReleaseVersionQueryText = "SELECT ReleaseRevision, IsCurrentRelease FROM ReleaseHistory WHERE (IsCurrentRelease = 1)"
    End Sub

#End Region

#Region " PRESETTABLE "

    ''' <summary> Initializes the known state. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    Public Overrides Sub InitializeKnownState()
    End Sub

#End Region

#Region " CONNECTION INFO "

#Region " BUILDERS "

    ''' <summary>
    ''' Returns the application data folder using the application company and product name or
    ''' directory path. In design mode the data folder is the program folder such as c:\Program File\
    ''' ISR\ISR Assembly 7.0\Program In production, this is the application data folder product name
    ''' such as Application Data\ISR\ISR Assembly 7.0\major.minor.revision under Windows XP or
    ''' ProgramData\ISR\ISR Assembly 7.0\major.minor.revision under Vista.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="useApplicationFolder"> true to use application folder. </param>
    ''' <param name="allUsers">             true to all users. </param>
    ''' <returns> Application data folder. </returns>
    Public Shared Function BuildApplicationDataFolderName(ByVal useApplicationFolder As Boolean,
                                                              ByVal allUsers As Boolean) As String

        Dim path As String = String.Empty

        If useApplicationFolder Then

            path = My.Application.Info.DirectoryPath

        Else

            Dim specialPath As String = String.Empty
            specialPath = If(allUsers,
                My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData,
                My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData)

            ' get the application folder.
            specialPath = My.Computer.FileSystem.GetParentPath(specialPath)
            ' get the top folder so that we can create our own path invariant of the executing assembly.
            specialPath = My.Computer.FileSystem.GetParentPath(specialPath)

            ' use the company name and product name to the actual path.
            path = System.IO.Path.Combine(specialPath, My.Application.Info.CompanyName)
            path = System.IO.Path.Combine(path, My.Application.Info.ProductName)
            path = System.IO.Path.Combine(path, My.Application.Info.Version.ToString(3))

        End If

        ' return the full path for the folder.
        Return path

    End Function

    ''' <summary>
    ''' Returns a standard data folder name.  
    ''' In design mode the data folder is a sibling to the program folder. In production, the data
    ''' folder resides under the application data folder product name.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="useApplicationFolder"> true to use application folder. </param>
    ''' <param name="allUsers">             true to all users. </param>
    ''' <param name="folderTitle">          . </param>
    ''' <param name="useSiblingFolder">     true to use sibling folder. </param>
    ''' <returns> Application data folder. </returns>
    Public Shared Function BuildApplicationDataFolderName(ByVal useApplicationFolder As Boolean,
                                                              ByVal allUsers As Boolean,
                                                              ByVal folderTitle As String,
                                                              ByVal useSiblingFolder As Boolean) As String

        Dim dataPath As String = Sqlce.DatabaseManager.BuildApplicationDataFolderName(useApplicationFolder, allUsers)
        If useSiblingFolder Then
            dataPath = My.Computer.FileSystem.GetParentPath(dataPath)
        End If
        Return System.IO.Path.Combine(dataPath, folderTitle)

    End Function

    ''' <summary> Builds a default SQL Compact Edition connection string. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="databaseName">          Name of the database. </param>
    ''' <param name="databaseFolderTitle">   The database folder title. </param>
    ''' <param name="databaseFileExtension"> The database file extension. </param>
    ''' <returns> Connection string for SQL Compact Edition. </returns>
    Public Shared Function BuildConnectionString(ByVal databaseName As String, ByVal databaseFolderTitle As String, ByVal databaseFileExtension As String) As String
        Dim folderPath As String = Sqlce.DatabaseManager.BuildApplicationDataFolderName(True, True, databaseFolderTitle, True)
        Dim filePath As String = System.IO.Path.Combine(folderPath, databaseName & databaseFileExtension)
        Return String.Format(Globalization.CultureInfo.InvariantCulture, "Data Source={0}", filePath)
    End Function

#End Region

#Region " PARSERS "

    ''' <summary> Parses the compact connection string. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="connectionString"> The connection string. </param>
    Public Overrides Sub ParseConnectionString(ByVal connectionString As String)
        If String.IsNullOrWhiteSpace(connectionString) Then
            Throw New ArgumentNullException(NameOf(connectionString))
        End If
        Dim elements As String() = connectionString.Split("="c)
        If elements.Length < 2 Then
            Throw New ArgumentException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Invalid connection string '{0}'.", connectionString),
                                            NameOf(connectionString))
        End If
        Me.ServerName = "SQLCE"
        Me.InstanceName = "SQLCE"
        Me.DatabaseFileInfo = New System.IO.FileInfo(elements(1).Trim.Trim(";"c))
        Me.DatabaseName = Me.DatabaseFileInfo.Name.Substring(0, Me.DatabaseFileInfo.Name.LastIndexOf(Me.DatabaseFileInfo.Extension, StringComparison.OrdinalIgnoreCase))
        Me.DataSourceCompatibilityLevel = 4

    End Sub

#End Region

#End Region

#Region " CONNECTION MANAGER "

    ''' <summary> Pings the database server. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> True if the server was located; otherwise, false. </returns>
    Public Overrides Function PingHost(ByVal timeout As TimeSpan) As Boolean
        Return My.Computer.Network.Ping(Me.ServerName, CInt(timeout.TotalMilliseconds))
    End Function

    ''' <summary> Creates the connection. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <returns> The new connection. </returns>
    Public Shared Function CreateConnection() As SqlServerCe.SqlCeConnection
        Dim conn As SqlServerCe.SqlCeConnection = Nothing
        Dim temp As SqlServerCe.SqlCeConnection = Nothing
        Try
            temp = New SqlServerCe.SqlCeConnection()
            conn = temp
            temp = Nothing
        Catch
            Throw
        Finally
            temp?.Dispose()
        End Try
        Return conn
    End Function

    ''' <summary> Opens a connection. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <returns> A SqlServerCe.SqlCeConnection. </returns>
    Public Shared Function OpenConnection(ByVal connectionString As String) As SqlServerCe.SqlCeConnection
        If String.IsNullOrWhiteSpace(connectionString) Then Throw New ArgumentNullException(NameOf(connectionString))
        Dim conn As SqlServerCe.SqlCeConnection = Nothing
        Dim temp As SqlServerCe.SqlCeConnection = Nothing
        Try
            temp = DatabaseManager.CreateConnection
            temp.ConnectionString = connectionString
            temp.Open()
            conn = temp
            temp = Nothing
        Catch
            Throw
        Finally
            temp?.Dispose()
        End Try
        Return conn

    End Function

    ''' <summary>
    ''' Returns true if able to connect to the database using the specified connection string.
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connectionString">   Specifies the connection string to connect to. </param>
    ''' <param name="keepConnectionOpen"> True to keep connection open. </param>
    ''' <returns> True if connected. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overrides Function TryConnect(ByVal connectionString As String, ByVal keepConnectionOpen As Boolean) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If String.IsNullOrWhiteSpace(connectionString) Then Throw New ArgumentNullException(NameOf(connectionString))
        Dim activity As String = String.Empty
        Dim connection As System.Data.SqlServerCe.SqlCeConnection = Nothing
        Try
            ' connect to the local database.
            activity = $"creating connection using {connectionString}"
            connection = New System.Data.SqlServerCe.SqlCeConnection(connectionString)
            activity = $"opening connection {connectionString}"
            connection.Open()
            MyBase.LastConnectionFailed = DatabaseManager.IsClosed(connection)
        Catch ex As Exception
            MyBase.LastConnectionFailed = True
            result = (False, $"Exception {activity};. {ex.ToFullBlownString}")
        Finally
            If connection IsNot Nothing Then
                If Not keepConnectionOpen Then
                    ' apparently, close also disposes the connection. Otherwise, uncomment this:
                    'If not DatabaseManager.IsClosed(connection)  Then connection.Close()
                    connection.Dispose()
                End If
            End If
        End Try
        Return result
    End Function

#End Region

#Region " COMMAND "

    ''' <summary> Executes the specified command and returns the number of rows affected. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the connection string. </param>
    ''' <param name="commandText">      Specifies a command. </param>
    ''' <returns> Row count. </returns>
    Public Overrides Function ExecuteCommand(ByVal connectionString As String, ByVal commandText As String) As Integer

        Dim rowCount As Integer = 0
        Using connection As System.Data.SqlServerCe.SqlCeConnection = New System.Data.SqlServerCe.SqlCeConnection(connectionString)
            Dim sqlCommand As System.Data.SqlServerCe.SqlCeCommand = connection.CreateCommand()
            sqlCommand.CommandText = commandText
            connection.Open()
            rowCount = sqlCommand.ExecuteNonQuery()
            ' note the close also disposes. Otherwise, do: connection.Close()
        End Using
        Return rowCount

    End Function

#End Region

#Region " DATA TABLE "

    ''' <summary> Opens a table. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="tableName">  Name of the table. </param>
    ''' <returns> A DataTable. </returns>
    Public Shared Function FillTable(ByVal connection As SqlServerCe.SqlCeConnection, ByVal tableName As String) As DataTable
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        If String.IsNullOrWhiteSpace(tableName) Then Throw New ArgumentNullException(NameOf(tableName))
        Return DatabaseManager.FillTable(connection, tableName, $"SELECT * FROM {tableName}")
    End Function

    ''' <summary> Opens a table. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="tableName">    Name of the table. </param>
    ''' <param name="queryCommand"> Specifies a query. </param>
    ''' <returns> A DataTable. </returns>
    Public Shared Function FillTable(ByVal connection As IDbConnection,
                                     ByVal tableName As String, ByVal queryCommand As String) As DataTable
        Return DatabaseManager.FillTable(TryCast(connection, SqlServerCe.SqlCeConnection), tableName, queryCommand)
    End Function

    ''' <summary> Opens a table. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="connection">   The connection. </param>
    ''' <param name="tableName">    Name of the table. </param>
    ''' <param name="queryCommand"> Specifies a query. </param>
    ''' <returns> A DataTable. </returns>
    Public Shared Function FillTable(ByVal connection As SqlServerCe.SqlCeConnection,
                                     ByVal tableName As String, ByVal queryCommand As String) As DataTable
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        If String.IsNullOrWhiteSpace(tableName) Then Throw New ArgumentNullException(NameOf(tableName))
        If DatabaseManager.IsClosed(connection) Then connection.Open()
        If DatabaseManager.IsClosed(connection) Then
            Throw New InvalidOperationException("Unable to open the connection to the database")
        End If
        Dim result As DataTable = Nothing
        Try
            result = New DataTable(tableName) With {.Locale = Globalization.CultureInfo.CurrentCulture}
            Using dataAdapter As New SqlServerCe.SqlCeDataAdapter(queryCommand, connection)
                dataAdapter.Fill(result)
            End Using
        Catch
            If result IsNot Nothing Then result.Dispose()
            Throw
        End Try
        Return result

    End Function

#End Region

#Region " READER "

    ''' <summary>
    ''' Executes the specified query command and returns reference to the
    ''' <see cref="Core.ConnectedReader">connected reader</see>
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> Specifies the connection string. </param>
    ''' <param name="queryCommand">     Specifies a query. </param>
    ''' <returns> <see cref="Core.ConnectedReader">connected reader</see>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope",
            Justification:="Required to execute the reader.")>
    Public Overrides Function ExecuteReader(ByVal connectionString As String, ByVal queryCommand As String) As Core.ConnectedReader

        Return ExecuteReader(New System.Data.SqlServerCe.SqlCeConnection(connectionString), queryCommand)

    End Function

    ''' <summary>
    ''' Executes the specified query command and returns reference to the
    ''' <see cref="Core.ConnectedReader">connected reader</see>
    ''' </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">   Specifies a
    '''                             <see cref="System.Data.SqlServerCe.SqlCeConnection">connection</see>. 
    ''' </param>
    ''' <param name="queryCommand"> Specifies a query. </param>
    ''' <returns> <see cref="Core.ConnectedReader">connected reader</see>. </returns>
    Public Overloads Shared Function ExecuteReader(ByVal connection As System.Data.SqlServerCe.SqlCeConnection, ByVal queryCommand As String) As Core.ConnectedReader

        If connection Is Nothing Then
            Throw New ArgumentNullException(NameOf(connection))
        End If
        Using command As New System.Data.SqlServerCe.SqlCeCommand()
            command.Connection = connection
            command.CommandType = System.Data.CommandType.Text
            command.CommandText = queryCommand
            If DatabaseManager.IsClosed(connection) Then command.Connection.Open()
            Dim reader As System.Data.SqlServerCe.SqlCeDataReader = command.ExecuteResultSet(
                                        System.Data.SqlServerCe.ResultSetOptions.Scrollable)
            Return New Core.ConnectedReader(connection, reader)
        End Using

    End Function

#End Region

#Region " UPDATE QUERY "

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString">  Specifies the database connection string. </param>
    ''' <param name="query">             Specifies the query of file name where the query is located.
    '''                                  A file name begins with '@'. </param>
    ''' <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
    '''                                  command. </param>
    ''' <returns>
    ''' The total number of minor errors minus the number of successful command executions.
    ''' </returns>
    Public Overrides Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String, ByVal ignoreMinorErrors As Boolean) As Integer
        Return Me.UpdateTableQuery(connectionString, Core.DatabaseManagerBase.PrepareCommandQuery(query), ignoreMinorErrors)
    End Function

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString">  Specifies the database connection string. </param>
    ''' <param name="query">             Specifies the query of file name where the query is located.
    '''                                  A file name begins with '@'. </param>
    ''' <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
    '''                                  command. </param>
    ''' <returns>
    ''' The total number of minor errors minus the number of successful command executions.
    ''' </returns>
    Public Overrides Function UpdateTableQuery(ByVal connectionString As String, ByVal query As String(), ByVal ignoreMinorErrors As Boolean) As Integer

        Using connection As New System.Data.SqlServerCe.SqlCeConnection(connectionString)
            Return Me.UpdateTableQuery(connection, query, ignoreMinorErrors)
        End Using

    End Function

    ''' <summary> Executes a table update query. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection">        Specifies a
    '''                                  <see cref="System.Data.SqlServerCe.SqlCeConnection">connection</see>. 
    ''' </param>
    ''' <param name="query">             Specifies the query of file name where the query is located.
    '''                                  A file name begins with '@'. </param>
    ''' <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
    '''                                  command. </param>
    ''' <returns>
    ''' The total number of minor errors minus the number of successful command executions.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overloads Function UpdateTableQuery(ByVal connection As System.Data.SqlServerCe.SqlCeConnection, ByVal query As String(), ByVal ignoreMinorErrors As Boolean) As Integer

        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        Dim recordCount As Int32
        Dim isOpened As Boolean
        MyBase.QueryUpdateCount = 0
        MyBase.QueryErrorCount = 0
        If query IsNot Nothing Then
            Using command As New System.Data.SqlServerCe.SqlCeCommand()
                command.Connection = connection
                command.CommandType = System.Data.CommandType.Text
                If DatabaseManager.IsClosed(connection) Then
                    isOpened = True
                    connection.Open()
                End If
                For Each queryCommand As String In query
                    If Not String.IsNullOrWhiteSpace(queryCommand) Then
                        ' trim any leading or lagging control characters and spaces.
                        queryCommand = queryCommand.Trim
                        If Not String.IsNullOrWhiteSpace(queryCommand) Then
                            command.CommandText = queryCommand
                            If ignoreMinorErrors Then
                                Try
                                    ' TO_DO: Check on this.
                                    ' each update returns -1 if success or and error  if failed.
                                    recordCount += command.ExecuteNonQuery()
                                    MyBase.QueryUpdateCount += 1
                                Catch ex As Exception
                                    MyBase.QueryErrorCount += 1
                                Finally
                                End Try
                            Else
                                recordCount += command.ExecuteNonQuery()
                                MyBase.QueryUpdateCount += 1
                            End If
                        End If
                    End If
                Next
            End Using
            If isOpened Then
                connection.Close()
            End If
        End If
        Return recordCount

    End Function

#End Region

End Class
