using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyCompany( "Microsoft" )]
[assembly: AssemblyCopyright( "(c) 2009 Microsoft Corporation. All rights reserved." )]
[assembly: AssemblyTrademark( "Licensed under The MIT License." )]
// [assembly: System.Resources.NeutralResourcesLanguage("en-US", System.Resources.UltimateResourceFallbackLocation.MainAssembly)]
// [assembly: AssemblyCulture( "en-US" )]
[assembly: AssemblyVersion( "9.0.*" )]
