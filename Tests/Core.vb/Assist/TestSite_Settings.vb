Partial Friend Class TestSite

    ''' <summary> Gets or sets a unique identifier of the oledb provider. </summary>
    ''' <value> Unique identifier of the oledb provider. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("SQLOLEDB.1")>
    Public Overridable Property OledbProviderUniqueId As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets a unique identifier of the jet oledb provider. </summary>
    ''' <value> Unique identifier of the jet oledb provider. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Microsoft.Jet.OLEDB.4.0.1")>
    Public Overridable Property JetOledbProviderUniqueId As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

#Region " SQL SERVER CONNECTION "

    ''' <summary> Gets or sets the connection string. </summary>
    ''' <value> The connection string. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(),
        Global.System.Configuration.DefaultSettingValueAttribute("data source=MUSCAT\SQLEXPRESS;initial catalog=Provers70;persist security info=False;User id=bank;Password=syncWithMe;")>
    Public Overridable Property ConnectionString As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the name of the server. </summary>
    ''' <value> The name of the server. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("MUSCAT\SQLEXPRESS")>
    Public Overridable Property ServerName As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the name of the database. </summary>
    ''' <value> The name of the database. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Provers70")>
    Public Overridable Property DatabaseName As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the name of the user. </summary>
    ''' <value> The name of the user. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("bank")>
    Public Overridable Property UserName As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the password. </summary>
    ''' <value> The password. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("syncWithMe")>
    Public Overridable Property Password As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets the credentials. </summary>
    ''' <value> The credentials. </value>
    Public ReadOnly Property Credentials As SqlClient.SqlCredential
        Get
            Dim pw As New Security.SecureString()
            For Each c As Char In Me.Password.ToCharArray
                pw.AppendChar(c)
            Next
            pw.MakeReadOnly()
            Return New SqlClient.SqlCredential(Me.UserName, pw)
        End Get
    End Property

#End Region

End Class
