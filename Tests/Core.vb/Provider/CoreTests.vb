Imports isr.Data.Core

''' <summary> Core data unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/10/2017 </para>
''' </remarks>
<TestClass()>
Public Class CoreTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            CoreTests._TestSite = New TestSite
            CoreTests.TestSite.ModifyApplicationDomainDataDirectoryPath()
            CoreTests.TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            CoreTests.TestSite.AddTraceMessagesQueue(isr.Data.Core.My.MyLibrary.UnpublishedTraceMessages)
            CoreTests.TestSite.InitializeTraceListener()

            ' assert reading of test settings from the configuration file.
            Assert.IsTrue(CoreTests.TestSite.Exists, $"{NameOf(Data.CoreTests.TestSite)} settings should exist")
            Dim expectedUpperLimit As Double = 12
            Assert.IsTrue(Math.Abs(CoreTests.TestSite.TimeZoneOffset) < expectedUpperLimit,
                          $"{NameOf(Data.CoreTests.TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")

        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        TestSite.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestSite.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets or sets the test context which provides information about and functionality for the
    ''' current test run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> Gets or sets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestSite() As TestSite

#End Region

#Region " CONNECTION MANAGER "

    ''' <summary> (Unit Test Method) tests parse connection string. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <TestMethod()>
    Public Sub ParseConnectionStringTest()
        Dim manager As New Data.Core.DatabaseManager(CoreTests.TestSite.ConnectionString)
        manager.ParseConnectionString(CoreTests.TestSite.ConnectionString)
        Assert.AreEqual(CoreTests.TestSite.ServerName, manager.ServerName, $"{NameOf(Data.Core.DatabaseManager.ServerName)}")
        Assert.AreEqual(CoreTests.TestSite.DatabaseName, manager.DatabaseName, $"{NameOf(Data.Core.DatabaseManager.DatabaseName)}")
        Assert.IsNotNull(manager.Credentials, $"{GetType(Data.Core.DatabaseManager)}.{NameOf(Data.Core.DatabaseManager.Credentials)} must not be null")
        Assert.AreEqual(CoreTests.TestSite.Credentials.UserId, manager.Credentials.UserId, $"{NameOf(SqlClient.SqlCredential.UserId)}")
        Assert.AreEqual(CoreTests.TestSite.Credentials.Password.ToString, manager.Credentials.Password.ToString, $"{NameOf(SqlClient.SqlCredential.Password)}")
    End Sub

#End Region

End Class
