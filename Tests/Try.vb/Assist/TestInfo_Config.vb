Imports System.Configuration

Partial Friend Class TestSite

#Region " TRACE EVENT PROJECT IDENTITY FILE "

    ''' <summary> Gets the filename of the trace event project identities file. </summary>
    ''' <value> The filename of the trace event project identities file. </value>
    Public Shared ReadOnly Property TraceEventProjectIdentitiesFileName As String
        Get
            Return isr.Core.AppSettingsReader.Get().AppSettingValue
        End Get
    End Property

    ''' <summary> Gets the SQLite getting started connection string. </summary>
    ''' <value> The sq lite getting started connection string. </value>
    Public Shared ReadOnly Property SQLiteGettingStartedConnectionString As String
        Get
            Return isr.Core.AppSettingsReader.Get().AppSettingValue
        End Get
    End Property

    ''' <summary> Gets the executing assembly location. </summary>
    ''' <value> The executing assembly location. </value>
    Public Shared ReadOnly Property ExecutingAssemblyLocation As String
        Get
            Return isr.Core.AppSettingsReader.Get().AppSettingValue
        End Get
    End Property

#End Region

End Class
