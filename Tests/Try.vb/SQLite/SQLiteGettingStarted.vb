Imports isr.Core
Imports isr.Core.VisualBasicLoggingExtensions

Imports System.Data.SQLite

''' <summary> A trace event identities. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/17/2018 </para>
''' </remarks>
<TestClass()>
Public Class SQLiteGettingStarted

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " CONNECTION TESTS "

    ''' <summary> Opens a connection. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <returns> A SQLiteConnection. </returns>
    Private Shared Function OpenConnection(ByVal connectionString As String) As SQLiteConnection
        Dim connection As New SQLiteConnection(connectionString)
        connection.Open()
        Assert.AreEqual(ConnectionState.Open, connection.State And ConnectionState.Open, $"Failed opening {connectionString}")
        Return connection
    End Function

    ''' <summary> (Unit Test Method) tests open close connection. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <TestMethod()>
    Public Sub OpenCloseConnectionTest()
        Return
        Dim connection As SQLiteConnection = SQLiteGettingStarted.OpenConnection(TestSite.SQLiteGettingStartedConnectionString)
        connection.Close()
        Assert.AreEqual(ConnectionState.Closed, connection.State And ConnectionState.Closed, $"Failed closing {TestSite.SQLiteGettingStartedConnectionString}")
    End Sub

#End Region

End Class
