Imports System.Configuration

''' <summary> SQLite getting started connection information. </summary>
Partial Friend NotInheritable Class TestAssist

#Region " SQLIte Connection String "

    ''' <summary> Gets the sq lite getting started connection string. </summary>
    ''' <value> The sq lite getting started connection string. </value>
    Public Shared ReadOnly Property SQLiteGettingStartedConnectionString As String
        Get
            Return isr.Core.AppSettingsReader.Get().AppSettingValue
        End Get
    End Property

#End Region

End Class
