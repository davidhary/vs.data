using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Data.SqlCeTests
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> A bridge compensation unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    public class DataTableTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        [ClassInitialize()]
        [CLSCompliant(false)]
        public static void MyClassInitialize(TestContext testContext)
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.ModifyApplicationDomainDataDirectoryPath();
                TestInfo.AddTraceMessagesQueue(TestInfo.TraceMessagesQueueListener);
                TestInfo.AddTraceMessagesQueue(Core.My.MyLibrary.UnpublishedTraceMessages);
                TestInfo.AddTraceMessagesQueue(Sqlce.My.MyLibrary.UnpublishedTraceMessages);
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if (TestInfo is object)
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{nameof(TestInfo)} settings should exist");
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( isr.Data.SqlCeTests.DataTableTests.TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Data.SqlCeTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " PART DATA "

#if false
	// Old data source using a relative folder. Program no longer finds the database file giving a test error.
    [DataSource("System.Data.SqlServerCe.4.0", "Data Source=PartDataTests\TestData.sdf", "Part", DataAccessMethod.Sequential)]
    [DataSource("Provider=Microsoft.SqlServerCe.Client.4.0;Data Source=PartDataTests\TestData.sdf", "Part")]
    [DataSource("PartDataTests")]
#endif

        /// <summary> (Unit Test Method) tests read part table. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [DataSource("System.Data.SqlServerCe.4.0", @"Data Source=C:\My\Libraries\VS\Data\Data\Tests\Sqlce\bin\Debug\PartDataTests\TestData.sdf", "Part", DataAccessMethod.Sequential)]
        [TestMethod()]
        public void ReadPartTableTest()
        {
            this.TestContext.DataConnection.ConnectionString = @"Data Source=C:\My\Libraries\VS\Data\Data\Tests\Sqlce\bin\Debug\PartDataTests\TestData.sdf";
            var part = new PartInfo( this.TestContext );
            double expectedVoltage = 5d;
            double actualVoltage = part.NominalVoltage;
            Assert.AreEqual(expectedVoltage, actualVoltage, $"{part.PartNumber}");
            var existing = new int[] { 201703161, 114836, 77072 };
            if (existing.Contains(part.PartNumber))
                Assert.AreEqual(part.PartNumber, part.NominalInfo.PartNumber);
        }

        #endregion

    }
}
