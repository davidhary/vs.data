﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Data SqlCe Tests")]
[assembly: AssemblyDescription("Unit Tests for Sql Compact Edition Data Library")]
[assembly: AssemblyProduct("isr.Data.SqlCeTests.Library")]
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
