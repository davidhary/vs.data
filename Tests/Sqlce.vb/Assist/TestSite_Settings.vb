Partial Friend Class TestSite

    ''' <summary> Gets or sets a unique identifier of the oledb provider. </summary>
    ''' <value> Unique identifier of the oledb provider. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("SQLOLEDB.1")>
    Public Overridable Property OledbProviderUniqueId As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets a unique identifier of the jet oledb provider. </summary>
    ''' <value> Unique identifier of the jet oledb provider. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Microsoft.Jet.OLEDB.4.0.1")>
    Public Overridable Property JetOledbProviderUniqueId As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

End Class
