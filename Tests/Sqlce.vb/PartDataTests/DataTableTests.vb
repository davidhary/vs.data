Imports System.Data.Common
Imports System.Text

Imports Microsoft.VisualStudio.TestTools.UnitTesting

''' <summary> A bridge compensation unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/10/2017 </para>
''' </remarks>
<TestClass()>
Public Class DataTableTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.ModifyApplicationDomainDataDirectoryPath()
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Data.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.AddTraceMessagesQueue(isr.Data.Sqlce.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " PART DATA "

#If False Then
	' Old data source using a relative folder. Program no longer finds the database file giving a test error.
    <DataSource("System.Data.SqlServerCe.4.0", "Data Source=PartDataTests\TestData.sdf", "Part", DataAccessMethod.Sequential)>
    <DataSource("Provider=Microsoft.SqlServerCe.Client.4.0;Data Source=PartDataTests\TestData.sdf", "Part")>
    <DataSource("PartDataTests")>
#End If

    ''' <summary> (Unit Test Method) tests read part table. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    <DataSource("System.Data.SqlServerCe.4.0", "Data Source=C:\My\Libraries\VS\Data\Data\Tests\Sqlce\bin\Debug\PartDataTests\TestData.sdf", "Part", DataAccessMethod.Sequential)>
    <TestMethod()>
    Public Sub ReadPartTableTest()
        Me.TestContext.DataConnection.ConnectionString = "Data Source=C:\My\Libraries\VS\Data\Data\Tests\Sqlce\bin\Debug\PartDataTests\TestData.sdf"
        Dim part As New PartInfo(Me.TestContext)
        Dim expectedVoltage As Double = 5
        Dim actualVoltage As Double = part.NominalVoltage
        Assert.AreEqual(expectedVoltage, actualVoltage, $"{part.PartNumber}")
        Dim existing As Integer() = New Integer() {201703161, 114836, 77072}
        If existing.Contains(part.PartNumber) Then Assert.AreEqual(part.PartNumber, part.NominalInfo.PartNumber)
    End Sub

#End Region

End Class
