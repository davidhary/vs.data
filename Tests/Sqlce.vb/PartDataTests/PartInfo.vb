''' <summary> Information about the part. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/11/2017 </para>
''' </remarks>
Public Class PartInfo

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="testContext"> Context for the test. </param>
    Public Sub New(ByVal testContext As TestContext)
        Me.New(TestSite.ValidatedDataRow(testContext))
        ' Me.PopulateRelatedRecords(testContext)
        Me.PopulateRelatedRecords(TestSite.ValidatedConnection(testContext))
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="row"> The row. </param>
    Public Sub New(ByVal row As DataRow)
        MyBase.New
        If row Is Nothing Then
            Throw New ArgumentNullException(NameOf(row))
        Else
            Me.PartNumber = row.Field(Of Integer)(NameOf(PartInfo.PartNumber))
            Me.NominalVoltage = row.Field(Of Double)(NameOf(PartInfo.NominalVoltage))
            Me.NominalCurrent = row.Field(Of Double)(NameOf(PartInfo.NominalCurrent))
            Me.NominalOutput = row.Field(Of Double)(NameOf(PartInfo.NominalOutput))
        End If
    End Sub

    ''' <summary> Gets or sets the part number. </summary>
    ''' <value> The part number. </value>
    Public Property PartNumber As Integer

    ''' <summary> Gets or sets the nominal voltage. </summary>
    ''' <value> The nominal voltage. </value>
    Public Property NominalVoltage As Double

    ''' <summary> Gets or sets the nominal current. </summary>
    ''' <value> The nominal current. </value>
    Public Property NominalCurrent As Double

    ''' <summary> Gets or sets the nominal output. </summary>
    ''' <value> The nominal output. </value>
    Public Property NominalOutput As Double

    ''' <summary> Gets or sets information describing the nominal. </summary>
    ''' <value> Information describing the nominal. </value>
    Public Property NominalInfo As NominalInfo

    ''' <summary> Gets or sets the name of the nominal table. </summary>
    ''' <value> The name of the nominal table. </value>
    Public Shared Property NominalTableName As String = "Nominal"

    ''' <summary> Gets or sets the part number record query format. </summary>
    ''' <value> The part number record query format. </value>
    Public Shared Property PartNumberRecordQueryFormat As String = "SELECT * FROM {0} WHERE PartNumber={1}"

    ''' <summary> Populates a related records. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="testContext"> Context for the test. </param>
    Public Sub PopulateRelatedRecords(ByVal testContext As TestContext)
        If testContext Is Nothing Then Throw New ArgumentNullException(NameOf(testContext))
        Dim query As String = String.Format(PartInfo.PartNumberRecordQueryFormat, PartInfo.NominalTableName, Me.PartNumber)
        Using dt As DataTable = Data.Sqlce.DatabaseManager.FillTable(testContext.DataConnection, PartInfo.NominalTableName, query)
            Me.NominalInfo = If(dt.Rows?.Count > 0, New NominalInfo(dt.Rows(0)), Nothing)
        End Using
    End Sub

    ''' <summary> Populates a related records. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="connection"> The connection. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")>
    Public Sub PopulateRelatedRecords(ByVal connection As IDbConnection)
        If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
        ' Dim conn As New System.Data.SqlServerCe.SqlCeConnection With {.ConnectionString = "Data Source=C:\My\Libraries\VS\Data\Data\Tests\Sqlce\bin\Debug\PartDataTests\TestData.sdf"}
        ' THe test context gets disposed killing the connection. 
        Dim conn As New System.Data.SqlServerCe.SqlCeConnection With {.ConnectionString = connection.ConnectionString}
        Using dt As New DataTable(PartInfo.NominalTableName)
            dt.Locale = Globalization.CultureInfo.CurrentCulture
            Dim query As String = String.Format(PartInfo.PartNumberRecordQueryFormat, PartInfo.NominalTableName, Me.PartNumber)
            '           Using dad As New SqlServerCe.SqlCeDataAdapter(query, TryCast(connection, SqlServerCe.SqlCeConnection))
            Using dad As New SqlServerCe.SqlCeDataAdapter(query, conn)
                dad.Fill(dt)
                Me.NominalInfo = If(dt.Rows?.Count > 0, New NominalInfo(dt.Rows(0)), Nothing)
            End Using
        End Using
    End Sub

End Class

''' <summary> Information about the nominal. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/13/2017 </para>
''' </remarks>
Public Class NominalInfo

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <param name="row"> The row. </param>
    Public Sub New(ByVal row As DataRow)
        MyBase.New
        Me.ParseDataRow(row)
    End Sub

    ''' <summary> Parse data row. </summary>
    ''' <remarks> David, 10/8/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="row"> The row. </param>
    Private Sub ParseDataRow(ByVal row As DataRow)
        If row Is Nothing Then Throw New ArgumentNullException(NameOf(row))
        Me.PartNumber = row.Field(Of Integer)(NameOf(NominalInfo.PartNumber))
        Me.NominalValue = row.Field(Of Double)(NameOf(NominalInfo.NominalValue))
        Me.Tolerance = row.Field(Of Double)(NameOf(NominalInfo.Tolerance))
    End Sub

    ''' <summary> Gets or sets the part number. </summary>
    ''' <value> The part number. </value>
    Public Property PartNumber As Integer

    ''' <summary> Gets or sets the nominal value. </summary>
    ''' <value> The nominal value. </value>
    Public Property NominalValue As Double

    ''' <summary> Gets or sets the tolerance. </summary>
    ''' <value> The tolerance. </value>
    Public Property Tolerance As Double

End Class

