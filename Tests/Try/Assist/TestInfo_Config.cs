﻿
namespace isr.Data.Try
{
    internal partial class TestSite
    {

        #region " TRACE EVENT PROJECT IDENTITY FILE "

        /// <summary> Gets the filename of the trace event project identities file. </summary>
        /// <value> The filename of the trace event project identities file. </value>
        public static string TraceEventProjectIdentitiesFileName
        {
            get
            {
                return Core.AppSettingsReader.Get().AppSettingValue();
            }
        }

        /// <summary> Gets the SQLite getting started connection string. </summary>
        /// <value> The sq lite getting started connection string. </value>
        public static string SQLiteGettingStartedConnectionString
        {
            get
            {
                return Core.AppSettingsReader.Get().AppSettingValue();
            }
        }

        /// <summary> Gets the executing assembly location. </summary>
        /// <value> The executing assembly location. </value>
        public static string ExecutingAssemblyLocation
        {
            get
            {
                return Core.AppSettingsReader.Get().AppSettingValue();
            }
        }

        #endregion

    }
}