﻿
namespace isr.Data.Try
{
    internal partial class TestSite
    {

        /// <summary> Gets or sets a unique identifier of the oledb provider. </summary>
        /// <value> Unique identifier of the oledb provider. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("SQLOLEDB.1")]
        public virtual string OledbProviderUniqueId
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a unique identifier of the jet oledb provider. </summary>
        /// <value> Unique identifier of the jet oledb provider. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("Microsoft.Jet.OLEDB.4.0.1")]
        public virtual string JetOledbProviderUniqueId
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }
    }
}