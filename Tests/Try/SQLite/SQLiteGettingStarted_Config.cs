﻿
namespace isr.Data.Try
{

    /// <summary> SQLite getting started connection information. </summary>
    internal sealed partial class TestAssist
    {

        #region " SQLIte Connection String "

        /// <summary> Gets the sq lite getting started connection string. </summary>
        /// <value> The sq lite getting started connection string. </value>
        public static string SQLiteGettingStartedConnectionString
        {
            get
            {
                return Core.AppSettingsReader.Get().AppSettingValue();
            }
        }

        #endregion

    }
}