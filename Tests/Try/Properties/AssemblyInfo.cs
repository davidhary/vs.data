﻿using System;
using System.Reflection;

[assembly: AssemblyTitle(isr.Data.Try.My.MyLibrary.AssemblyTitle)]
[assembly: AssemblyDescription(isr.Data.Try.My.MyLibrary.AssemblyDescription)]
[assembly: AssemblyProduct(isr.Data.Try.My.MyLibrary.AssemblyProduct)]
[assembly: CLSCompliant(true)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
