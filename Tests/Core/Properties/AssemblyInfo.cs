﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Data Core Tests")]
[assembly: AssemblyDescription("Unit Tests for Core Data Library")]
[assembly: AssemblyProduct("isr.Data.CoreTests.Library")]
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
