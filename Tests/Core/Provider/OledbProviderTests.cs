using System;
using isr.Data.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Data.CoreTests
{

    /// <summary> OLD DB Provider unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    public class OledbProviderTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        [ClassInitialize()]
        [CLSCompliant(false)]
        public static void MyClassInitialize(TestContext testContext)
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.ModifyApplicationDomainDataDirectoryPath();
                TestInfo.AddTraceMessagesQueue(TestInfo.TraceMessagesQueueListener);
                TestInfo.AddTraceMessagesQueue(Core.My.MyLibrary.UnpublishedTraceMessages);
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if (TestInfo is object)
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue(TestInfo.Exists, $"{nameof(TestInfo)} settings should exist");
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( isr.Data.CoreTests.TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " OLE INFO "

        /// <summary> (Unit Test Method) tests enumerate oledb provider information exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void EnumerateOledbProviderInformationExistsTest()
        {
            var providers = OledbProviderInfoCollection.Providers();
            Assert.IsTrue(providers.Contains(TestInfo.OledbProviderUniqueId));
        }

        #endregion

    }
}
