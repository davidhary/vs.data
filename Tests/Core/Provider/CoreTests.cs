﻿using System;
using isr.Data.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Data.CoreTests
{

    /// <summary> Core data unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    public class CoreTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
        [ClassInitialize()]
        [CLSCompliant(false)]
        public static void MyClassInitialize(TestContext testContext)
        {
            try
            {
                TestSite = new TestSite();
                TestSite.ModifyApplicationDomainDataDirectoryPath();
                TestSite.AddTraceMessagesQueue(TestSite.TraceMessagesQueueListener);
                TestSite.AddTraceMessagesQueue(Core.My.MyLibrary.UnpublishedTraceMessages);
                TestSite.InitializeTraceListener();

                // assert reading of test settings from the configuration file.
                Assert.IsTrue(TestSite.Exists, $"{nameof(Data.CoreTests.TestSite)} settings should exist");
                double expectedUpperLimit = 12d;
                Assert.IsTrue(Math.Abs(TestSite.TimeZoneOffset) < expectedUpperLimit, $"{nameof(Data.CoreTests.TestSite.TimeZoneOffset).Split()} should be lower than {expectedUpperLimit}");
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if (TestSite is object)
            {
                TestSite.Dispose();
                TestSite = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TestSite.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestSite.AssertMessageQueue();
        }

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets or sets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestSite { get; set; }

        #endregion

        #region " CONNECTION MANAGER "

        /// <summary> (Unit Test Method) tests parse connection string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void ParseConnectionStringTest()
        {
            var manager = new DatabaseManager(TestSite.ConnectionString);
            manager.ParseConnectionString(TestSite.ConnectionString);
            Assert.AreEqual(TestSite.ServerName, manager.ServerName, $"{nameof(DatabaseManager.ServerName)}");
            Assert.AreEqual(TestSite.DatabaseName, manager.DatabaseName, $"{nameof(DatabaseManager.DatabaseName)}");
            Assert.IsNotNull(manager.Credentials, $"{typeof(DatabaseManager)}.{nameof(DatabaseManager.Credentials)} must not be null");
            Assert.AreEqual(TestSite.Credentials.UserId, manager.Credentials.UserId, $"{nameof(System.Data.SqlClient.SqlCredential.UserId)}");
            Assert.AreEqual(TestSite.Credentials.Password.ToString(), manager.Credentials.Password.ToString(), $"{nameof(System.Data.SqlClient.SqlCredential.Password)}");
        }

        #endregion

    }
}