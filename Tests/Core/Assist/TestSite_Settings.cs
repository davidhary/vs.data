﻿
namespace isr.Data.CoreTests
{
    internal partial class TestSite
    {

        /// <summary> Gets or sets a unique identifier of the oledb provider. </summary>
        /// <value> Unique identifier of the oledb provider. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("SQLOLEDB.1")]
        public virtual string OledbProviderUniqueId
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a unique identifier of the jet oledb provider. </summary>
        /// <value> Unique identifier of the jet oledb provider. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("Microsoft.Jet.OLEDB.4.0.1")]
        public virtual string JetOledbProviderUniqueId
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #region " SQL SERVER CONNECTION "

        /// <summary> Gets or sets the connection string. </summary>
        /// <value> The connection string. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue(@"data source=MUSCAT\SQLEXPRESS;initial catalog=Provers70;persist security info=False;User id=bank;Password=syncWithMe;")]
        public virtual string ConnectionString
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the name of the server. </summary>
        /// <value> The name of the server. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue(@"MUSCAT\SQLEXPRESS")]
        public virtual string ServerName
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the name of the database. </summary>
        /// <value> The name of the database. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("Provers70")]
        public virtual string DatabaseName
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the name of the user. </summary>
        /// <value> The name of the user. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("bank")]
        public virtual string UserName
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the password. </summary>
        /// <value> The password. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("syncWithMe")]
        public virtual string Password
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets the credentials. </summary>
        /// <value> The credentials. </value>
        public System.Data.SqlClient.SqlCredential Credentials
        {
            get
            {
                var pw = new System.Security.SecureString();
                foreach (char c in Password.ToCharArray())
                    pw.AppendChar(c);
                pw.MakeReadOnly();
                return new System.Data.SqlClient.SqlCredential(UserName, pw);
            }
        }

        #endregion

    }
}