//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.OleDb;

namespace Microsoft.Data.ConnectionUI
{
    public class OleDBConnectionProperties : AdoDotNetConnectionProperties
    {
        public OleDBConnectionProperties()
            : base( "System.Data.OleDb" )
        {
        }

        public bool DisableProviderSelection { get; set; }

        public override bool IsComplete => this.ConnectionStringBuilder["Provider"] is string &&
                    (this.ConnectionStringBuilder["Provider"] as string).Length != 0;

        protected override PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            PropertyDescriptorCollection descriptors = base.GetProperties( attributes );
            if ( this.DisableProviderSelection )
            {
                PropertyDescriptor providerDescriptor = descriptors.Find( "Provider", true );
                if ( providerDescriptor != null )
                {
                    int index = descriptors.IndexOf( providerDescriptor );
                    PropertyDescriptor[] descriptorArray = new PropertyDescriptor[descriptors.Count];
                    descriptors.CopyTo( descriptorArray, 0 );
                    descriptorArray[index] = new DynamicPropertyDescriptor( providerDescriptor, ReadOnlyAttribute.Yes );
                    (descriptorArray[index] as DynamicPropertyDescriptor).CanResetValueHandler = new CanResetValueHandler( this.CanResetProvider );
                    descriptors = new PropertyDescriptorCollection( descriptorArray, true );
                }
            }
            return descriptors;
        }

        /// <summary>
        /// Gets the registered OLE DB providers as an array of ProgIDs.
        /// </summary>
        public static List<string> GetRegisteredProviders()
        {
            // Get the sources rowset for the root OLE DB enumerator
            Type rootEnumeratorType = Type.GetTypeFromCLSID( NativeMethods.CLSID_OLEDB_ENUMERATOR );
            OleDbDataReader dr = OleDbEnumerator.GetEnumerator( rootEnumeratorType );

            // Read the CLSIDs of each data source (not binders or enumerators)
            Dictionary<string, string> sources = new Dictionary<string, string>(); // avoids duplicate entries
            using ( dr )
            {
                while ( dr.Read() )
                {
                    int type = ( int ) dr["SOURCES_TYPE"];
                    if ( type == NativeMethods.DBSOURCETYPE_DATASOURCE_TDP ||
                        type == NativeMethods.DBSOURCETYPE_DATASOURCE_MDP )
                    {
                        sources[dr["SOURCES_CLSID"] as string] = null;
                    }
                }
            } // reader is disposed here

            // Get the ProgID for each data source
            List<string> sourceProgIds = new List<string>( sources.Count );
            Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey( "CLSID" );
            using ( key )
            {
                foreach ( KeyValuePair<string, string> source in sources )
                {
                    Microsoft.Win32.RegistryKey subKey = key.OpenSubKey( source.Key + "\\ProgID" );
                    // if this subkey does not exist, ignore it.
                    if ( subKey != null )
                    {
                        using ( subKey )
                        {
                            sourceProgIds.Add( subKey.GetValue( null ) as string );
                        } // subKey is disposed here
                    }
                }
            } // key is disposed here

            // Sort the prog ID array by name
            sourceProgIds.Sort();

            // The OLE DB provider for ODBC is not supported by the OLE DB .NET provider, so remove it
            while ( sourceProgIds.Contains( "MSDASQL.1" ) )
            {
                _ = sourceProgIds.Remove( "MSDASQL.1" );
            }

            return sourceProgIds;
        }

        private bool CanResetProvider( object component )
        {
            return false;
        }
    }

    public class OleDBSpecializedConnectionProperties : OleDBConnectionProperties
    {
        public OleDBSpecializedConnectionProperties( string provider )
        {
            Debug.Assert( provider != null );
            this._Provider = provider;
            this.LocalReset();
        }

        public override void Reset()
        {
            base.Reset();
            this.LocalReset();
        }

        protected override PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            bool disableProviderSelection = this.DisableProviderSelection;
            try
            {
                this.DisableProviderSelection = true;
                return base.GetProperties( attributes );
            }
            finally
            {
                this.DisableProviderSelection = disableProviderSelection;
            }
        }

        private void LocalReset()
        {
            // Initialize with the selected provider
            this["Provider"] = this._Provider;
        }

        private readonly string _Provider;
    }

    public class OleDBSqlConnectionProperties : OleDBSpecializedConnectionProperties
    {

        public OleDBSqlConnectionProperties()
            : base( "SQLOLEDB" )
        {
            this.LocalReset();
        }

        public override void Reset()
        {
            base.Reset();
            this.LocalReset();
        }

        public override bool IsComplete => !base.IsComplete
                    ? false
                    : !(this.ConnectionStringBuilder["Data Source"] is string) ||
                    (this.ConnectionStringBuilder["Data Source"] as string).Length == 0
                    ? false
                    : this.ConnectionStringBuilder["Integrated Security"] != null &&
                    this.ConnectionStringBuilder["Integrated Security"].ToString().Equals( "SSPI", StringComparison.OrdinalIgnoreCase ) ||
                    this.ConnectionStringBuilder["User ID"] is string &&
                    (this.ConnectionStringBuilder["User ID"] as string).Length != 0;

        protected override PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            PropertyDescriptorCollection descriptors = base.GetProperties( attributes );
            if ( SqlNativeClientRegistered )
            {
                if ( descriptors.Find( "Provider", true ) is DynamicPropertyDescriptor providerDescriptor )
                {
                    if ( !this.DisableProviderSelection )
                    {
                        providerDescriptor.SetIsReadOnly( false );
                    }
                    providerDescriptor.SetConverterType( typeof( SqlProviderConverter ) );
                }
            }
            return descriptors;
        }

        private void LocalReset()
        {
            // We always start with integrated security turned on
            this["Integrated Security"] = "SSPI";
        }

        public static List<string> SqlNativeClientProviders
        {
            get {
                if ( _SqlNativeClientProviders == null )
                {
                    _SqlNativeClientProviders = new List<string>();

                    List<string> providers = GetRegisteredProviders();
                    Debug.Assert( providers != null, "provider list is null" );
                    foreach ( string provider in providers )
                    {
                        if ( provider.StartsWith( "SQLNCLI" ) )
                        {
                            int idx = provider.IndexOf( "." );
                            if ( idx > 0 )
                            {
                                _SqlNativeClientProviders.Add( provider.Substring( 0, idx ).ToUpperInvariant() );
                            }
                        }
                    }

                    _SqlNativeClientProviders.Sort();
                }

                Debug.Assert( _SqlNativeClientProviders != null, "Native Client list is null" );
                return _SqlNativeClientProviders;
            }
        }

        private static bool SqlNativeClientRegistered
        {
            get {
                if ( !_GotSqlNativeClientRegistered )
                {
                    Microsoft.Win32.RegistryKey key = null;
                    try
                    {
                        _SqlNativeClientRegistered = SqlNativeClientProviders.Count > 0;
                    }
                    finally
                    {
                        if ( key != null )
                        {
                            key.Close();
                        }
                    }
                    _GotSqlNativeClientRegistered = true;
                }
                return _SqlNativeClientRegistered;
            }
        }

        private static bool _SqlNativeClientRegistered;
        private static List<string> _SqlNativeClientProviders = null;
        private static bool _GotSqlNativeClientRegistered;

        private class SqlProviderConverter : StringConverter
        {
            public SqlProviderConverter()
            {
            }

            public override bool GetStandardValuesSupported( ITypeDescriptorContext context )
            {
                return true;
            }

            public override bool GetStandardValuesExclusive( ITypeDescriptorContext context )
            {
                return true;
            }

            public override StandardValuesCollection GetStandardValues( ITypeDescriptorContext context )
            {
                List<string> stdCollection = new List<string> {
                    "SQLOLEDB"
                };

                foreach ( string provider in SqlNativeClientProviders )
                {
                    stdCollection.Add( provider );
                }

                return new StandardValuesCollection( stdCollection );
            }
        }
    }

    public class OleDBOracleConnectionProperties : OleDBSpecializedConnectionProperties
    {
        public OleDBOracleConnectionProperties()
            : base( "MSDAORA" )
        {
        }

        public override bool IsComplete => !base.IsComplete
                    ? false
                    : !(this.ConnectionStringBuilder["Data Source"] is string) ||
                    (this.ConnectionStringBuilder["Data Source"] as string).Length == 0
                    ? false
                    : this.ConnectionStringBuilder["User ID"] is string &&
                    (this.ConnectionStringBuilder["User ID"] as string).Length != 0;
    }

    public class OleDBAccessConnectionProperties : OleDBSpecializedConnectionProperties
    {
        public OleDBAccessConnectionProperties()
            : base( "Microsoft.Jet.OLEDB.4.0" )
        {
            this._UserChangedProvider = false;
        }

        public override void Reset()
        {
            base.Reset();
            this._UserChangedProvider = false;
        }

        public override object this[string propertyName]
        {
            set {
                base[propertyName] = value;
                if ( String.Equals( propertyName, "Provider", StringComparison.OrdinalIgnoreCase ) )
                {
                    if ( value != null && value != DBNull.Value )
                    {
                        this.OnProviderChanged( this.ConnectionStringBuilder, EventArgs.Empty );
                    }
                    else
                    {
                        this._UserChangedProvider = false;
                    }
                }
                if ( String.Equals( propertyName, "Data Source", StringComparison.Ordinal ) )
                {
                    this.OnDataSourceChanged( this.ConnectionStringBuilder, EventArgs.Empty );
                }
            }
        }

        public override void Remove( string propertyName )
        {
            base.Remove( propertyName );
            if ( String.Equals( propertyName, "Provider", StringComparison.OrdinalIgnoreCase ) )
            {
                this._UserChangedProvider = false;
            }
            if ( String.Equals( propertyName, "Data Source", StringComparison.Ordinal ) )
            {
                this.OnDataSourceChanged( this.ConnectionStringBuilder, EventArgs.Empty );
            }
        }

        public override void Reset( string propertyName )
        {
            base.Reset( propertyName );
            if ( String.Equals( propertyName, "Provider", StringComparison.OrdinalIgnoreCase ) )
            {
                this._UserChangedProvider = false;
            }
            if ( String.Equals( propertyName, "Data Source", StringComparison.Ordinal ) )
            {
                this.OnDataSourceChanged( this.ConnectionStringBuilder, EventArgs.Empty );
            }
        }

        public override bool IsComplete => !base.IsComplete
                    ? false
                    : this.ConnectionStringBuilder["Data Source"] is string &&
                    (this.ConnectionStringBuilder["Data Source"] as string).Length != 0;

        public override void Test()
        {
            if ( !(this.ConnectionStringBuilder["Data Source"] is string dataSource) || dataSource.Length == 0 )
            {
                throw new InvalidOperationException( Dialog.Resources.Strings.OleDBAccessConnectionProperties_MustSpecifyDataSource );
            }
            base.Test();
        }

        public override string ToDisplayString()
        {
            string savedPassword = null;
            if ( this.ConnectionStringBuilder.ContainsKey( "Jet OLEDB:Database Password" ) &&
                this.ConnectionStringBuilder.ShouldSerialize( "Jet OLEDB:Database Password" ) )
            {
                savedPassword = this.ConnectionStringBuilder["Jet OLEDB:Database Password"] as string;
                _ = this.ConnectionStringBuilder.Remove( "Jet OLEDB:Database Password" );
            }
            string displayString = base.ToDisplayString();
            if ( savedPassword != null )
            {
                this.ConnectionStringBuilder["Jet OLEDB:Database Password"] = savedPassword;
            }
            return displayString;
        }

        protected override PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            PropertyDescriptorCollection descriptors = base.GetProperties( attributes );
            if ( Access12ProviderRegistered )
            {
                if ( descriptors.Find( "Provider", true ) is DynamicPropertyDescriptor providerDescriptor )
                {
                    if ( !this.DisableProviderSelection )
                    {
                        providerDescriptor.SetIsReadOnly( false );
                    }
                    providerDescriptor.SetConverterType( typeof( JetProviderConverter ) );
                    providerDescriptor.AddValueChanged( this.ConnectionStringBuilder, new EventHandler( this.OnProviderChanged ) );
                }
                PropertyDescriptor dataSourceDescriptor = descriptors.Find( "DataSource", true );
                if ( dataSourceDescriptor != null )
                {
                    int index = descriptors.IndexOf( dataSourceDescriptor );
                    PropertyDescriptor[] descriptorArray = new PropertyDescriptor[descriptors.Count];
                    descriptors.CopyTo( descriptorArray, 0 );
                    descriptorArray[index] = new DynamicPropertyDescriptor( dataSourceDescriptor );
                    descriptorArray[index].AddValueChanged( this.ConnectionStringBuilder, new EventHandler( this.OnDataSourceChanged ) );
                    descriptors = new PropertyDescriptorCollection( descriptorArray, true );
                }
            }
            PropertyDescriptor passwordDescriptor = descriptors.Find( "Jet OLEDB:Database Password", true );
            if ( passwordDescriptor != null )
            {
                int index = descriptors.IndexOf( passwordDescriptor );
                PropertyDescriptor[] descriptorArray = new PropertyDescriptor[descriptors.Count];
                descriptors.CopyTo( descriptorArray, 0 );
                descriptorArray[index] = new DynamicPropertyDescriptor( passwordDescriptor, PasswordPropertyTextAttribute.Yes );
                descriptors = new PropertyDescriptorCollection( descriptorArray, true );
            }
            return descriptors;
        }

        private static bool Access12ProviderRegistered
        {
            get {
                if ( !_GotAccess12ProviderRegistered )
                {
                    Microsoft.Win32.RegistryKey key = null;
                    try
                    {
                        key = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey( "Microsoft.ACE.OLEDB.12.0" );
                        _Access12ProviderRegistered = (key != null);
                    }
                    finally
                    {
                        if ( key != null )
                        {
                            key.Close();
                        }
                    }
                    _GotAccess12ProviderRegistered = true;
                }
                return _Access12ProviderRegistered;
            }
        }
        private static bool _Access12ProviderRegistered;
        private static bool _GotAccess12ProviderRegistered;

        private class JetProviderConverter : StringConverter
        {
            public JetProviderConverter()
            {
            }

            public override bool GetStandardValuesSupported( ITypeDescriptorContext context )
            {
                return true;
            }

            public override bool GetStandardValuesExclusive( ITypeDescriptorContext context )
            {
                return true;
            }

            public override StandardValuesCollection GetStandardValues( ITypeDescriptorContext context )
            {
                return new StandardValuesCollection( new string[] {
                    "Microsoft.Jet.OLEDB.4.0", "Microsoft.ACE.OLEDB.12.0"
                } );
            }
        }

        private void OnProviderChanged( object sender, EventArgs e )
        {
            if ( Access12ProviderRegistered )
            {
                this._UserChangedProvider = true;
            }
        }

        private void OnDataSourceChanged( object sender, EventArgs e )
        {
            if ( Access12ProviderRegistered && !this._UserChangedProvider )
            {
                if ( this["Data Source"] is string dataSource )
                {
                    dataSource = dataSource.Trim().ToUpperInvariant();
                    base["Provider"] = dataSource.EndsWith( ".ACCDB", StringComparison.Ordinal ) ? "Microsoft.ACE.OLEDB.12.0" : "Microsoft.Jet.OLEDB.4.0";
                }
            }
        }

        private bool _UserChangedProvider;
    }
}
