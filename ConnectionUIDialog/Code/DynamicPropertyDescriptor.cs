﻿//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Collections.Generic;

namespace Microsoft.Data.ConnectionUI
{
    internal class DynamicPropertyDescriptor : PropertyDescriptor
    {
        public DynamicPropertyDescriptor( string name )
            : base( name, null )
        {
        }

#if NOT_USED
		public DynamicPropertyDescriptor(string name, string displayName) : this(name, displayName, null, null, ReadOnlyAttribute.Default.IsReadOnly)
		{
		}

		public DynamicPropertyDescriptor(string name, string displayName, string category) : this(name, displayName, category, null, ReadOnlyAttribute.Default.IsReadOnly)
		{
		}

		public DynamicPropertyDescriptor(string name, string displayName, string category, string description) : this(name, displayName, category, description, ReadOnlyAttribute.Default.IsReadOnly)
		{
		}

		public DynamicPropertyDescriptor(string name, string displayName, string category, string description, bool isReadOnly) : base(name, BuildAttributes(displayName, category, description, isReadOnly))
		{
		}
#endif

        public DynamicPropertyDescriptor( string name, params Attribute[] attributes )
            : base( name, FilterAttributes( attributes ) )
        {
        }

        public DynamicPropertyDescriptor( PropertyDescriptor baseDescriptor )
            : this( baseDescriptor, null )
        {
        }

        public DynamicPropertyDescriptor( PropertyDescriptor baseDescriptor, params Attribute[] newAttributes )
            : base( baseDescriptor, newAttributes )
        {
            this.AttributeArray = FilterAttributes( this.AttributeArray );
            this._baseDescriptor = baseDescriptor;
        }

        public override string Name => this._name ?? base.Name;

        public override string Category => this._category ?? base.Category;

        public override string Description => this._description ?? base.Description;

        public override Type PropertyType => this._propertyType ?? (this._baseDescriptor != null ? this._baseDescriptor.PropertyType : null);

        public override bool IsReadOnly => (ReadOnlyAttribute.Yes.Equals( this.Attributes[typeof( ReadOnlyAttribute )] ));

        public override TypeConverter Converter
        {
            get {
                if ( this._converterTypeName != null )
                {
                    if ( this._converter == null )
                    {
                        Type converterType = this.GetTypeFromName( this._converterTypeName );
                        if ( typeof( TypeConverter ).IsAssignableFrom( converterType ) )
                        {
                            this._converter = ( TypeConverter ) this.CreateInstance( converterType );
                        }
                    }
                    if ( this._converter != null )
                    {
                        return this._converter;
                    }
                }
                return base.Converter;
            }
        }

        public override AttributeCollection Attributes
        {
            get {
                if ( this._attributes != null )
                {
                    Dictionary<object, Attribute> attributes = new Dictionary<object, Attribute>();
                    foreach ( Attribute attr in this.AttributeArray )
                    {
                        attributes[attr.TypeId] = attr;
                    }
                    foreach ( Attribute attr in this._attributes )
                    {
                        if ( !attr.IsDefaultAttribute() )
                        {
                            attributes[attr.TypeId] = attr;
                        }
                        else if ( attributes.ContainsKey( attr.TypeId ) )
                        {
                            _ = attributes.Remove( attr.TypeId );
                        }
                        if ( attr is CategoryAttribute categoryAttr )
                        {
                            this._category = categoryAttr.Category;
                        }
                        if ( attr is DescriptionAttribute descriptionAttr )
                        {
                            this._description = descriptionAttr.Description;
                        }
                        if ( attr is TypeConverterAttribute typeConverterAttr )
                        {
                            this._converterTypeName = typeConverterAttr.ConverterTypeName;
                            this._converter = null;
                        }
                    }
                    Attribute[] newAttributes = new Attribute[attributes.Values.Count];
                    attributes.Values.CopyTo( newAttributes, 0 );
                    this.AttributeArray = newAttributes;
                    this._attributes = null;
                }
                return base.Attributes;
            }
        }

        public GetValueHandler GetValueHandler { get; set; }

        public SetValueHandler SetValueHandler { get; set; }

        public CanResetValueHandler CanResetValueHandler { get; set; }

        public ResetValueHandler ResetValueHandler { get; set; }

        public ShouldSerializeValueHandler ShouldSerializeValueHandler { get; set; }

        public GetChildPropertiesHandler GetChildPropertiesHandler { get; set; }

        public override Type ComponentType => this._componentType ?? (this._baseDescriptor != null ? this._baseDescriptor.ComponentType : null);

        public void SetName( string value )
        {
            if ( value == null )
            {
                value = String.Empty;
            }
            this._name = value;
        }

        public void SetDisplayName( string value )
        {
            if ( value == null )
            {
                value = DisplayNameAttribute.Default.DisplayName;
            }
            this.SetAttribute( new DisplayNameAttribute( value ) );
        }

        public void SetCategory( string value )
        {
            if ( value == null )
            {
                value = CategoryAttribute.Default.Category;
            }
            this._category = value;
            this.SetAttribute( new CategoryAttribute( value ) );
        }

        public void SetDescription( string value )
        {
            if ( value == null )
            {
                value = DescriptionAttribute.Default.Description;
            }
            this._description = value;
            this.SetAttribute( new DescriptionAttribute( value ) );
        }

        public void SetPropertyType( Type value )
        {
            this._propertyType = value ?? throw new ArgumentNullException( nameof( value ) );
        }

        public void SetDesignTimeOnly( bool value )
        {
            this.SetAttribute( new DesignOnlyAttribute( value ) );
        }

        public void SetIsBrowsable( bool value )
        {
            this.SetAttribute( new BrowsableAttribute( value ) );
        }

        public void SetIsLocalizable( bool value )
        {
            this.SetAttribute( new LocalizableAttribute( value ) );
        }

        public void SetIsReadOnly( bool value )
        {
            this.SetAttribute( new ReadOnlyAttribute( value ) );
        }

        public void SetConverterType( Type value )
        {
            this._converterTypeName = (value != null) ? value.AssemblyQualifiedName : null;
            if ( this._converterTypeName != null )
            {
                this.SetAttribute( new TypeConverterAttribute( value ) );
            }
            else
            {
                this.SetAttribute( TypeConverterAttribute.Default );
            }
            this._converter = null;
        }

        public void SetAttribute( Attribute value )
        {
            if ( value == null )
            {
                throw new ArgumentNullException( nameof( value ) );
            }
            if ( this._attributes == null )
            {
                this._attributes = new List<Attribute>();
            }
            this._attributes.Add( value );
        }

        public void SetAttributes( params Attribute[] values )
        {
            foreach ( Attribute value in values )
            {
                this.SetAttribute( value );
            }
        }

        public void SetComponentType( Type value )
        {
            this._componentType = value;
        }

        public override object GetValue( object component )
        {
            return this.GetValueHandler != null
                ? this.GetValueHandler( component )
                : this._baseDescriptor != null ? this._baseDescriptor.GetValue( component ) : null;
        }

        public override void SetValue( object component, object value )
        {
            if ( this.SetValueHandler != null )
            {
                this.SetValueHandler( component, value );
                this.OnValueChanged( component, EventArgs.Empty );
            }
            else if ( this._baseDescriptor != null )
            {
                this._baseDescriptor.SetValue( component, value );
                this.OnValueChanged( component, EventArgs.Empty );
            }
        }

        public override bool CanResetValue( object component )
        {
            return this.CanResetValueHandler != null
                ? this.CanResetValueHandler( component )
                : this._baseDescriptor != null
                ? this._baseDescriptor.CanResetValue( component )
                : this.Attributes[typeof( DefaultValueAttribute )] != null;
        }

        public override void ResetValue( object component )
        {
            if ( this.ResetValueHandler != null )
            {
                this.ResetValueHandler( component );
            }
            else if ( this._baseDescriptor != null )
            {
                this._baseDescriptor.ResetValue( component );
            }
            else
            {
                if ( this.Attributes[typeof( DefaultValueAttribute )] is DefaultValueAttribute attribute )
                {
                    this.SetValue( component, attribute.Value );
                }
            }
        }

        public override bool ShouldSerializeValue( object component )
        {
            if ( this.ShouldSerializeValueHandler != null )
            {
                return this.ShouldSerializeValueHandler( component );
            }
            return this._baseDescriptor != null
                ? this._baseDescriptor.ShouldSerializeValue( component )
                : this.Attributes[typeof( DefaultValueAttribute )] is DefaultValueAttribute attribute && !Object.Equals( this.GetValue( component ), attribute.Value );
        }

        public override PropertyDescriptorCollection GetChildProperties( object instance, Attribute[] filter )
        {
            return this.GetChildPropertiesHandler != null
                ? this.GetChildPropertiesHandler( instance, filter )
                : this._baseDescriptor != null
                ? this._baseDescriptor.GetChildProperties( instance, filter )
                : base.GetChildProperties( instance, filter );
        }

        protected override int NameHashCode => this._name != null ? this._name.GetHashCode() : base.NameHashCode;

#if NOT_USED
		private static Attribute[] BuildAttributes(string displayName, string category, string description, bool isReadOnly)
		{
			List<Attribute> attributes = new List<Attribute>();
			if (displayName != null && displayName != DisplayNameAttribute.Default.DisplayName)
			{
				attributes.Add(new DisplayNameAttribute(displayName));
			}
			if (category != null && category != CategoryAttribute.Default.Category)
			{
				attributes.Add(new CategoryAttribute(category));
			}
			if (description != null && description != DescriptionAttribute.Default.Description)
			{
				attributes.Add(new DescriptionAttribute(description));
			}
			if (isReadOnly != ReadOnlyAttribute.Default.IsReadOnly)
			{
				attributes.Add(new ReadOnlyAttribute(isReadOnly));
			}
			return attributes.ToArray();
		}
#endif

        private static Attribute[] FilterAttributes( Attribute[] attributes )
        {
            Dictionary<object, Attribute> dictionary = new Dictionary<object, Attribute>();
            foreach ( Attribute attribute in attributes )
            {
                if ( !attribute.IsDefaultAttribute() )
                {
                    dictionary.Add( attribute.TypeId, attribute );
                }
            }
            Attribute[] newAttributes = new Attribute[dictionary.Values.Count];
            dictionary.Values.CopyTo( newAttributes, 0 );
            return newAttributes;
        }

        private string _name;
        private string _category;
        private string _description;
        private Type _propertyType;
        private string _converterTypeName;
        private TypeConverter _converter;
        private List<Attribute> _attributes;
        private Type _componentType;
        private PropertyDescriptor _baseDescriptor;
    }

    internal delegate object GetValueHandler( object component );

    internal delegate void SetValueHandler( object component, object value );

    internal delegate bool CanResetValueHandler( object component );

    internal delegate void ResetValueHandler( object component );

    internal delegate bool ShouldSerializeValueHandler( object component );

    internal delegate PropertyDescriptorCollection GetChildPropertiesHandler( object instance, Attribute[] filter );
}
