//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.ComponentModel;
using System.Collections.Generic;

namespace Microsoft.Data.ConnectionUI
{
    public class AdoDotNetConnectionProperties : IDataConnectionProperties, ICustomTypeDescriptor
    {
        public AdoDotNetConnectionProperties( string providerName )
        {
            Debug.Assert( providerName != null );
            this._ProviderName = providerName;

            // Create an underlying connection string builder object
            DbProviderFactory factory = DbProviderFactories.GetFactory( providerName );
            Debug.Assert( factory != null );
            this.ConnectionStringBuilder = factory.CreateConnectionStringBuilder();
            Debug.Assert( this.ConnectionStringBuilder != null );
            this.ConnectionStringBuilder.BrowsableConnectionString = false;
        }

        public virtual void Reset()
        {
            this.ConnectionStringBuilder.Clear();
            this.OnPropertyChanged( EventArgs.Empty );
        }

        public virtual void Parse( string s )
        {
            this.ConnectionStringBuilder.ConnectionString = s;
            this.OnPropertyChanged( EventArgs.Empty );
        }

        public virtual bool IsExtensible => !this.ConnectionStringBuilder.IsFixedSize;

        public virtual void Add( string propertyName )
        {
            if ( !this.ConnectionStringBuilder.ContainsKey( propertyName ) )
            {
                this.ConnectionStringBuilder.Add( propertyName, String.Empty );
                this.OnPropertyChanged( EventArgs.Empty );
            }
        }

        public virtual bool Contains( string propertyName )
        {
            return this.ConnectionStringBuilder.ContainsKey( propertyName );
        }

        public virtual object this[string propertyName]
        {
            get {
                // Property name must not be null
                if ( propertyName == null )
                {
                    throw new ArgumentNullException( nameof( propertyName ) );
                }

                // If property doesn't exist, just return null
                if ( !this.ConnectionStringBuilder.TryGetValue( propertyName, out _ ) )
                {
                    return null;
                }

                // If property value has been set, return this value
                if ( this.ConnectionStringBuilder.ShouldSerialize( propertyName ) )
                {
                    return this.ConnectionStringBuilder[propertyName];
                }

                // Get the property's default value (if any)
                object val = this.ConnectionStringBuilder[propertyName];

                // If a default value exists, return it
                if ( val != null )
                {
                    return val;
                }

                // No value has been set and no default value exists, so return DBNull.Value
                return DBNull.Value;
            }
            set {
                // Property name must not be null
                if ( propertyName == null )
                {
                    throw new ArgumentNullException( nameof( propertyName ) );
                }

                // Remove the value
                _ = this.ConnectionStringBuilder.Remove( propertyName );

                // Handle cases where value is DBNull.Value
                if ( value == DBNull.Value )
                {
                    // Leave the property in the reset state
                    this.OnPropertyChanged( EventArgs.Empty );
                    return;
                }

                // Get the property's default value (if any)
                _ = this.ConnectionStringBuilder.TryGetValue( propertyName, out object val );

                // Set the value
                this.ConnectionStringBuilder[propertyName] = value;

                // If the value is equal to the default, remove it again
                if ( Object.Equals( val, value ) )
                {
                    _ = this.ConnectionStringBuilder.Remove( propertyName );
                }

                this.OnPropertyChanged( EventArgs.Empty );
            }
        }

        public virtual void Remove( string propertyName )
        {
            if ( this.ConnectionStringBuilder.ContainsKey( propertyName ) )
            {
                _ = this.ConnectionStringBuilder.Remove( propertyName );
                this.OnPropertyChanged( EventArgs.Empty );
            }
        }

        public event EventHandler PropertyChanged;

        public virtual void Reset( string propertyName )
        {
            if ( this.ConnectionStringBuilder.ContainsKey( propertyName ) )
            {
                _ = this.ConnectionStringBuilder.Remove( propertyName );
                this.OnPropertyChanged( EventArgs.Empty );
            }
        }

        public virtual bool IsComplete => true;

        public virtual void Test()
        {
            string testString = this.ToTestString();

            // If the connection string is empty, don't even bother testing
            if ( testString == null || testString.Length == 0 )
            {
                throw new InvalidOperationException( Dialog.Resources.Strings.AdoDotNetConnectionProperties_NoProperties );
            }

            DbProviderFactory factory = DbProviderFactories.GetFactory( this._ProviderName );
            Debug.Assert( factory != null );
            // Create a connection object
            DbConnection connection = factory.CreateConnection();
            Debug.Assert( connection != null );

            // Try to open it
            try
            {
                connection.ConnectionString = testString;
                connection.Open();
                this.Inspect( connection );
            }
            finally
            {
                connection.Dispose();
            }
        }

        public override string ToString()
        {
            return this.ToFullString();
        }

        public virtual string ToFullString()
        {
            return this.ConnectionStringBuilder.ConnectionString;
        }

        public virtual string ToDisplayString()
        {
            PropertyDescriptorCollection sensitiveProperties = this.GetProperties( new Attribute[] { PasswordPropertyTextAttribute.Yes } );
            List<KeyValuePair<string, object>> savedValues = new List<KeyValuePair<string, object>>();
            foreach ( PropertyDescriptor sensitiveProperty in sensitiveProperties )
            {
                string propertyName = sensitiveProperty.DisplayName;
                if ( this.ConnectionStringBuilder.ShouldSerialize( propertyName ) )
                {
                    savedValues.Add( new KeyValuePair<string, object>( propertyName, this.ConnectionStringBuilder[propertyName] ) );
                    _ = this.ConnectionStringBuilder.Remove( propertyName );
                }
            }
            try
            {
                return this.ConnectionStringBuilder.ConnectionString;
            }
            finally
            {
                foreach ( KeyValuePair<string, object> savedValue in savedValues )
                {
                    if ( savedValue.Value != null )
                    {
                        this.ConnectionStringBuilder[savedValue.Key] = savedValue.Value;
                    }
                }
            }
        }

        public DbConnectionStringBuilder ConnectionStringBuilder { get; }

        protected virtual PropertyDescriptor DefaultProperty => TypeDescriptor.GetDefaultProperty( this.ConnectionStringBuilder, true );

        protected virtual PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            return TypeDescriptor.GetProperties( this.ConnectionStringBuilder, attributes );
        }

        protected virtual void OnPropertyChanged( EventArgs e )
        {
            if ( PropertyChanged != null )
            {
                PropertyChanged( this, e );
            }
        }

        protected virtual string ToTestString()
        {
            return this.ConnectionStringBuilder.ConnectionString;
        }

        protected virtual void Inspect( DbConnection connection )
        {
        }

        #region ICustomTypeDescriptor implementation

        string ICustomTypeDescriptor.GetClassName()
        {
            return TypeDescriptor.GetClassName( this.ConnectionStringBuilder, true );
        }

        string ICustomTypeDescriptor.GetComponentName()
        {
            return TypeDescriptor.GetComponentName( this.ConnectionStringBuilder, true );
        }

        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            return TypeDescriptor.GetAttributes( this.ConnectionStringBuilder, true );
        }

        object ICustomTypeDescriptor.GetEditor( Type editorBaseType )
        {
            return TypeDescriptor.GetEditor( this.ConnectionStringBuilder, editorBaseType, true );
        }

        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            return TypeDescriptor.GetConverter( this.ConnectionStringBuilder, true );
        }

        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            return this.DefaultProperty;
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            return this.GetProperties( Array.Empty<Attribute>() );
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties( Attribute[] attributes )
        {
            return this.GetProperties( attributes );
        }

        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent( this.ConnectionStringBuilder, true );
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return TypeDescriptor.GetEvents( this.ConnectionStringBuilder, true );
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents( Attribute[] attributes )
        {
            return TypeDescriptor.GetEvents( this.ConnectionStringBuilder, attributes, true );
        }

        object ICustomTypeDescriptor.GetPropertyOwner( PropertyDescriptor pd )
        {
            return this.ConnectionStringBuilder;
        }

        #endregion

        private readonly string _ProviderName;
    }
}
