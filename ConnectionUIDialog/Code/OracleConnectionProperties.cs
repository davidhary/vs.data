﻿//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;

namespace Microsoft.Data.ConnectionUI
{
    public class OracleConnectionProperties : AdoDotNetConnectionProperties
    {
        public OracleConnectionProperties()
            : base( "System.Data.OracleClient" )
        {
            this.LocalReset();
        }

        public override void Reset()
        {
            base.Reset();
            this.LocalReset();
        }

        public override bool IsComplete => !(this.ConnectionStringBuilder["Data Source"] is string) ||
                    (this.ConnectionStringBuilder["Data Source"] as string).Length == 0
                    ? false
                    : ( bool ) this.ConnectionStringBuilder["Integrated Security"] ||
                    this.ConnectionStringBuilder["User ID"] is string &&
                    (this.ConnectionStringBuilder["User ID"] as string).Length != 0;

        protected override string ToTestString()
        {
            bool savedPooling = ( bool ) this.ConnectionStringBuilder["Pooling"];
            bool wasDefault = !this.ConnectionStringBuilder.ShouldSerialize( "Pooling" );
            this.ConnectionStringBuilder["Pooling"] = false;
            string testString = this.ConnectionStringBuilder.ConnectionString;
            this.ConnectionStringBuilder["Pooling"] = savedPooling;
            if ( wasDefault )
            {
                _ = this.ConnectionStringBuilder.Remove( "Pooling" );
            }
            return testString;
        }

        private void LocalReset()
        {
            // We always start with unicode turned on
            this["Unicode"] = true;
        }

    }
}
