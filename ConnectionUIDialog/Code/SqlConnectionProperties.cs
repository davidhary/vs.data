//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;

namespace Microsoft.Data.ConnectionUI
{
    public class SqlConnectionProperties : AdoDotNetConnectionProperties
    {
        public SqlConnectionProperties()
            : base( "System.Data.SqlClient" )
        {
            this.LocalReset();
        }

        public override void Reset()
        {
            base.Reset();
            this.LocalReset();
        }

        public override bool IsComplete => !(this.ConnectionStringBuilder["Data Source"] is string) ||
                    (this.ConnectionStringBuilder["Data Source"] as string).Length == 0
                    ? false
                    : ( bool ) this.ConnectionStringBuilder["Integrated Security"] ||
                    this.ConnectionStringBuilder["User ID"] is string &&
                    (this.ConnectionStringBuilder["User ID"] as string).Length != 0;

        public override void Test()
        {
            if ( !(this.ConnectionStringBuilder["Data Source"] is string dataSource) || dataSource.Length == 0 )
            {
                throw new InvalidOperationException( Dialog.Resources.Strings.SqlConnectionProperties_MustSpecifyDataSource );
            }
            try
            {
                base.Test();
            }
            catch ( SqlException e )
            {
                if ( e.Number == _SqlErrorCannotOpenDatabase && this.ConnectionStringBuilder["Initial Catalog"] is string database && database.Length > 0 )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.SqlConnectionProperties_CannotTestNonExistentDatabase );
                }
                else
                {
                    throw;
                }
            }
        }

        protected override PropertyDescriptor DefaultProperty => this.GetProperties( Array.Empty<Attribute>() )["DataSource"];

        protected override string ToTestString()
        {
            bool savedPooling = ( bool ) this.ConnectionStringBuilder["Pooling"];
            bool wasDefault = !this.ConnectionStringBuilder.ShouldSerialize( "Pooling" );
            this.ConnectionStringBuilder["Pooling"] = false;
            string testString = this.ConnectionStringBuilder.ConnectionString;
            this.ConnectionStringBuilder["Pooling"] = savedPooling;
            if ( wasDefault )
            {
                _ = this.ConnectionStringBuilder.Remove( "Pooling" );
            }
            return testString;
        }

        protected override void Inspect( DbConnection connection )
        {
            if ( connection.ServerVersion.StartsWith( "07", StringComparison.Ordinal ) ||
                connection.ServerVersion.StartsWith( "08", StringComparison.Ordinal ) )
            {
                throw new NotSupportedException( Dialog.Resources.Strings.SqlConnectionProperties_UnsupportedSqlVersion );
            }
        }

        private void LocalReset()
        {
            // We always start with integrated security turned on
            this["Integrated Security"] = true;
        }

        private const int _SqlErrorCannotOpenDatabase = 4060;
    }

    public class SqlFileConnectionProperties : SqlConnectionProperties
    {
        public SqlFileConnectionProperties()
            : this( null )
        {
        }

        public SqlFileConnectionProperties( string defaultInstanceName )
        {
            this._DefaultDataSource = ".";
            if ( defaultInstanceName != null && defaultInstanceName.Length > 0 )
            {
                this._DefaultDataSource += "\\" + defaultInstanceName;
            }
            else
            {
                DataSourceConverter conv = new DataSourceConverter();
                System.ComponentModel.TypeConverter.StandardValuesCollection coll = conv.GetStandardValues( null );
                if ( coll.Count > 0 )
                {
                    this._DefaultDataSource = coll[0] as string;
                }
            }
            this.LocalReset();
        }

        public override void Reset()
        {
            base.Reset();
            this.LocalReset();
        }

        public override bool IsComplete => !base.IsComplete
                    ? false
                    : this.ConnectionStringBuilder["AttachDbFilename"] is string &&
                    (this.ConnectionStringBuilder["AttachDbFilename"] as string).Length != 0;

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0019:Use pattern matching", Justification = "<Pending>" )]
        public override void Test()
        {
            string attachDbFilename = this.ConnectionStringBuilder["AttachDbFilename"] as string;
            try
            {
                if ( attachDbFilename == null || attachDbFilename.Length == 0 )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.SqlFileConnectionProperties_NoFileSpecified );
                }
                this.ConnectionStringBuilder["AttachDbFilename"] = System.IO.Path.GetFullPath( attachDbFilename );
                if ( !System.IO.File.Exists( this.ConnectionStringBuilder["AttachDbFilename"] as string ) )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.SqlFileConnectionProperties_CannotTestNonExistentMdf );
                }
                base.Test();
            }
            catch ( SqlException e )
            {
                if ( e.Number == -2 ) // timeout
                {
                    throw new ApplicationException( e.Errors[0].Message + Environment.NewLine + Dialog.Resources.Strings.SqlFileConnectionProperties_TimeoutReasons );
                }
                throw;
            }
            finally
            {
                if ( attachDbFilename != null && attachDbFilename.Length > 0 )
                {
                    this.ConnectionStringBuilder["AttachDbFilename"] = attachDbFilename;
                }
            }
        }

        protected override PropertyDescriptorCollection GetProperties( Attribute[] attributes )
        {
            PropertyDescriptorCollection descriptors = base.GetProperties( attributes );
            PropertyDescriptor dataSourceDescriptor = descriptors.Find( "DataSource", true );
            if ( dataSourceDescriptor != null )
            {
                int index = descriptors.IndexOf( dataSourceDescriptor );
                PropertyDescriptor[] descriptorArray = new PropertyDescriptor[descriptors.Count];
                descriptors.CopyTo( descriptorArray, 0 );
                descriptorArray[index] = new DynamicPropertyDescriptor( dataSourceDescriptor, new TypeConverterAttribute( typeof( DataSourceConverter ) ) );
                (descriptorArray[index] as DynamicPropertyDescriptor).CanResetValueHandler = new CanResetValueHandler( this.CanResetDataSource );
                (descriptorArray[index] as DynamicPropertyDescriptor).ResetValueHandler = new ResetValueHandler( this.ResetDataSource );
                descriptors = new PropertyDescriptorCollection( descriptorArray, true );
            }
            return descriptors;
        }

        private void LocalReset()
        {
            this["Data Source"] = this._DefaultDataSource;
            this["User Instance"] = true;
            this["Connection Timeout"] = 30;
        }

        private bool CanResetDataSource( object component )
        {
            return !(this["Data Source"] is string) || !(this["Data Source"] as string).Equals( this._DefaultDataSource, StringComparison.OrdinalIgnoreCase );
        }

        private void ResetDataSource( object component )
        {
            this["Data Source"] = this._DefaultDataSource;
        }

        private class DataSourceConverter : StringConverter
        {
            public DataSourceConverter()
            {
            }

            public override bool GetStandardValuesSupported( ITypeDescriptorContext context )
            {
                return true;
            }

            public override bool GetStandardValuesExclusive( ITypeDescriptorContext context )
            {
                return true;
            }

            public override StandardValuesCollection GetStandardValues( ITypeDescriptorContext context )
            {
                if ( this._StandardValues == null )
                {
                    string[] dataSources = null;

                    if ( HelpUtils.IsWow64() )
                    {
                        List<String> dataSourceList = new List<String>();
                        // Read 64 registry key of SQL Server Instances Names. 
                        dataSourceList.AddRange( HelpUtils.GetValueNamesWow64( "SOFTWARE\\Microsoft\\Microsoft SQL Server\\Instance Names\\SQL", NativeMethods.KEY_WOW64_64KEY | NativeMethods.KEY_QUERY_VALUE ) );
                        // Read 32 registry key of SQL Server Instances Names. 
                        dataSourceList.AddRange( HelpUtils.GetValueNamesWow64( "SOFTWARE\\Microsoft\\Microsoft SQL Server\\Instance Names\\SQL", NativeMethods.KEY_WOW64_32KEY | NativeMethods.KEY_QUERY_VALUE ) );
                        dataSources = dataSourceList.ToArray();
                    }
                    else
                    {
                        // Look in the registry for all local SQL Server instances
                        Win32.RegistryKey key = Win32.Registry.LocalMachine.OpenSubKey( "SOFTWARE\\Microsoft\\Microsoft SQL Server\\Instance Names\\SQL" );
                        if ( key != null )
                        {
                            using ( key )
                            {
                                dataSources = key.GetValueNames();
                            } // key is Disposed here
                        }
                    }

                    if ( dataSources != null )
                    {
                        for ( int i = 0; i < dataSources.Length; i++ )
                        {
                            dataSources[i] = String.Equals( dataSources[i], "MSSQLSERVER", StringComparison.OrdinalIgnoreCase ) ? "." : ".\\" + dataSources[i];
                        }
                        this._StandardValues = new StandardValuesCollection( dataSources );
                    }
                    else
                    {
                        this._StandardValues = new StandardValuesCollection( Array.Empty<string>() );
                    }
                }
                return this._StandardValues;
            }

            private StandardValuesCollection _StandardValues;
        }

        private readonly string _DefaultDataSource;
    }
}
