//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Microsoft.Data.ConnectionUI
{
    public class DataProvider
    {
        public DataProvider( string name, string displayName, string shortDisplayName )
            : this( name, displayName, shortDisplayName, null, null )
        {
        }

        public DataProvider( string name, string displayName, string shortDisplayName, string description )
            : this( name, displayName, shortDisplayName, description, null )
        {
        }

        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType )
        {
            this.Name = name ?? throw new ArgumentNullException( nameof( name ) );
            this._DisplayName = displayName;
            this.ShortDisplayName = shortDisplayName;
            this._Description = description;
            this.TargetConnectionType = targetConnectionType;
        }

        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType, Type connectionPropertiesType )
            : this( name, displayName, shortDisplayName, description, targetConnectionType )
        {
            if ( connectionPropertiesType == null )
            {
                throw new ArgumentNullException( nameof( connectionPropertiesType ) );
            }

            this._ConnectionPropertiesTypes = new Dictionary<string, Type> {
                { String.Empty, connectionPropertiesType }
            };
        }

        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType, Type connectionUIControlType, Type connectionPropertiesType )
            : this( name, displayName, shortDisplayName, description, targetConnectionType, connectionPropertiesType )
        {
            if ( connectionUIControlType == null )
            {
                throw new ArgumentNullException( nameof( connectionUIControlType ) );
            }

            this._ConnectionUIControlTypes = new Dictionary<string, Type> {
                { String.Empty, connectionUIControlType }
            };
        }

        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType, IDictionary<string, Type> connectionUIControlTypes, Type connectionPropertiesType )
            : this( name, displayName, shortDisplayName, description, targetConnectionType, connectionPropertiesType )
        {
            this._ConnectionUIControlTypes = connectionUIControlTypes;
        }

        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType, IDictionary<string, string> dataSourceDescriptions, IDictionary<string, Type> connectionUIControlTypes, Type connectionPropertiesType )
            : this( name, displayName, shortDisplayName, description, targetConnectionType, connectionUIControlTypes, connectionPropertiesType )
        {
            this._DataSourceDescriptions = dataSourceDescriptions;
        }

        public DataProvider( string name, string displayName, string shortDisplayName, string description, Type targetConnectionType, IDictionary<string, string> dataSourceDescriptions, IDictionary<string, Type> connectionUIControlTypes, IDictionary<string, Type> connectionPropertiesTypes )
            : this( name, displayName, shortDisplayName, description, targetConnectionType )
        {
            this._DataSourceDescriptions = dataSourceDescriptions;
            this._ConnectionUIControlTypes = connectionUIControlTypes;
            this._ConnectionPropertiesTypes = connectionPropertiesTypes;
        }

        public static DataProvider SqlDataProvider
        {
            get {
                if ( _SqlDataProvider == null )
                {
                    Dictionary<string, string> descriptions = new Dictionary<string, string> {
                        { DataSource.SqlDataSource.Name, Dialog.Resources.Strings.DataProvider_Sql_DataSource_Description },
                        { DataSource.MicrosoftSqlServerFileName, Dialog.Resources.Strings.DataProvider_Sql_FileDataSource_Description }
                    };

                    Dictionary<string, Type> uiControls = new Dictionary<string, Type> {
                        { DataSource.SqlDataSource.Name, typeof( SqlConnectionUIControl ) },
                        { DataSource.MicrosoftSqlServerFileName, typeof( SqlFileConnectionUIControl ) },
                        { String.Empty, typeof( SqlConnectionUIControl ) }
                    };

                    Dictionary<string, Type> properties = new Dictionary<string, Type> {
                        { DataSource.MicrosoftSqlServerFileName, typeof( SqlFileConnectionProperties ) },
                        { String.Empty, typeof( SqlConnectionProperties ) }
                    };

                    _SqlDataProvider = new DataProvider(
                        "System.Data.SqlClient",
                        Dialog.Resources.Strings.DataProvider_Sql,
                        Dialog.Resources.Strings.DataProvider_Sql_Short,
                        Dialog.Resources.Strings.DataProvider_Sql_Description,
                        typeof( System.Data.SqlClient.SqlConnection ),
                        descriptions,
                        uiControls,
                        properties );
                }
                return _SqlDataProvider;
            }
        }
        private static DataProvider _SqlDataProvider;

        public static DataProvider OracleDataProvider
        {
            get {
                if ( _OracleDataProvider == null )
                {
                    Dictionary<string, string> descriptions = new Dictionary<string, string> {
                        { DataSource.OracleDataSource.Name, Dialog.Resources.Strings.DataProvider_Oracle_DataSource_Description }
                    };

                    Dictionary<string, Type> uiControls = new Dictionary<string, Type> {
                        { String.Empty, typeof( OracleConnectionUIControl ) }
                    };

                    _OracleDataProvider = new DataProvider(
                        "System.Data.OracleClient",
                        Dialog.Resources.Strings.DataProvider_Oracle,
                        Dialog.Resources.Strings.DataProvider_Oracle_Short,
                        Dialog.Resources.Strings.DataProvider_Oracle_Description,
                        // Disable OracleClient deprecation warnings
#pragma warning disable 618
                        typeof( System.Data.OracleClient.OracleConnection ),
#pragma warning restore 618
 descriptions,
                        uiControls,
                        typeof( OracleConnectionProperties ) );
                }
                return _OracleDataProvider;
            }
        }
        private static DataProvider _OracleDataProvider;

        public static DataProvider OleDBDataProvider
        {
            get {
                if ( _OleDBDataProvider == null )
                {
                    Dictionary<string, string> descriptions = new Dictionary<string, string> {
                        { DataSource.SqlDataSource.Name, Dialog.Resources.Strings.DataProvider_OleDB_SqlDataSource_Description },
                        { DataSource.OracleDataSource.Name, Dialog.Resources.Strings.DataProvider_OleDB_OracleDataSource_Description },
                        { DataSource.AccessDataSource.Name, Dialog.Resources.Strings.DataProvider_OleDB_AccessDataSource_Description }
                    };

                    Dictionary<string, Type> uiControls = new Dictionary<string, Type> {
                        { DataSource.SqlDataSource.Name, typeof( SqlConnectionUIControl ) },
                        { DataSource.OracleDataSource.Name, typeof( OracleConnectionUIControl ) },
                        { DataSource.AccessDataSource.Name, typeof( AccessConnectionUIControl ) },
                        { String.Empty, typeof( OleDBConnectionUIControl ) }
                    };

                    Dictionary<string, Type> properties = new Dictionary<string, Type> {
                        { DataSource.SqlDataSource.Name, typeof( OleDBSqlConnectionProperties ) },
                        { DataSource.OracleDataSource.Name, typeof( OleDBOracleConnectionProperties ) },
                        { DataSource.AccessDataSource.Name, typeof( OleDBAccessConnectionProperties ) },
                        { String.Empty, typeof( OleDBConnectionProperties ) }
                    };

                    _OleDBDataProvider = new DataProvider(
                        "System.Data.OleDb",
                        Dialog.Resources.Strings.DataProvider_OleDB,
                        Dialog.Resources.Strings.DataProvider_OleDB_Short,
                        Dialog.Resources.Strings.DataProvider_OleDB_Description,
                        typeof( System.Data.OleDb.OleDbConnection ),
                        descriptions,
                        uiControls,
                        properties );
                }
                return _OleDBDataProvider;
            }
        }
        private static DataProvider _OleDBDataProvider;

        public static DataProvider OdbcDataProvider
        {
            get {
                if ( _OdbcDataProvider == null )
                {
                    Dictionary<string, string> descriptions = new Dictionary<string, string> {
                        { DataSource.OdbcDataSource.Name, Dialog.Resources.Strings.DataProvider_Odbc_DataSource_Description }
                    };

                    Dictionary<string, Type> uiControls = new Dictionary<string, Type> {
                        { String.Empty, typeof( OdbcConnectionUIControl ) }
                    };

                    _OdbcDataProvider = new DataProvider(
                        "System.Data.Odbc",
                        Dialog.Resources.Strings.DataProvider_Odbc,
                        Dialog.Resources.Strings.DataProvider_Odbc_Short,
                        Dialog.Resources.Strings.DataProvider_Odbc_Description,
                        typeof( System.Data.Odbc.OdbcConnection ),
                        descriptions,
                        uiControls,
                        typeof( OdbcConnectionProperties ) );
                }
                return _OdbcDataProvider;
            }
        }
        private static DataProvider _OdbcDataProvider;

        public string Name { get; }

        public string DisplayName => this._DisplayName ?? this.Name;

        public string ShortDisplayName { get; }

        public string Description => this.GetDescription( null );

        public Type TargetConnectionType { get; }

        public virtual string GetDescription( DataSource dataSource )
        {
            return this._DataSourceDescriptions != null && dataSource != null &&
                this._DataSourceDescriptions.ContainsKey( dataSource.Name )
                ? this._DataSourceDescriptions[dataSource.Name]
                : this._Description;
        }

        public IDataConnectionUIControl CreateConnectionUIControl()
        {
            return this.CreateConnectionUIControl( null );
        }

        public virtual IDataConnectionUIControl CreateConnectionUIControl( DataSource dataSource )
        {
            string key;
            return this._ConnectionUIControlTypes != null &&
                (dataSource != null && this._ConnectionUIControlTypes.ContainsKey( key = dataSource.Name )) ||
                this._ConnectionUIControlTypes.ContainsKey( key = String.Empty )
                ? Activator.CreateInstance( this._ConnectionUIControlTypes[key] ) as IDataConnectionUIControl
                : null;
        }

        public IDataConnectionProperties CreateConnectionProperties()
        {
            return this.CreateConnectionProperties( null );
        }

        public virtual IDataConnectionProperties CreateConnectionProperties( DataSource dataSource )
        {
            string key;
            return this._ConnectionPropertiesTypes != null &&
                ((dataSource != null && this._ConnectionPropertiesTypes.ContainsKey( key = dataSource.Name )) ||
                this._ConnectionPropertiesTypes.ContainsKey( key = String.Empty ))
                ? Activator.CreateInstance( this._ConnectionPropertiesTypes[key] ) as IDataConnectionProperties
                : null;
        }

        private readonly string _DisplayName;
        private readonly string _Description;
        private readonly IDictionary<string, string> _DataSourceDescriptions;
        private readonly IDictionary<string, Type> _ConnectionUIControlTypes;
        private readonly IDictionary<string, Type> _ConnectionPropertiesTypes;
    }
}
