//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace Microsoft.Data.ConnectionUI
{
    public class DataSource
    {
        public const string MicrosoftSqlServerFileName = "MicrosoftSqlServerFile";

        private DataSource()
        {
            this._DisplayName = Dialog.Resources.Strings.DataSource_UnspecifiedDisplayName;
            this.Providers = new DataProviderCollection( this );
        }

        public DataSource( string name, string displayName )
        {
            this.Name = name ?? throw new ArgumentNullException( nameof( name ) );
            this._DisplayName = displayName;
            this.Providers = new DataProviderCollection( this );
        }

        public static void AddStandardDataSources( DataConnectionDialog dialog )
        {
            dialog.DataSources.Add( DataSource.SqlDataSource );
            dialog.DataSources.Add( DataSource.SqlFileDataSource );
            dialog.DataSources.Add( DataSource.OracleDataSource );
            dialog.DataSources.Add( DataSource.AccessDataSource );
            dialog.DataSources.Add( DataSource.OdbcDataSource );
            dialog.UnspecifiedDataSource.Providers.Add( DataProvider.SqlDataProvider );
            dialog.UnspecifiedDataSource.Providers.Add( DataProvider.OracleDataProvider );
            dialog.UnspecifiedDataSource.Providers.Add( DataProvider.OleDBDataProvider );
            dialog.UnspecifiedDataSource.Providers.Add( DataProvider.OdbcDataProvider );
            dialog.DataSources.Add( dialog.UnspecifiedDataSource );
        }

        public static DataSource SqlDataSource
        {
            get {
                if ( _SqlDataSource == null )
                {
                    _SqlDataSource = new DataSource( "MicrosoftSqlServer", Dialog.Resources.Strings.DataSource_MicrosoftSqlServer );
                    _SqlDataSource.Providers.Add( DataProvider.SqlDataProvider );
                    _SqlDataSource.Providers.Add( DataProvider.OleDBDataProvider );
                    _SqlDataSource.DefaultProvider = DataProvider.SqlDataProvider;
                }
                return _SqlDataSource;
            }
        }
        private static DataSource _SqlDataSource;

        public static DataSource SqlFileDataSource
        {
            get {
                if ( _SqlFileDataSource == null )
                {
                    _SqlFileDataSource = new DataSource( "MicrosoftSqlServerFile", Dialog.Resources.Strings.DataSource_MicrosoftSqlServerFile );
                    _SqlFileDataSource.Providers.Add( DataProvider.SqlDataProvider );
                }
                return _SqlFileDataSource;
            }
        }
        private static DataSource _SqlFileDataSource;

        public static DataSource OracleDataSource
        {
            get {
                if ( _OracleDataSource == null )
                {
                    _OracleDataSource = new DataSource( "Oracle", Dialog.Resources.Strings.DataSource_Oracle );
                    _OracleDataSource.Providers.Add( DataProvider.OracleDataProvider );
                    _OracleDataSource.Providers.Add( DataProvider.OleDBDataProvider );
                    _OracleDataSource.DefaultProvider = DataProvider.OracleDataProvider;
                }
                return _OracleDataSource;
            }
        }
        private static DataSource _OracleDataSource;

        public static DataSource AccessDataSource
        {
            get {
                if ( _AccessDataSource == null )
                {
                    _AccessDataSource = new DataSource( "MicrosoftAccess", Dialog.Resources.Strings.DataSource_MicrosoftAccess );
                    _AccessDataSource.Providers.Add( DataProvider.OleDBDataProvider );
                }
                return _AccessDataSource;
            }
        }
        private static DataSource _AccessDataSource;

        public static DataSource OdbcDataSource
        {
            get {
                if ( _OdbcDataSource == null )
                {
                    _OdbcDataSource = new DataSource( "OdbcDsn", Dialog.Resources.Strings.DataSource_MicrosoftOdbcDsn );
                    _OdbcDataSource.Providers.Add( DataProvider.OdbcDataProvider );
                }
                return _OdbcDataSource;
            }
        }
        private static DataSource _OdbcDataSource;

        public string Name { get; }

        public string DisplayName => this._DisplayName ?? this.Name;

        public DataProvider DefaultProvider
        {
            get {
                switch ( this.Providers.Count )
                {
                    case 0:
                        Debug.Assert( this._DefaultProvider == null );
                        return null;
                    case 1:
                        // If there is only one data provider, it must be the default
                        IEnumerator<DataProvider> e = this.Providers.GetEnumerator();
                        _ = e.MoveNext();
                        return e.Current;
                    default:
                        return (this.Name != null) ? this._DefaultProvider : null;
                }
            }
            set {
                if ( this.Providers.Count == 1 && this._DefaultProvider != value )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataSource_CannotChangeSingleDataProvider );
                }
                if ( value != null && !this.Providers.Contains( value ) )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataSource_DataProviderNotFound );
                }
                this._DefaultProvider = value;
            }
        }

        public ICollection<DataProvider> Providers { get; }

        internal static DataSource CreateUnspecified()
        {
            return new DataSource();
        }

        private class DataProviderCollection : ICollection<DataProvider>
        {
            public DataProviderCollection( DataSource source )
            {
                Debug.Assert( source != null );

                this._List = new List<DataProvider>();
                this._Source = source;
            }

            public int Count => this._List.Count;

            public bool IsReadOnly => false;

            public void Add( DataProvider item )
            {
                if ( item == null )
                {
                    throw new ArgumentNullException( nameof( item ) );
                }
                if ( !this._List.Contains( item ) )
                {
                    this._List.Add( item );
                }
            }

            public bool Contains( DataProvider item )
            {
                return this._List.Contains( item );
            }

            public bool Remove( DataProvider item )
            {
                bool result = this._List.Remove( item );
                if ( item == this._Source._DefaultProvider )
                {
                    this._Source._DefaultProvider = null;
                }
                return result;
            }

            public void Clear()
            {
                this._List.Clear();
                this._Source._DefaultProvider = null;
            }

            public void CopyTo( DataProvider[] array, int arrayIndex )
            {
                this._List.CopyTo( array, arrayIndex );
            }

            public IEnumerator<DataProvider> GetEnumerator()
            {
                return this._List.GetEnumerator();
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return this._List.GetEnumerator();
            }

            private readonly ICollection<DataProvider> _List;
            private readonly DataSource _Source;
        }

        private readonly string _DisplayName;
        private DataProvider _DefaultProvider;
    }
}
