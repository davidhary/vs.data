//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.ComponentModel;
using System.Windows.Forms.Design;
using Microsoft.Win32;

namespace Microsoft.Data.ConnectionUI
{
    internal sealed class UserPreferenceChangedHandler : IComponent
    {
        public UserPreferenceChangedHandler( Form form )
        {
            Debug.Assert( form != null );
            SystemEvents.UserPreferenceChanged += new UserPreferenceChangedEventHandler( this.HandleUserPreferenceChanged );
            this._form = form;
        }

        ~UserPreferenceChangedHandler()
        {
            this.Dispose( false );
        }

        public ISite Site
        {
            get => this._form.Site;
            set {
                // This shouldn't be called
            }
        }

        public event EventHandler Disposed;

        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        private void HandleUserPreferenceChanged( object sender, UserPreferenceChangedEventArgs e )
        {
            // Need to update the font
            IUIService uiService = (this._form.Site != null) ? this._form.Site.GetService( typeof( IUIService ) ) as IUIService : null;
            if ( uiService != null )
            {
                if ( uiService.Styles["DialogFont"] is Font newFont )
                {
                    this._form.Font = newFont;
                }
            }
        }

        private void Dispose( bool disposing )
        {
            if ( disposing )
            {
                SystemEvents.UserPreferenceChanged -= new UserPreferenceChangedEventHandler( this.HandleUserPreferenceChanged );
                if ( Disposed != null )
                {
                    Disposed( this, EventArgs.Empty );
                }
            }
        }

        private Form _form;
    }
}
