//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;

namespace Microsoft.Data.ConnectionUI
{
    public partial class AccessConnectionUIControl : UserControl, IDataConnectionUIControl
    {
        public AccessConnectionUIControl()
        {
            this.InitializeComponent();
            this.RightToLeft = RightToLeft.Inherit;

            int requiredHeight = LayoutUtils.GetPreferredCheckBoxHeight( this.savePasswordCheckBox );
            if ( this.savePasswordCheckBox.Height < requiredHeight )
            {
                this.savePasswordCheckBox.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom;
                this.loginTableLayoutPanel.Height += this.loginTableLayoutPanel.Margin.Bottom;
                this.loginTableLayoutPanel.Margin = new Padding( this.loginTableLayoutPanel.Margin.Left, this.loginTableLayoutPanel.Margin.Top, this.loginTableLayoutPanel.Margin.Right, 0 );
            }
        }

        public void Initialize( IDataConnectionProperties connectionProperties )
        {
            if ( connectionProperties == null )
            {
                throw new ArgumentNullException( nameof( connectionProperties ) );
            }

            if ( !(connectionProperties is OleDBAccessConnectionProperties) )
            {
                throw new ArgumentException( Dialog.Resources.Strings.AccessConnectionUIControl_InvalidConnectionProperties );
            }

            if ( connectionProperties is OdbcConnectionProperties )
            {
                // ODBC does not support saving the password
                this.savePasswordCheckBox.Enabled = false;
            }

            this.Properties = connectionProperties;
        }

        public void LoadProperties()
        {
            this._Loading = true;

            this.databaseFileTextBox.Text = this.Properties[this.DatabaseFileProperty] as string;
            this.userNameTextBox.Text = this.Properties[this.UserNameProperty] as string;
            if ( this.userNameTextBox.Text.Length == 0 )
            {
                this.userNameTextBox.Text = "Admin";
            }
            this.passwordTextBox.Text = this.Properties[this.PasswordProperty] as string;
            this.savePasswordCheckBox.Checked = !(this.Properties is OdbcConnectionProperties) ? ( bool ) this.Properties["Persist Security Info"] : false;

            this._Loading = false;
        }

        // Simulate RTL mirroring
        protected override void OnRightToLeftChanged( EventArgs e )
        {
            base.OnRightToLeftChanged( e );
            if ( this.ParentForm != null &&
                this.ParentForm.RightToLeftLayout == true &&
                this.RightToLeft == RightToLeft.Yes )
            {
                LayoutUtils.MirrorControl( this.databaseFileLabel, this.databaseFileTableLayoutPanel );
            }
            else
            {
                LayoutUtils.UnmirrorControl( this.databaseFileLabel, this.databaseFileTableLayoutPanel );
            }
        }

        protected override void ScaleControl( SizeF factor, BoundsSpecified specified )
        {
            Size baseSize = this.Size;
            this.MinimumSize = Size.Empty;
            base.ScaleControl( factor, specified );
            this.MinimumSize = new Size(
                ( int ) Math.Round( ( float ) baseSize.Width * factor.Width ),
                ( int ) Math.Round( ( float ) baseSize.Height * factor.Height ) );
        }

        protected override void OnParentChanged( EventArgs e )
        {
            base.OnParentChanged( e );
            if ( this.Parent == null )
            {
                this.OnFontChanged( e );
            }
        }

        private void SetDatabaseFile( object sender, System.EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties[this.DatabaseFileProperty] = (this.databaseFileTextBox.Text.Trim().Length > 0) ? this.databaseFileTextBox.Text.Trim() : null;
            }
        }

        private void Browse( object sender, System.EventArgs e )
        {
            OpenFileDialog fileDialog = new OpenFileDialog {
                Title = Dialog.Resources.Strings.AccessConnectionUIControl_BrowseFileTitle,
                Multiselect = false,
                RestoreDirectory = true,
                Filter = Dialog.Resources.Strings.AccessConnectionUIControl_BrowseFileFilter,
                DefaultExt = Dialog.Resources.Strings.AccessConnectionUIControl_BrowseFileDefaultExt
            };
            if ( this.Container != null )
            {
                this.Container.Add( fileDialog );
            }
            try
            {
                DialogResult result = fileDialog.ShowDialog( this.ParentForm );
                if ( result == DialogResult.OK )
                {
                    this.databaseFileTextBox.Text = fileDialog.FileName.Trim();
                }
            }
            finally
            {
                if ( this.Container != null )
                {
                    this.Container.Remove( fileDialog );
                }
                fileDialog.Dispose();
            }
        }

        private void SetUserName( object sender, EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties[this.UserNameProperty] = (this.userNameTextBox.Text.Trim().Length > 0) ? this.userNameTextBox.Text.Trim() : null;
                if ( (this.Properties[this.UserNameProperty] as string).Equals( "Admin" ) )
                {
                    this.Properties[this.UserNameProperty] = null;
                }
            }
        }

        private void SetPassword( object sender, EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties[this.PasswordProperty] = (this.passwordTextBox.Text.Length > 0) ? this.passwordTextBox.Text : null;
                this.passwordTextBox.Text = this.passwordTextBox.Text; // forces reselection of all text
            }
        }

        private void SetSavePassword( object sender, EventArgs e )
        {
            if ( !this._Loading )
            {
                this.Properties["Persist Security Info"] = this.savePasswordCheckBox.Checked;
            }
        }

        private void TrimControlText( object sender, EventArgs e )
        {
            Control c = sender as Control;
            c.Text = c.Text.Trim();
        }

        private string DatabaseFileProperty => !(this.Properties is OdbcConnectionProperties) ? "Data Source" : "DBQ";

        private string UserNameProperty => !(this.Properties is OdbcConnectionProperties) ? "User ID" : "UID";

        private string PasswordProperty => !(this.Properties is OdbcConnectionProperties) ? "Jet OLEDB:Database Password" : "PWD";

        private IDataConnectionProperties Properties { get; set; }

        private bool _Loading;
    }
}
