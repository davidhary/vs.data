//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Security;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms.Design;
using System.Security.Permissions;
using System.Runtime.InteropServices;

namespace Microsoft.Data.ConnectionUI
{
    public partial class DataConnectionDialog : Form
    {
        public DataConnectionDialog()
        {
            this.InitializeComponent();
            this.dataSourceTextBox.Width = 0;

            // Make sure we handle a user preference change
            this.components.Add( new UserPreferenceChangedHandler( this ) );

            // Configure initial label values
            ComponentResourceManager resources = new ComponentResourceManager( typeof( DataConnectionSourceDialog ) );
            this._ChooseDataSourceTitle = resources.GetString( "$this.Text" );
            this._ChooseDataSourceAcceptText = resources.GetString( "okButton.Text" );
            this._ChangeDataSourceTitle = Dialog.Resources.Strings.DataConnectionDialog_ChangeDataSourceTitle;

            this.DataSources = new DataSourceCollection( this );
        }

        public static DialogResult Show( DataConnectionDialog dialog )
        {

            return Show( dialog, null );
        }

        public static DialogResult Show( DataConnectionDialog dialog, IWin32Window owner )
        {
            if ( dialog == null )
            {
                throw new ArgumentNullException( nameof( dialog ) );
            }
            if ( dialog.DataSources.Count == 0 )
            {
                throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_NoDataSourcesAvailable );
            }
            foreach ( DataSource dataSource in dialog.DataSources )
            {
                if ( dataSource.Providers.Count == 0 )
                {
                    throw new InvalidOperationException( String.Format( Dialog.Resources.Strings.DataConnectionDialog_NoDataProvidersForDataSource + dataSource.DisplayName.Replace( "'", "''" ) ) );
                }
            }

            Application.ThreadException += new ThreadExceptionEventHandler( dialog.HandleDialogException );
            dialog._ShowingDialog = true;
            try
            {
                // If there is no selected data source or provider, show the data connection source dialog
                if ( dialog.SelectedDataSource == null || dialog.SelectedDataProvider == null )
                {
                    DataConnectionSourceDialog sourceDialog = new DataConnectionSourceDialog( dialog ) {
                        Title = dialog.ChooseDataSourceTitle,
                        HeaderLabel = dialog.ChooseDataSourceHeaderLabel
                    };
                    (sourceDialog.AcceptButton as Button).Text = dialog.ChooseDataSourceAcceptText;
                    if ( dialog.Container != null )
                    {
                        dialog.Container.Add( sourceDialog );
                    }
                    try
                    {
                        if ( owner == null )
                        {
                            sourceDialog.StartPosition = FormStartPosition.CenterScreen;
                        }
                        _ = sourceDialog.ShowDialog( owner );
                        if ( dialog.SelectedDataSource == null || dialog.SelectedDataProvider == null )
                        {
                            return DialogResult.Cancel;
                        }
                    }
                    finally
                    {
                        if ( dialog.Container != null )
                        {
                            dialog.Container.Remove( sourceDialog );
                        }
                        sourceDialog.Dispose();
                    }
                }
                else
                {
                    dialog.SaveSelection = false;
                }
                if ( owner == null )
                {
                    dialog.StartPosition = FormStartPosition.CenterScreen;
                }
                for (; ; )
                {
                    DialogResult result = dialog.ShowDialog( owner );
                    if ( result == DialogResult.Ignore )
                    {
                        DataConnectionSourceDialog sourceDialog = new DataConnectionSourceDialog( dialog ) {
                            Title = dialog.ChangeDataSourceTitle,
                            HeaderLabel = dialog.ChangeDataSourceHeaderLabel
                        };
                        if ( dialog.Container != null )
                        {
                            dialog.Container.Add( sourceDialog );
                        }
                        try
                        {
                            if ( owner == null )
                            {
                                sourceDialog.StartPosition = FormStartPosition.CenterScreen;
                            }
                            result = sourceDialog.ShowDialog( owner );
                        }
                        finally
                        {
                            if ( dialog.Container != null )
                            {
                                dialog.Container.Remove( sourceDialog );
                            }
                            sourceDialog.Dispose();
                        }
                    }
                    else
                    {
                        return result;
                    }
                }
            }
            finally
            {
                dialog._ShowingDialog = false;
                Application.ThreadException -= new ThreadExceptionEventHandler( dialog.HandleDialogException );
            }
        }

        public string Title
        {
            get => this.Text;
            set => this.Text = value;
        }

        public string HeaderLabel
        {
            get => (this._HeaderLabel != null) ? this._HeaderLabel.Text : String.Empty;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( this._HeaderLabel == null && (value == null || value.Length == 0) )
                {
                    return;
                }
                if ( this._HeaderLabel != null && value == this._HeaderLabel.Text )
                {
                    return;
                }
                if ( value != null && value.Length > 0 )
                {
                    if ( this._HeaderLabel == null )
                    {
                        this._HeaderLabel = new Label {
                            Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                            FlatStyle = FlatStyle.System,
                            Location = new Point( 12, 12 ),
                            Margin = new Padding( 3 ),
                            Name = "dataSourceLabel",
                            Width = this.dataSourceTableLayoutPanel.Width,
                            TabIndex = 100
                        };
                        this.Controls.Add( this._HeaderLabel );
                    }
                    this._HeaderLabel.Text = value;
                    this.MinimumSize = Size.Empty;
                    this._HeaderLabel.Height = LayoutUtils.GetPreferredLabelHeight( this._HeaderLabel );
                    int dy =
                        this._HeaderLabel.Bottom +
                        this._HeaderLabel.Margin.Bottom +
                        this.dataSourceLabel.Margin.Top -
                        this.dataSourceLabel.Top;
                    this.containerControl.Anchor &= ~AnchorStyles.Bottom;
                    this.Height += dy;
                    this.containerControl.Anchor |= AnchorStyles.Bottom;
                    this.containerControl.Top += dy;
                    this.dataSourceTableLayoutPanel.Top += dy;
                    this.dataSourceLabel.Top += dy;
                    this.MinimumSize = this.Size;
                }
                else
                {
                    if ( this._HeaderLabel != null )
                    {
                        int dy = this._HeaderLabel.Height;
                        try
                        {
                            this.Controls.Remove( this._HeaderLabel );
                        }
                        finally
                        {
                            this._HeaderLabel.Dispose();
                            this._HeaderLabel = null;
                        }
                        this.MinimumSize = Size.Empty;
                        this.dataSourceLabel.Top -= dy;
                        this.dataSourceTableLayoutPanel.Top -= dy;
                        this.containerControl.Top -= dy;
                        this.containerControl.Anchor &= ~AnchorStyles.Bottom;
                        this.Height -= dy;
                        this.containerControl.Anchor |= AnchorStyles.Bottom;
                        this.MinimumSize = this.Size;
                    }
                }
            }
        }

        public bool TranslateHelpButton { get; set; } = true;

        public string ChooseDataSourceTitle
        {
            get => this._ChooseDataSourceTitle;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( value == null )
                {
                    value = String.Empty;
                }
                if ( value == this._ChooseDataSourceTitle )
                {
                    return;
                }
                this._ChooseDataSourceTitle = value;
            }
        }

        public string ChooseDataSourceHeaderLabel
        {
            get => this._ChooseDataSourceHeaderLabel;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( value == null )
                {
                    value = String.Empty;
                }
                if ( value == this._ChooseDataSourceHeaderLabel )
                {
                    return;
                }
                this._ChooseDataSourceHeaderLabel = value;
            }
        }

        public string ChooseDataSourceAcceptText
        {
            get => this._ChooseDataSourceAcceptText;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( value == null )
                {
                    value = String.Empty;
                }
                if ( value == this._ChooseDataSourceAcceptText )
                {
                    return;
                }
                this._ChooseDataSourceAcceptText = value;
            }
        }

        public string ChangeDataSourceTitle
        {
            get => this._ChangeDataSourceTitle;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( value == null )
                {
                    value = String.Empty;
                }
                if ( value == this._ChangeDataSourceTitle )
                {
                    return;
                }
                this._ChangeDataSourceTitle = value;
            }
        }

        public string ChangeDataSourceHeaderLabel
        {
            get => this._ChangeDataSourceHeaderLabel;
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( value == null )
                {
                    value = String.Empty;
                }
                if ( value == this._ChangeDataSourceHeaderLabel )
                {
                    return;
                }
                this._ChangeDataSourceHeaderLabel = value;
            }
        }

        public ICollection<DataSource> DataSources { get; }

        public DataSource UnspecifiedDataSource { get; } = DataSource.CreateUnspecified();

        public DataSource SelectedDataSource
        {
            get {
                if ( this.DataSources == null )
                {
                    return null;
                }
                switch ( this.DataSources.Count )
                {
                    case 0:
                        Debug.Assert( this._SelectedDataSource == null );
                        return null;
                    case 1:
                        // If there is only one data source, it must be selected
                        IEnumerator<DataSource> e = this.DataSources.GetEnumerator();
                        _ = e.MoveNext();
                        return e.Current;
                    default:
                        return this._SelectedDataSource;
                }
            }
            set {
                if ( this.SelectedDataSource != value )
                {
                    if ( this._ShowingDialog )
                    {
                        throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                    }
                    this.SetSelectedDataSource( value, false );
                }
            }
        }

        public DataProvider SelectedDataProvider
        {
            get => this.GetSelectedDataProvider( this.SelectedDataSource );
            set {
                if ( this.SelectedDataProvider != value )
                {
                    if ( this.SelectedDataSource == null )
                    {
                        throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_NoDataSourceSelected );
                    }
                    this.SetSelectedDataProvider( this.SelectedDataSource, value );
                }
            }
        }

        public DataProvider GetSelectedDataProvider( DataSource dataSource )
        {
            if ( dataSource == null )
            {
                return null;
            }
            switch ( dataSource.Providers.Count )
            {
                case 0:
                    return null;
                case 1:
                    // If there is only one data provider, it must be selected
                    IEnumerator<DataProvider> e = dataSource.Providers.GetEnumerator();
                    _ = e.MoveNext();
                    return e.Current;
                default:
                    return (this._DataProviderSelections.ContainsKey( dataSource )) ? this._DataProviderSelections[dataSource] : dataSource.DefaultProvider;
            }
        }

        public void SetSelectedDataProvider( DataSource dataSource, DataProvider dataProvider )
        {
            if ( this.GetSelectedDataProvider( dataSource ) != dataProvider )
            {
                if ( dataSource == null )
                {
                    throw new ArgumentNullException( nameof( dataSource ) );
                }
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                this.SetSelectedDataProvider( dataSource, dataProvider, false );
            }
        }

        public bool SaveSelection { get; set; } = true;

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public string DisplayConnectionString
        {
            get {
                string s = null;
                if ( this.ConnectionProperties != null )
                {
                    try
                    {
                        s = this.ConnectionProperties.ToDisplayString();
                    }
                    catch { }
                }
                return s ?? String.Empty;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public string ConnectionString
        {
            get {
                string s = null;
                if ( this.ConnectionProperties != null )
                {
                    try
                    {
                        s = this.ConnectionProperties.ToString();
                    }
                    catch { }
                }
                return s ?? String.Empty;
            }
            set {
                if ( this._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( this.SelectedDataProvider == null )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_NoDataProviderSelected );
                }
                Debug.Assert( this.ConnectionProperties != null );
                if ( this.ConnectionProperties != null )
                {
                    this.ConnectionProperties.Parse( value );
                }
            }
        }

        public string AcceptButtonText
        {
            get => this.acceptButton.Text;
            set => this.acceptButton.Text = value;
        }

        public event EventHandler VerifySettings;

        public event EventHandler<ContextHelpEventArgs> ContextHelpRequested;

        public event ThreadExceptionEventHandler DialogException;

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        internal UserControl ConnectionUIControl
        {
            get {
                if ( this.SelectedDataProvider == null )
                {
                    return null;
                }
                if ( !this._ConnectionUIControlTable.ContainsKey( this.SelectedDataSource ) )
                {
                    this._ConnectionUIControlTable[this.SelectedDataSource] = new Dictionary<DataProvider, IDataConnectionUIControl>();
                }
                if ( !this._ConnectionUIControlTable[this.SelectedDataSource].ContainsKey( this.SelectedDataProvider ) )
                {
                    IDataConnectionUIControl uiControl = null;
                    UserControl control = null;
                    try
                    {
                        uiControl = this.SelectedDataSource == this.UnspecifiedDataSource
                            ? this.SelectedDataProvider.CreateConnectionUIControl()
                            : this.SelectedDataProvider.CreateConnectionUIControl( this.SelectedDataSource );
                        control = uiControl as UserControl;
                        if ( control == null )
                        {
                            if ( uiControl is IContainerControl ctControl )
                            {
                                control = ctControl.ActiveControl as UserControl;
                            }
                        }
                    }
                    catch { }
                    if ( uiControl == null || control == null )
                    {
                        uiControl = new PropertyGridUIControl();
                        control = uiControl as UserControl;
                    }
                    control.Location = Point.Empty;
                    control.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                    control.AutoSize = false;
                    try
                    {
                        uiControl.Initialize( this.ConnectionProperties );
                    }
                    catch { }
                    this._ConnectionUIControlTable[this.SelectedDataSource][this.SelectedDataProvider] = uiControl;
                    this.components.Add( control ); // so that it is disposed when the form is disposed
                }
                if ( !(this._ConnectionUIControlTable[this.SelectedDataSource][this.SelectedDataProvider] is UserControl result) )
                {
                    result = (this._ConnectionUIControlTable[this.SelectedDataSource][this.SelectedDataProvider] as IContainerControl).ActiveControl as UserControl;
                }
                return result;
            }
        }

        internal IDataConnectionProperties ConnectionProperties
        {
            get {
                if ( this.SelectedDataProvider == null )
                {
                    return null;
                }
                if ( !this._ConnectionPropertiesTable.ContainsKey( this.SelectedDataSource ) )
                {
                    this._ConnectionPropertiesTable[this.SelectedDataSource] = new Dictionary<DataProvider, IDataConnectionProperties>();
                }
                if ( !this._ConnectionPropertiesTable[this.SelectedDataSource].ContainsKey( this.SelectedDataProvider ) )
                {
                    IDataConnectionProperties properties = this.SelectedDataSource == this.UnspecifiedDataSource
                        ? this.SelectedDataProvider.CreateConnectionProperties()
                        : this.SelectedDataProvider.CreateConnectionProperties( this.SelectedDataSource );
                    if ( properties == null )
                    {
                        properties = new BasicConnectionProperties();
                    }
                    properties.PropertyChanged += new EventHandler( this.ConfigureAcceptButton );
                    this._ConnectionPropertiesTable[this.SelectedDataSource][this.SelectedDataProvider] = properties;
                }
                return this._ConnectionPropertiesTable[this.SelectedDataSource][this.SelectedDataProvider];
            }
        }

        protected override void OnLoad( EventArgs e )
        {
            if ( !this._ShowingDialog )
            {
                throw new NotSupportedException( Dialog.Resources.Strings.DataConnectionDialog_ShowDialogNotSupported );
            }
            this.ConfigureDataSourceTextBox();
            this.ConfigureChangeDataSourceButton();
            this.ConfigureContainerControl();
            this.ConfigureAcceptButton( this, EventArgs.Empty );
            base.OnLoad( e );
        }

        protected override void OnShown( EventArgs e )
        {
            base.OnShown( e );

            // Set focus to the connection UI control (if any)
            if ( this.ConnectionUIControl != null )
            {
                _ = this.ConnectionUIControl.Focus();
            }
        }

        protected override void OnFontChanged( EventArgs e )
        {
            base.OnFontChanged( e );

            this.dataSourceTableLayoutPanel.Anchor &= ~AnchorStyles.Right;
            this.containerControl.Anchor &= ~AnchorStyles.Right & ~AnchorStyles.Bottom;
            this.advancedButton.Anchor |= AnchorStyles.Top | AnchorStyles.Left;
            this.advancedButton.Anchor &= ~AnchorStyles.Right & ~AnchorStyles.Bottom;
            this.separatorPanel.Anchor |= AnchorStyles.Top;
            this.separatorPanel.Anchor &= ~AnchorStyles.Right & ~AnchorStyles.Bottom;
            this.testConnectionButton.Anchor |= AnchorStyles.Top;
            this.testConnectionButton.Anchor &= ~AnchorStyles.Bottom;
            this.buttonsTableLayoutPanel.Anchor |= AnchorStyles.Top | AnchorStyles.Left;
            this.buttonsTableLayoutPanel.Anchor &= ~AnchorStyles.Right & ~AnchorStyles.Bottom;
            Size properSize = new Size(
                this.containerControl.Right +
                this.containerControl.Margin.Right +
                this.Padding.Right,
                this.buttonsTableLayoutPanel.Bottom +
                this.buttonsTableLayoutPanel.Margin.Bottom +
                this.Padding.Bottom );
            properSize = this.SizeFromClientSize( properSize );
            Size dsize = this.Size - properSize;
            this.MinimumSize -= dsize;
            this.Size -= dsize;
            this.buttonsTableLayoutPanel.Anchor |= AnchorStyles.Right | AnchorStyles.Bottom;
            this.buttonsTableLayoutPanel.Anchor &= ~AnchorStyles.Top & ~AnchorStyles.Left;
            this.testConnectionButton.Anchor |= AnchorStyles.Bottom;
            this.testConnectionButton.Anchor &= ~AnchorStyles.Top;
            this.separatorPanel.Anchor |= AnchorStyles.Right | AnchorStyles.Bottom;
            this.separatorPanel.Anchor &= ~AnchorStyles.Top;
            this.advancedButton.Anchor |= AnchorStyles.Right | AnchorStyles.Bottom;
            this.advancedButton.Anchor &= ~AnchorStyles.Top & ~AnchorStyles.Left;
            this.containerControl.Anchor |= AnchorStyles.Right | AnchorStyles.Bottom;
            this.dataSourceTableLayoutPanel.Anchor |= AnchorStyles.Right;
        }

        protected virtual void OnVerifySettings( EventArgs e )
        {
            if ( VerifySettings != null )
            {
                VerifySettings( this, e );
            }
        }

        protected internal virtual void OnContextHelpRequested( ContextHelpEventArgs e )
        {
            if ( ContextHelpRequested != null )
            {
                ContextHelpRequested( this, e );
            }
            if ( e.Handled == false )
            {
                this.ShowError( null, Dialog.Resources.Strings.DataConnectionDialog_NoHelpAvailable );
                e.Handled = true;
            }
        }

        protected override void OnHelpRequested( HelpEventArgs hevent )
        {
            // Get the active control
            Control activeControl = this;
            while ( activeControl is ContainerControl containrControl &&
                containrControl != this.ConnectionUIControl &&
                containrControl.ActiveControl != null )
            {
                activeControl = containrControl.ActiveControl;
            }

            // Figure out the context
            DataConnectionDialogContext context = DataConnectionDialogContext.Main;
            if ( activeControl == this.dataSourceTextBox )
            {
                context = DataConnectionDialogContext.MainDataSourceTextBox;
            }
            if ( activeControl == this.changeDataSourceButton )
            {
                context = DataConnectionDialogContext.MainChangeDataSourceButton;
            }
            if ( activeControl == this.ConnectionUIControl )
            {
                context = DataConnectionDialogContext.MainConnectionUIControl;
                if ( this.ConnectionUIControl is SqlConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainSqlConnectionUIControl;
                }
                if ( this.ConnectionUIControl is SqlFileConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainSqlFileConnectionUIControl;
                }
                if ( this.ConnectionUIControl is OracleConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainOracleConnectionUIControl;
                }
                if ( this.ConnectionUIControl is AccessConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainAccessConnectionUIControl;
                }
                if ( this.ConnectionUIControl is OleDBConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainOleDBConnectionUIControl;
                }
                if ( this.ConnectionUIControl is OdbcConnectionUIControl )
                {
                    context = DataConnectionDialogContext.MainOdbcConnectionUIControl;
                }
                if ( this.ConnectionUIControl is PropertyGridUIControl )
                {
                    context = DataConnectionDialogContext.MainGenericConnectionUIControl;
                }
            }
            if ( activeControl == this.advancedButton )
            {
                context = DataConnectionDialogContext.MainAdvancedButton;
            }
            if ( activeControl == this.testConnectionButton )
            {
                context = DataConnectionDialogContext.MainTestConnectionButton;
            }
            if ( activeControl == this.acceptButton )
            {
                context = DataConnectionDialogContext.MainAcceptButton;
            }
            if ( activeControl == this.cancelButton )
            {
                context = DataConnectionDialogContext.MainCancelButton;
            }

            // Call OnContextHelpRequested
            ContextHelpEventArgs e = new ContextHelpEventArgs( context, hevent.MousePos );
            this.OnContextHelpRequested( e );
            hevent.Handled = e.Handled;
            if ( !e.Handled )
            {
                base.OnHelpRequested( hevent );
            }
        }

        protected virtual void OnDialogException( ThreadExceptionEventArgs e )
        {
            if ( DialogException != null )
            {
                DialogException( this, e );
            }
            else
            {
                this.ShowError( null, e.Exception );
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        protected override void OnFormClosing( FormClosingEventArgs e )
        {
            if ( this.DialogResult == DialogResult.OK )
            {
                try
                {
                    this.OnVerifySettings( EventArgs.Empty );
                }
                catch ( Exception ex )
                {
                    if ( !(ex is ExternalException exex) || exex.ErrorCode != NativeMethods.DB_E_CANCELED )
                    {
                        this.ShowError( null, ex );
                    }
                    e.Cancel = true;
                }
            }

            base.OnFormClosing( e );
        }

        [SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode )]
        protected override void WndProc( ref Message m )
        {
            if ( this.TranslateHelpButton && HelpUtils.IsContextHelpMessage( ref m ) )
            {
                // Force the ? in the title bar to invoke the help topic
                HelpUtils.TranslateContextHelpMessage( this, ref m );
                base.DefWndProc( ref m ); // pass to the active control
                return;
            }
            base.WndProc( ref m );
        }

        internal void SetSelectedDataSourceInternal( DataSource value )
        {
            this.SetSelectedDataSource( value, false );
        }

        internal void SetSelectedDataProviderInternal( DataSource dataSource, DataProvider value )
        {
            this.SetSelectedDataProvider( dataSource, value, false );
        }

        private void SetSelectedDataSource( DataSource value, bool noSingleItemCheck )
        {
            if ( !noSingleItemCheck && this.DataSources.Count == 1 && this._SelectedDataSource != value )
            {
                IEnumerator<DataSource> e = this.DataSources.GetEnumerator();
                _ = e.MoveNext();
                if ( value != e.Current )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotChangeSingleDataSource );
                }
            }
            if ( this._SelectedDataSource != value )
            {
                if ( value != null )
                {
                    if ( !this.DataSources.Contains( value ) )
                    {
                        throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_DataSourceNotFound );
                    }
                    this._SelectedDataSource = value;
                    switch ( this._SelectedDataSource.Providers.Count )
                    {
                        case 0:
                            this.SetSelectedDataProvider( this._SelectedDataSource, null, noSingleItemCheck );
                            break;
                        case 1:
                            IEnumerator<DataProvider> e = this._SelectedDataSource.Providers.GetEnumerator();
                            _ = e.MoveNext();
                            this.SetSelectedDataProvider( this._SelectedDataSource, e.Current, true );
                            break;
                        default:
                            DataProvider defaultProvider = this._SelectedDataSource.DefaultProvider;
                            if ( this._DataProviderSelections.ContainsKey( this._SelectedDataSource ) )
                            {
                                defaultProvider = this._DataProviderSelections[this._SelectedDataSource];
                            }
                            this.SetSelectedDataProvider( this._SelectedDataSource, defaultProvider, noSingleItemCheck );
                            break;
                    }
                }
                else
                {
                    this._SelectedDataSource = null;
                }

                if ( this._ShowingDialog )
                {
                    this.ConfigureDataSourceTextBox();
                }
            }
        }

        private void SetSelectedDataProvider( DataSource dataSource, DataProvider value, bool noSingleItemCheck )
        {
            Debug.Assert( dataSource != null );
            if ( !noSingleItemCheck && dataSource.Providers.Count == 1 &&
                ((this._DataProviderSelections.ContainsKey( dataSource ) && this._DataProviderSelections[dataSource] != value) ||
                (!this._DataProviderSelections.ContainsKey( dataSource ) && value != null)) )
            {
                IEnumerator<DataProvider> e = dataSource.Providers.GetEnumerator();
                _ = e.MoveNext();
                if ( value != e.Current )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotChangeSingleDataProvider );
                }
            }
            if ( (this._DataProviderSelections.ContainsKey( dataSource ) && this._DataProviderSelections[dataSource] != value) ||
                (!this._DataProviderSelections.ContainsKey( dataSource ) && value != null) )
            {
                if ( value != null )
                {
                    if ( !dataSource.Providers.Contains( value ) )
                    {
                        throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_DataSourceNoAssociation );
                    }
                    this._DataProviderSelections[dataSource] = value;
                }
                else if ( this._DataProviderSelections.ContainsKey( dataSource ) )
                {
                    _ = this._DataProviderSelections.Remove( dataSource );
                }

                if ( this._ShowingDialog )
                {
                    this.ConfigureContainerControl();
                }
            }
        }

        private void ConfigureDataSourceTextBox()
        {
            if ( this.SelectedDataSource != null )
            {
                if ( this.SelectedDataSource == this.UnspecifiedDataSource )
                {
                    this.dataSourceTextBox.Text = this.SelectedDataProvider?.DisplayName;
                    this.dataProviderToolTip.SetToolTip( this.dataSourceTextBox, null );
                }
                else
                {
                    this.dataSourceTextBox.Text = this.SelectedDataSource.DisplayName;
                    if ( this.SelectedDataProvider != null )
                    {
                        if ( this.SelectedDataProvider.ShortDisplayName != null )
                        {
                            this.dataSourceTextBox.Text = String.Format( Dialog.Resources.Strings.DataConnectionDialog_DataSourceWithShortProvider, this.dataSourceTextBox.Text, this.SelectedDataProvider.ShortDisplayName );
                        }
                        this.dataProviderToolTip.SetToolTip( this.dataSourceTextBox, this.SelectedDataProvider.DisplayName );
                    }
                    else
                    {
                        this.dataProviderToolTip.SetToolTip( this.dataSourceTextBox, null );
                    }
                }
            }
            else
            {
                this.dataSourceTextBox.Text = null;
                this.dataProviderToolTip.SetToolTip( this.dataSourceTextBox, null );
            }
            this.dataSourceTextBox.Select( 0, 0 );
        }

        private void ConfigureChangeDataSourceButton()
        {
            this.changeDataSourceButton.Enabled = (this.DataSources.Count > 1 || this.SelectedDataSource.Providers.Count > 1);
        }

        private void ChangeDataSource( object sender, EventArgs e )
        {
            this.DialogResult = DialogResult.Ignore;
            this.Close();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void ConfigureContainerControl()
        {
            if ( this.containerControl.Controls.Count == 0 )
            {
                this._InitialContainerControlSize = this.containerControl.Size;
            }
            if ( (this.containerControl.Controls.Count == 0 && this.ConnectionUIControl != null) ||
                (this.containerControl.Controls.Count > 0 && this.ConnectionUIControl != this.containerControl.Controls[0]) )
            {
                this.containerControl.Controls.Clear();
                if ( this.ConnectionUIControl != null && this.ConnectionUIControl.PreferredSize.Width > 0 && this.ConnectionUIControl.PreferredSize.Height > 0 )
                {
                    // Add it to the container control
                    this.containerControl.Controls.Add( this.ConnectionUIControl );

                    // Size dialog appropriately
                    this.MinimumSize = Size.Empty;
                    Size currentSize = this.containerControl.Size;
                    this.containerControl.Size = this._InitialContainerControlSize;
                    Size preferredSize = this.ConnectionUIControl.PreferredSize;
                    this.containerControl.Size = currentSize;
                    int minimumWidth =
                        this._InitialContainerControlSize.Width - (this.Width - this.ClientSize.Width) -
                        this.Padding.Left -
                        this.containerControl.Margin.Left -
                        this.containerControl.Margin.Right -
                        this.Padding.Right;
                    minimumWidth = Math.Max( minimumWidth,
                        this.testConnectionButton.Width +
                        this.testConnectionButton.Margin.Right +
                        this.buttonsTableLayoutPanel.Margin.Left +
                        this.buttonsTableLayoutPanel.Width +
                        this.buttonsTableLayoutPanel.Margin.Right );
                    preferredSize.Width = Math.Max( preferredSize.Width, minimumWidth );
                    this.Size += preferredSize - this.containerControl.Size;
                    if ( this.containerControl.Bottom == this.advancedButton.Top )
                    {
                        this.containerControl.Margin = new Padding(
                                this.containerControl.Margin.Left,
                                this.dataSourceTableLayoutPanel.Margin.Bottom,
                                this.containerControl.Margin.Right,
                                this.advancedButton.Margin.Top );
                        this.Height += this.containerControl.Margin.Bottom + this.advancedButton.Margin.Top;
                        this.containerControl.Height -= this.containerControl.Margin.Bottom + this.advancedButton.Margin.Top;
                    }
                    Size maximumSize =
                        SystemInformation.PrimaryMonitorMaximizedWindowSize -
                        SystemInformation.FrameBorderSize -
                        SystemInformation.FrameBorderSize;
                    if ( this.Width > maximumSize.Width )
                    {
                        this.Width = maximumSize.Width;
                        if ( this.Height + SystemInformation.HorizontalScrollBarHeight <= maximumSize.Height )
                        {
                            this.Height += SystemInformation.HorizontalScrollBarHeight;
                        }
                    }
                    if ( this.Height > maximumSize.Height )
                    {
                        if ( this.Width + SystemInformation.VerticalScrollBarWidth <= maximumSize.Width )
                        {
                            this.Width += SystemInformation.VerticalScrollBarWidth;
                        }
                        this.Height = maximumSize.Height;
                    }
                    this.MinimumSize = this.Size;

                    // The advanced button is only enabled for actual UI controls
                    this.advancedButton.Enabled = !(this.ConnectionUIControl is PropertyGridUIControl);
                }
                else
                {
                    // Size dialog appropriately
                    this.MinimumSize = Size.Empty;
                    if ( this.containerControl.Bottom != this.advancedButton.Top )
                    {
                        this.containerControl.Height += this.containerControl.Margin.Bottom + this.advancedButton.Margin.Top;
                        this.Height -= this.containerControl.Margin.Bottom + this.advancedButton.Margin.Top;
                        this.containerControl.Margin = new Padding(
                                this.containerControl.Margin.Left,
                                0,
                                this.containerControl.Margin.Right,
                                0 );
                    }
                    this.Size -= this.containerControl.Size - new Size( 300, 0 );
                    this.MinimumSize = this.Size;

                    // The advanced button is always enabled for no UI control
                    this.advancedButton.Enabled = true;
                }
            }
            if ( this.ConnectionUIControl != null )
            {
                // Load properties into the connection UI control
                try
                {
                    this._ConnectionUIControlTable[this.SelectedDataSource][this.SelectedDataProvider].LoadProperties();
                }
                catch { }
            }
        }
        private Size _InitialContainerControlSize;

        private void SetConnectionUIControlDockStyle( object sender, EventArgs e )
        {
            if ( this.containerControl.Controls.Count > 0 )
            {
                DockStyle dockStyle = DockStyle.None;
                Size containerControlSize = this.containerControl.Size;
                Size connectionUIControlMinimumSize = this.containerControl.Controls[0].MinimumSize;
                if ( containerControlSize.Width >= connectionUIControlMinimumSize.Width &&
                    containerControlSize.Height >= connectionUIControlMinimumSize.Height )
                {
                    dockStyle = DockStyle.Fill;
                }
                if ( containerControlSize.Width - SystemInformation.VerticalScrollBarWidth >= connectionUIControlMinimumSize.Width &&
                    containerControlSize.Height < connectionUIControlMinimumSize.Height )
                {
                    dockStyle = DockStyle.Top;
                }
                if ( containerControlSize.Width < connectionUIControlMinimumSize.Width &&
                    containerControlSize.Height - SystemInformation.HorizontalScrollBarHeight >= connectionUIControlMinimumSize.Height )
                {
                    dockStyle = DockStyle.Left;
                }
                this.containerControl.Controls[0].Dock = dockStyle;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void ShowAdvanced( object sender, EventArgs e )
        {
            DataConnectionAdvancedDialog advancedDialog = new DataConnectionAdvancedDialog( this.ConnectionProperties, this );
            DialogResult dialogResult = DialogResult.None;
            try
            {
                if ( this.Container != null )
                {
                    this.Container.Add( advancedDialog );
                }
                dialogResult = advancedDialog.ShowDialog( this );
            }
            finally
            {
                if ( this.Container != null )
                {
                    this.Container.Remove( advancedDialog );
                }
                advancedDialog.Dispose();
            }
            if ( dialogResult == DialogResult.OK && this.ConnectionUIControl != null )
            {
                try
                {
                    this._ConnectionUIControlTable[this.SelectedDataSource][this.SelectedDataProvider].LoadProperties();
                }
                catch { }
                this.ConfigureAcceptButton( this, EventArgs.Empty );
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void TestConnection( object sender, EventArgs e )
        {
            Cursor currentCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.ConnectionProperties.Test();
            }
            catch ( Exception ex )
            {
                Cursor.Current = currentCursor;
                this.ShowError( Dialog.Resources.Strings.DataConnectionDialog_TestResults, ex );
                return;
            }
            Cursor.Current = currentCursor;
            this.ShowMessage( Dialog.Resources.Strings.DataConnectionDialog_TestResults, Dialog.Resources.Strings.DataConnectionDialog_TestConnectionSucceeded );
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void ConfigureAcceptButton( object sender, EventArgs e )
        {
            try
            {
                this.acceptButton.Enabled = (this.ConnectionProperties != null) ? this.ConnectionProperties.IsComplete : false;
            }
            catch
            {
                this.acceptButton.Enabled = true;
            }
        }

        private void HandleAccept( object sender, EventArgs e )
        {
            _ = this.acceptButton.Focus(); // ensures connection properties are up to date
        }

        private void PaintSeparator( object sender, PaintEventArgs e )
        {
            Graphics graphics = e.Graphics;
            Pen dark = new Pen( ControlPaint.Dark( this.BackColor, 0f ) );
            Pen light = new Pen( ControlPaint.Light( this.BackColor, 1f ) );
            int width = this.separatorPanel.Width;

            graphics.DrawLine( dark, 0, 0, width, 0 );
            graphics.DrawLine( light, 0, 1, width, 1 );
        }

        private void HandleDialogException( object sender, ThreadExceptionEventArgs e )
        {
            this.OnDialogException( e );
        }

        private void ShowMessage( string title, string message )
        {
            if ( this.GetService( typeof( IUIService ) ) is IUIService uiService )
            {
                uiService.ShowMessage( message );
            }
            else
            {
                _ = RTLAwareMessageBox.Show( title, message, MessageBoxIcon.Information );
            }
        }

        private void ShowError( string title, Exception ex )
        {
            if ( this.GetService( typeof( IUIService ) ) is IUIService uiService )
            {
                uiService.ShowError( ex );
            }
            else
            {
                _ = RTLAwareMessageBox.Show( title, ex.Message, MessageBoxIcon.Exclamation );
            }
        }

        private void ShowError( string title, string message )
        {
            if ( this.GetService( typeof( IUIService ) ) is IUIService uiService )
            {
                uiService.ShowError( message );
            }
            else
            {
                _ = RTLAwareMessageBox.Show( title, message, MessageBoxIcon.Exclamation );
            }
        }

        private class DataSourceCollection : ICollection<DataSource>
        {
            public DataSourceCollection( DataConnectionDialog dialog )
            {
                Debug.Assert( dialog != null );

                this._Dialog = dialog;
            }

            public int Count => this._List.Count;

            public bool IsReadOnly => this._Dialog._ShowingDialog;

            public void Add( DataSource item )
            {
                if ( item == null )
                {
                    throw new ArgumentNullException( nameof( item ) );
                }
                if ( this._Dialog._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                if ( !this._List.Contains( item ) )
                {
                    this._List.Add( item );
                }
            }

            public bool Contains( DataSource item )
            {
                return this._List.Contains( item );
            }

            public bool Remove( DataSource item )
            {
                if ( this._Dialog._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                bool result = this._List.Remove( item );
                if ( item == this._Dialog.SelectedDataSource )
                {
                    this._Dialog.SetSelectedDataSource( null, true );
                }
                return result;
            }

            public void Clear()
            {
                if ( this._Dialog._ShowingDialog )
                {
                    throw new InvalidOperationException( Dialog.Resources.Strings.DataConnectionDialog_CannotModifyState );
                }
                this._List.Clear();
                this._Dialog.SetSelectedDataSource( null, true );
            }

            public void CopyTo( DataSource[] array, int arrayIndex )
            {
                this._List.CopyTo( array, arrayIndex );
            }

            public IEnumerator<DataSource> GetEnumerator()
            {
                return this._List.GetEnumerator();
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return this._List.GetEnumerator();
            }

            private readonly List<DataSource> _List = new List<DataSource>();
            private readonly DataConnectionDialog _Dialog;
        }

        private class PropertyGridUIControl : UserControl, IDataConnectionUIControl
        {
            public PropertyGridUIControl()
            {
                this._PropertyGrid = new DataConnectionAdvancedDialog.SpecializedPropertyGrid();
                this.SuspendLayout();
                // 
                // propertyGrid
                // 
                this._PropertyGrid.CommandsVisibleIfAvailable = true;
                this._PropertyGrid.Dock = DockStyle.Fill;
                this._PropertyGrid.Location = Point.Empty;
                this._PropertyGrid.Margin = new Padding( 0 );
                this._PropertyGrid.Name = "propertyGrid";
                this._PropertyGrid.TabIndex = 0;
                // 
                // DataConnectionDialog
                // 
                this.Controls.Add( this._PropertyGrid );
                this.Name = "PropertyGridUIControl";
                this.ResumeLayout( false );
                this.PerformLayout();
            }

            public void Initialize( IDataConnectionProperties dataConnectionProperties )
            {
                this._ConnectionProperties = dataConnectionProperties;
            }

            public void LoadProperties()
            {
                this._PropertyGrid.SelectedObject = this._ConnectionProperties;
            }

            public override Size GetPreferredSize( Size proposedSize )
            {
                return this._PropertyGrid.GetPreferredSize( proposedSize );
            }

            private IDataConnectionProperties _ConnectionProperties;
            private readonly DataConnectionAdvancedDialog.SpecializedPropertyGrid _PropertyGrid;
        }

        private class BasicConnectionProperties : IDataConnectionProperties
        {
            public BasicConnectionProperties()
            {
            }

            public void Reset()
            {
                this._S = String.Empty;
            }

            public void Parse( string s )
            {
                this._S = s;
                if ( PropertyChanged != null )
                {
                    PropertyChanged( this, EventArgs.Empty );
                }
            }

            [Browsable( false )]
            public bool IsExtensible => false;

            public void Add( string propertyName )
            {
                throw new NotImplementedException();
            }

            public bool Contains( string propertyName )
            {
                return (propertyName == "ConnectionString");
            }

            public object this[string propertyName]
            {
                get => propertyName == "ConnectionString" ? this.ConnectionString : null;
                set {
                    if ( propertyName == "ConnectionString" )
                    {
                        this.ConnectionString = value as string;
                    }
                }
            }

            public void Remove( string propertyName )
            {
                throw new NotImplementedException();
            }

            public event EventHandler PropertyChanged;

            public void Reset( string propertyName )
            {
                Debug.Assert( propertyName == "ConnectionString" );
                this._S = String.Empty;
            }

            [Browsable( false )]
            public bool IsComplete => true;

            public string ConnectionString
            {
                get => this.ToFullString();
                set => this.Parse( value );
            }

            public void Test()
            {
            }

            public string ToFullString()
            {
                return this._S;
            }

            public string ToDisplayString()
            {
                return this._S;
            }

            private string _S;
        }

        private bool _ShowingDialog;
        private Label _HeaderLabel;
        private string _ChooseDataSourceTitle;
        private string _ChooseDataSourceHeaderLabel = String.Empty;
        private string _ChooseDataSourceAcceptText;
        private string _ChangeDataSourceTitle;
        private string _ChangeDataSourceHeaderLabel = String.Empty;
        private DataSource _SelectedDataSource;
        private readonly IDictionary<DataSource, DataProvider> _DataProviderSelections = new Dictionary<DataSource, DataProvider>();
        private readonly IDictionary<DataSource, IDictionary<DataProvider, IDataConnectionUIControl>> _ConnectionUIControlTable = new Dictionary<DataSource, IDictionary<DataProvider, IDataConnectionUIControl>>();
        private readonly IDictionary<DataSource, IDictionary<DataProvider, IDataConnectionProperties>> _ConnectionPropertiesTable = new Dictionary<DataSource, IDictionary<DataProvider, IDataConnectionProperties>>();
    }
}
