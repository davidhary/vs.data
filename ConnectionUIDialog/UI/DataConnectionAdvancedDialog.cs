//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.ComponentModel;

namespace Microsoft.Data.ConnectionUI
{
    internal partial class DataConnectionAdvancedDialog : Form
    {
        public DataConnectionAdvancedDialog()
        {
            this.InitializeComponent();

            // Make sure we handle a user preference change
            if ( this.components == null )
            {
                this.components = new System.ComponentModel.Container();
            }
            this.components.Add( new UserPreferenceChangedHandler( this ) );
        }

        public DataConnectionAdvancedDialog( IDataConnectionProperties connectionProperties, DataConnectionDialog mainDialog )
            : this()
        {
            Debug.Assert( connectionProperties != null );
            Debug.Assert( mainDialog != null );

            this._SavedConnectionString = connectionProperties.ToFullString();

            this.propertyGrid.SelectedObject = connectionProperties;

            this._MainDialog = mainDialog;
        }

        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );
            this.ConfigureTextBox();
        }

        protected override void OnShown( EventArgs e )
        {
            base.OnShown( e );
            _ = this.propertyGrid.Focus();
        }

        protected override void OnFontChanged( EventArgs e )
        {
            base.OnFontChanged( e );

            this.textBox.Width = this.propertyGrid.Width;
        }

        protected override void OnHelpRequested( HelpEventArgs hevent )
        {
            // Get the active control
            Control activeControl = this;
            while ( activeControl is ContainerControl containerControl &&
                containerControl != this.propertyGrid &&
                containerControl.ActiveControl != null )
            {
                activeControl = containerControl.ActiveControl;
            }

            // Figure out the context
            DataConnectionDialogContext context = DataConnectionDialogContext.Advanced;
            if ( activeControl == this.propertyGrid )
            {
                context = DataConnectionDialogContext.AdvancedPropertyGrid;
            }
            if ( activeControl == this.textBox )
            {
                context = DataConnectionDialogContext.AdvancedTextBox;
            }
            if ( activeControl == this.okButton )
            {
                context = DataConnectionDialogContext.AdvancedOkButton;
            }
            if ( activeControl == this.cancelButton )
            {
                context = DataConnectionDialogContext.AdvancedCancelButton;
            }

            // Call OnContextHelpRequested
            ContextHelpEventArgs e = new ContextHelpEventArgs( context, hevent.MousePos );
            this._MainDialog.OnContextHelpRequested( e );
            hevent.Handled = e.Handled;
            if ( !e.Handled )
            {
                base.OnHelpRequested( hevent );
            }
        }

        protected override void WndProc( ref Message m )
        {
            if ( this._MainDialog.TranslateHelpButton && HelpUtils.IsContextHelpMessage( ref m ) )
            {
                // Force the ? in the title bar to invoke the help topic
                HelpUtils.TranslateContextHelpMessage( this, ref m );
            }
            base.WndProc( ref m );
        }

        internal class SpecializedPropertyGrid : PropertyGrid
        {
            public SpecializedPropertyGrid()
            {
                this._ContextMenu = new ContextMenuStrip();

                this._ContextMenu.Items.AddRange( new ToolStripItem[] {
                    new ToolStripMenuItem(),
                    new ToolStripSeparator(),
                    new ToolStripMenuItem(),
                    new ToolStripMenuItem(),
                    new ToolStripSeparator(),
                    new ToolStripMenuItem()
                } );
                this._ContextMenu.Items[0].Text = Dialog.Resources.Strings.DataConnectionAdvancedDialog_Reset;
                this._ContextMenu.Items[0].Click += new EventHandler( this.ResetProperty );
                this._ContextMenu.Items[2].Text = Dialog.Resources.Strings.DataConnectionAdvancedDialog_Add;
                this._ContextMenu.Items[2].Click += new EventHandler( this.AddProperty );
                this._ContextMenu.Items[3].Text = Dialog.Resources.Strings.DataConnectionAdvancedDialog_Remove;
                this._ContextMenu.Items[3].Click += new EventHandler( this.RemoveProperty );
                this._ContextMenu.Items[5].Text = Dialog.Resources.Strings.DataConnectionAdvancedDialog_Description;
                this._ContextMenu.Items[5].Click += new EventHandler( this.ToggleDescription );
                (this._ContextMenu.Items[5] as ToolStripMenuItem).Checked = this.HelpVisible;
                this._ContextMenu.Opened += new EventHandler( this.SetupContextMenu );

                this.ContextMenuStrip = this._ContextMenu;
                this.DrawFlatToolbar = true;
                this.Size = new Size( 270, 250 ); // magic numbers, but a reasonable starting point
                this.MinimumSize = this.Size;
            }

            protected override void OnHandleCreated( EventArgs e )
            {
                ProfessionalColorTable colorTable = (this.ParentForm != null && this.ParentForm.Site != null) ? this.ParentForm.Site.GetService( typeof( ProfessionalColorTable ) ) as ProfessionalColorTable : null;
                if ( colorTable != null )
                {
                    this.ToolStripRenderer = new ToolStripProfessionalRenderer( colorTable );
                }
                base.OnHandleCreated( e );
            }

            protected override void OnFontChanged( EventArgs e )
            {
                base.OnFontChanged( e );
                this.LargeButtons = (this.Font.SizeInPoints >= 15.0);
            }

            protected override void WndProc( ref Message m )
            {
                switch ( m.Msg )
                {
                    case NativeMethods.WM_SETFOCUS:
                        // Make sure the property grid view has proper focus
                        _ = this.Focus();
                        (( System.Windows.Forms.ComponentModel.Com2Interop.IComPropertyBrowser ) this).HandleF4();
                        break;
                }
                base.WndProc( ref m );
            }

            private void SetupContextMenu( object sender, System.EventArgs e )
            {
                // Decide if reset should be enabled
                this._ContextMenu.Items[0].Enabled = (this.SelectedGridItem.GridItemType == GridItemType.Property);
                if ( this._ContextMenu.Items[0].Enabled && this.SelectedGridItem.PropertyDescriptor != null )
                {
                    object propertyOwner = this.SelectedObject;
                    if ( this.SelectedObject is ICustomTypeDescriptor )
                    {
                        propertyOwner = (this.SelectedObject as ICustomTypeDescriptor).GetPropertyOwner( this.SelectedGridItem.PropertyDescriptor );
                    }
                    this._ContextMenu.Items[0].Enabled = this._ContextMenu.Items[3].Enabled = this.SelectedGridItem.PropertyDescriptor.CanResetValue( propertyOwner );
                }

                // Decide if we are allowed to add/remove custom properties
                this._ContextMenu.Items[2].Visible = this._ContextMenu.Items[3].Visible = (this.SelectedObject as IDataConnectionProperties).IsExtensible;
                if ( this._ContextMenu.Items[3].Visible )
                {
                    this._ContextMenu.Items[3].Enabled = (this.SelectedGridItem.GridItemType == GridItemType.Property);
                    if ( this._ContextMenu.Items[3].Enabled && this.SelectedGridItem.PropertyDescriptor != null )
                    {
                        this._ContextMenu.Items[3].Enabled = !this.SelectedGridItem.PropertyDescriptor.IsReadOnly;
                    }
                }

                // Hide the first separator if there is no need for it
                this._ContextMenu.Items[1].Visible = (this._ContextMenu.Items[2].Visible || this._ContextMenu.Items[3].Visible);
            }

            private void ResetProperty( object sender, System.EventArgs e )
            {
                object oldValue = this.SelectedGridItem.Value;
                object propertyOwner = this.SelectedObject;
                if ( this.SelectedObject is ICustomTypeDescriptor )
                {
                    propertyOwner = (this.SelectedObject as ICustomTypeDescriptor).GetPropertyOwner( this.SelectedGridItem.PropertyDescriptor );
                }
                this.SelectedGridItem.PropertyDescriptor.ResetValue( propertyOwner );
                this.Refresh();
                this.OnPropertyValueChanged( new PropertyValueChangedEventArgs( this.SelectedGridItem, oldValue ) );
            }

            private void AddProperty( object sender, System.EventArgs e )
            {
                if ( !(this.ParentForm is DataConnectionDialog mainDialog) )
                {
                    Debug.Assert( this.ParentForm is DataConnectionAdvancedDialog );
                    mainDialog = (this.ParentForm as DataConnectionAdvancedDialog)._MainDialog;
                    Debug.Assert( mainDialog != null );
                }
                AddPropertyDialog dialog = new AddPropertyDialog( mainDialog );
                try
                {
                    if ( this.ParentForm.Container != null )
                    {
                        this.ParentForm.Container.Add( dialog );
                    }
                    DialogResult result = dialog.ShowDialog( this.ParentForm );
                    if ( result == DialogResult.OK )
                    {
                        (this.SelectedObject as IDataConnectionProperties).Add( dialog.PropertyName );
                        this.Refresh();
                        GridItem rootItem = this.SelectedGridItem;
                        while ( rootItem.Parent != null )
                        {
                            rootItem = rootItem.Parent;
                        }
                        GridItem newItem = this.LocateGridItem( rootItem, dialog.PropertyName );
                        if ( newItem != null )
                        {
                            this.SelectedGridItem = newItem;
                        }
                    }
                }
                finally
                {
                    if ( this.ParentForm.Container != null )
                    {
                        this.ParentForm.Container.Remove( dialog );
                    }
                    dialog.Dispose();
                }
            }

            private void RemoveProperty( object sender, System.EventArgs e )
            {
                (this.SelectedObject as IDataConnectionProperties).Remove( this.SelectedGridItem.Label );
                this.Refresh();
                this.OnPropertyValueChanged( new PropertyValueChangedEventArgs( null, null ) );
            }

            private void ToggleDescription( object sender, System.EventArgs e )
            {
                this.HelpVisible = !this.HelpVisible;
                (this._ContextMenu.Items[5] as ToolStripMenuItem).Checked = !(this._ContextMenu.Items[5] as ToolStripMenuItem).Checked;
            }

            private GridItem LocateGridItem( GridItem currentItem, string propertyName )
            {
                if ( currentItem.GridItemType == GridItemType.Property &&
                    currentItem.Label.Equals( propertyName, StringComparison.CurrentCulture ) )
                {
                    return currentItem;
                }

                GridItem foundItem = null;
                foreach ( GridItem childItem in currentItem.GridItems )
                {
                    foundItem = this.LocateGridItem( childItem, propertyName );
                    if ( foundItem != null )
                    {
                        break;
                    }
                }

                return foundItem;
            }

            private readonly ContextMenuStrip _ContextMenu;
        }

        private void SetTextBox( object s, PropertyValueChangedEventArgs e )
        {
            this.ConfigureTextBox();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void ConfigureTextBox()
        {
            if ( this.propertyGrid.SelectedObject is IDataConnectionProperties )
            {
                try
                {
                    this.textBox.Text = (this.propertyGrid.SelectedObject as IDataConnectionProperties).ToDisplayString();
                }
                catch
                {
                    this.textBox.Text = null;
                }
            }
            else
            {
                this.textBox.Text = null;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void RevertProperties( object sender, EventArgs e )
        {
            try
            {
                (this.propertyGrid.SelectedObject as IDataConnectionProperties).Parse( this._SavedConnectionString );
            }
            catch { }
        }

        private readonly string _SavedConnectionString;
        private readonly DataConnectionDialog _MainDialog;
    }
}
