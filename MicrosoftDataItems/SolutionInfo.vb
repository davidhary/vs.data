' file:	MicrosoftDataItems\SolutionInfo.vb
' 
' summary:	Solution information Class
' ---------------------------------------------------------------------------------------------------
Imports System.Reflection

<Assembly: AssemblyCompany("Microsoft")>
<Assembly: AssemblyCopyright("(c) 2009 Microsoft Corporation. All rights reserved.")>
<Assembly: AssemblyTrademark("Licensed under The MIT License.")>
<Assembly: System.Resources.NeutralResourcesLanguage("en-US", System.Resources.UltimateResourceFallbackLocation.MainAssembly)>
