//------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Data.ConnectionUI
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1052:Static holder types should be Static or NotInheritable", Justification = "<Pending>" )]
    public class SqlCe
	{
		public static DataSource SqlCeDataSource
		{
			get
			{
				if (_SqlCeDataSource == null)
				{
					_SqlCeDataSource = new DataSource("SqlCeClient", "Microsoft SQL Server Compact 3.5");
					_SqlCeDataSource.Providers.Add(SqlCeDataProvider);
				}
				return _SqlCeDataSource;
			}
		}

		private static DataSource _SqlCeDataSource;

		public static DataProvider SqlCeDataProvider
		{
			get
			{
				if (_SqlCeDataProvider == null)
				{
                    Dictionary<string, string> descriptions = new Dictionary<string, string> {
                        { SqlCeDataSource.Name, Resources.DataProvider_SqlEverywhere_Description }
                    };

                    Dictionary<string, Type> uiControls = new Dictionary<string, Type> {
                        { String.Empty, typeof( SqlCeConnectionUIControl ) }
                    };

                    _SqlCeDataProvider = new DataProvider(
						"System.Data.SqlCeClient",
						Resources.DataProvider_SqlEverywhere,
						"SqlCeClient",
						Resources.DataProvider_SqlEverywhere_Description,
						typeof(System.Data.SqlServerCe.SqlCeConnection),
						descriptions,
						uiControls,
						typeof(SqlCeConnectionProperties));
				}
				return _SqlCeDataProvider;
			}
		}
		private static DataProvider _SqlCeDataProvider;
	}
}
