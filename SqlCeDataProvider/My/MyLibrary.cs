
namespace Microsoft.Data.ConnectionUI.My
{
    /// <summary>   my library. </summary>
    /// <remarks>   David, 2020-03-18. </remarks>
    internal class MyLibrary
    {
        public const string AssemblyTitle = "Microsoft Data Connection UI SqlCe Data Provider Library";
        public const string AssemblyDescription = "Microsoft Data Connection UI SqlCe Data Provider Library";
        public const string AssemblyProduct = "Microsoft.Data.ConnectionUI.SqlCeDataProvider";
        public const string AssemblyConfiguration = "Yet to be tested";
    }
}
